# NOISYmputer

This is the core code of [NoisymputerStandAlone](https://gitlab.cirad.fr/noisymputer/noisymputerstandalone). It contains all the functions used in pre-imputation, imputation and post-imputation steps. (Data reading is done in NoisymputerStandAlone.)  
This code could be reused as a dependency into an existing java tool to add an imputation functionality.

All data that must be imputed is stored as AllDataInBytes object. AllDataInBytes class contains all the methods applied to the data to perform imputation.
