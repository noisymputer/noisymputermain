/*******************************************************************************
 * NOISYmputer
 * Distributed under the GNU Affero General Public License V3.0
 * See <https://www.gnu.org/licenses/agpl-3.0.en.html> for details
 ******************************************************************************/
package noisymputer;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author boizet
 */
public class SnpSelectionCriteria {
    
    private String parent1;
    private String parent2;
    private PopType popType;
    public enum PopType {F2, BC1, SSD, DH, BCSSD, BC1F3} ;
    private long bpPercM = 240000;
    private MappingFunction mappingFunction = MappingFunction.KOSAMBI;
    public enum MappingFunction {NONE, KOSAMBI, HALDANE};
    
    private List<String> regions = new ArrayList();
    private List<String> excludedRegions = new ArrayList();
    private String regionsFile = "";
    private String excludedRegionsFile = "";
    
    private int minReadsDP = -1;
    private int maxReadsDP = -1;
    private int minReadsVariant = -1;
    private int maxReadsVariant = -1;
       
    private boolean filterVariantsOnGenotypicFrequencies = true;
    private double minFreqMD = -1;
    private double maxFreqMD = -1;
    private double minFreqA = -1;
    private double minFreqB = -1;
    private double minFreqH = -1; 
    private double maxFreqA = -1;
    private double maxFreqB = -1;
    private double maxFreqH = -1; 
    
    private double lowPercentileA = 0.025;
    private double lowPercentileB = 0.025;
    private double lowPercentileH = 0.025;
    private double lowPercentileMD = 0.025;
    private double lowPercentileTrans = 0.025;
    private double highPercentileA = 0.975;
    private double highPercentileB = 0.975;
    private double highPercentileH = 0.975;
    private double highPercentileMD = 0.95;
    private double highPercentileTrans = 0.975;

    private double correctMinPercentileA = 0.001;
    private double correctMaxPercentileA = 0.95;
    private double correctMinPercentileB = 0.001;
    private double correctMaxPercentileB = 0.95;
    private double correctMinPercentileH = 0.001;
    private double correctMaxPercentileH = 0.75;
    private double correctMinPercentileMD = 0.001;
    private double correctMaxPercentileMD = 0.75;    
        
        
    private boolean filterOutRedundant = false;
    
    private boolean correctFalseHTZ = false;
    private boolean filterOutIncoherent= true;
    private boolean fillMissingDataBefore = false;
    private int incoherentHalfWindowSize = -1;
    
    private double chi2alpha = 0.01;
    private boolean tryOtherPhase = false;    
    
    private boolean correctSingletons = false;
    
    //Imputation
    private boolean impute = true;
    private boolean imputeStep1 = true;
    private boolean useJointProbas = true;
    private boolean imputeStep2 = true;
    private boolean imputeStep3 = true;
    
    private double errorA = 0.05;
    private double errorB = 0.03;
    private int imputeHalfWindowSize = 20;   
    private double genoLRthreshold = 0.999;
    private double alphaFillGaps = 0.01;
    private int breakpointHalfWindowSize = -1;
    private double alphaBreakpointLooseInterval = 0.05;
    private double dropBreakpointSupportInterval = 1;
    private boolean recalculateErrorsBeforeStep3 = true;
    private int breakpointProbaCalcMethod = 2;
    
    //TODO : remove ?
    private boolean imputeSingletonsInBP = false;
    
    //chunks detection
    private boolean detectImprobableChunks = true;
    private double alphaChunk = 0.001;
    private int smallChunkMaxSize = 500;
    private double chunkEnvironmentMultiplier = 0.51;
    private boolean usePhysicalDistanceToFindDE = true;
    private boolean usePhysicalDistanceToFindB = false;
    
    //Collapsing matrix
    private boolean collapseGenoMatrix = true;
    private boolean keepBothBreakpointMarkers = true;  
    
    //Alien detection
    private boolean filterOutAlienSegments = false;
    private int alienMaxWindowSize = 250;
    private double alienMaxRF = 0.02;
    private int slopeCutoff = 10;
    private int minAlienSize = 20;
    private int maxStartAlienOffset = 20; 
    private boolean newAlienMethod = true;
    private int transitionsNbMinForHS = 10;
    private double minInflationcM = 1;
    private double minInflationPercent = 80; 
    
    private boolean useHtzinSSD = true;
    //private boolean filterOnTransitions = false;
    private int minTransitions = -1;
    private int maxTransitions = -1;

    public String getParent1() {
        return parent1;
    }

    public void setParent1(String parent1) {
        this.parent1 = parent1;
    }

    public String getParent2() {
        return parent2;
    }

    public void setParent2(String parent2) {
        this.parent2 = parent2;
    }

    public PopType getPopType() {
        return popType;
    }

    public void setPopType(PopType popType) {
        this.popType = popType;
    }

    public boolean isKeepBothBreakpointMarkers() {
        return keepBothBreakpointMarkers;
    }

    public void setKeepBothBreakpointMarkers(boolean keepBothBreakpointMarkers) {
        this.keepBothBreakpointMarkers = keepBothBreakpointMarkers;
    }

    public boolean isFilterVariantsOnGenotypicFrequencies() {
        return filterVariantsOnGenotypicFrequencies;
    }

    public void setFilterVariantsOnGenotypicFrequencies(boolean filterVariantsOnGenotypicFrequencies) {
        this.filterVariantsOnGenotypicFrequencies = filterVariantsOnGenotypicFrequencies;
    }

    public boolean isFilterOutRedundant() {
        return filterOutRedundant;
    }

    public void setFilterOutRedundant(boolean filterOutRedundant) {
        this.filterOutRedundant = filterOutRedundant;
    }

    public boolean isCorrectFalseHTZ() {
        return correctFalseHTZ;
    }

    public void setCorrectFalseHTZ(boolean correctFalseHTZ) {
        this.correctFalseHTZ = correctFalseHTZ;
    }

    public boolean isFilterOutIncoherent() {
        return filterOutIncoherent;
    }

    public void setFilterOutIncoherent(boolean filterOutIncoherent) {
        this.filterOutIncoherent = filterOutIncoherent;
    }

    public boolean isCorrectSingletons() {
        return correctSingletons;
    }

    public void setCorrectSingletons(boolean correctSingletons) {
        this.correctSingletons = correctSingletons;
    }

    public boolean isImpute() {
        return impute;
    }

    public void setImpute(boolean impute) {
        this.impute = impute;
    }
    
    public boolean isImputeSingletonsInBP() {
        return imputeSingletonsInBP;
    }

    public void setImputeSingletonsInBP(boolean imputeSingletonsInBP) {
        this.imputeSingletonsInBP = imputeSingletonsInBP;
    }    

    public boolean isDetectImprobableChunks() {
        return detectImprobableChunks;
    }

    public void setDetectImprobableChunks(boolean detectImprobableChunks) {
        this.detectImprobableChunks = detectImprobableChunks;
    }

    public double getAlphaChunk() {
        return alphaChunk;
    }

    public void setAlphaChunk(double alphaChunk) {
        this.alphaChunk = alphaChunk;
    }

    public int getSmallChunkMaxSize() {
        return smallChunkMaxSize;
    }

    public void setSmallChunkMaxSize(int smallChunkMaxSize) {
        this.smallChunkMaxSize = smallChunkMaxSize;
    }
    
    public boolean isCollapseGenoMatrix() {
        return collapseGenoMatrix;
    }

    public void setCollapseGenoMatrix(boolean collapseGenoMatrix) {
        this.collapseGenoMatrix = collapseGenoMatrix;
    }

    public int getMinReadsDP() {
        return minReadsDP;
    }

    public void setMinReadsDP(int minReadsDP) {
        this.minReadsDP = minReadsDP;
    }

    public int getMaxReadsDP() {
        return maxReadsDP;
    }

    public void setMaxReadsDP(int maxReadsDP) {
        this.maxReadsDP = maxReadsDP;
    }

    public int getMinReadsVariant() {
        return minReadsVariant;
    }

    public void setMinReadsVariant(int minReadsVariant) {
        this.minReadsVariant = minReadsVariant;
    }

    public int getMaxReadsVariant() {
        return maxReadsVariant;
    }

    public void setMaxReadsVariant(int maxReadsVariant) {
        this.maxReadsVariant = maxReadsVariant;
    }

    public List<String> getRegions() {
        return regions;
    }

    public void setRegions(List<String> regions) {
        this.regions = regions;
    }

    public int getIncoherentHalfWindowSize() {
        return incoherentHalfWindowSize;
    }

    public void setIncoherentHalfWindowSize(int incoherentHalfWindowSize) {
        this.incoherentHalfWindowSize = incoherentHalfWindowSize;
    }

    public int getImputeHalfWindowSize() {
        return imputeHalfWindowSize;
    }

    public void setImputeHalfWindowSize(int imputeHalfWindowSize) {
        this.imputeHalfWindowSize = imputeHalfWindowSize;
    }

    public double getChi2alpha() {
        return chi2alpha;
    }

    public void setChi2alpha(double chi2alpha) {
        this.chi2alpha = chi2alpha;
    }

    public boolean isTryOtherPhase() {
        return tryOtherPhase;
    }

    public void setTryOtherPhase(boolean tryOtherPhase) {
        this.tryOtherPhase = tryOtherPhase;
    }

    public double getMinFreqMD() {
        return minFreqMD;
    }

    public void setMinFreqMD(double minFreqMD) {
        this.minFreqMD = minFreqMD;
    }

    public double getMaxFreqMD() {
        return maxFreqMD;
    }

    public void setMaxFreqMD(double maxFreqMD) {
        this.maxFreqMD = maxFreqMD;
    }

    public double getMinFreqA() {
        return minFreqA;
    }

    public void setMinFreqA(double minFreqA) {
        this.minFreqA = minFreqA;
    }

    public double getMinFreqB() {
        return minFreqB;
    }

    public void setMinFreqB(double minFreqB) {
        this.minFreqB = minFreqB;
    }

    public double getMinFreqH() {
        return minFreqH;
    }

    public void setMinFreqH(double minFreqH) {
        this.minFreqH = minFreqH;
    }

    public double getMaxFreqA() {
        return maxFreqA;
    }

    public void setMaxFreqA(double maxFreqA) {
        this.maxFreqA = maxFreqA;
    }

    public double getMaxFreqB() {
        return maxFreqB;
    }

    public void setMaxFreqB(double maxFreqB) {
        this.maxFreqB = maxFreqB;
    }

    public double getMaxFreqH() {
        return maxFreqH;
    }

    public void setMaxFreqH(double maxFreqH) {
        this.maxFreqH = maxFreqH;
    }

    public double getLowPercentileA() {
        return lowPercentileA;
    }

    public void setLowPercentileA(double lowPercentileA) {
        this.lowPercentileA = lowPercentileA;
    }

    public double getLowPercentileB() {
        return lowPercentileB;
    }

    public void setLowPercentileB(double lowPercentileB) {
        this.lowPercentileB = lowPercentileB;
    }

    public double getLowPercentileH() {
        return lowPercentileH;
    }

    public void setLowPercentileH(double lowPercentileH) {
        this.lowPercentileH = lowPercentileH;
    }

    public double getLowPercentileMD() {
        return lowPercentileMD;
    }

    public void setLowPercentileMD(double lowPercentileMD) {
        this.lowPercentileMD = lowPercentileMD;
    }

    public double getHighPercentileA() {
        return highPercentileA;
    }

    public void setHighPercentileA(double highPercentileA) {
        this.highPercentileA = highPercentileA;
    }

    public double getHighPercentileB() {
        return highPercentileB;
    }

    public void setHighPercentileB(double highPercentileB) {
        this.highPercentileB = highPercentileB;
    }

    public double getHighPercentileH() {
        return highPercentileH;
    }

    public void setHighPercentileH(double highPercentileH) {
        this.highPercentileH = highPercentileH;
    }

    public double getHighPercentileMD() {
        return highPercentileMD;
    }

    public void setHighPercentileMD(double highPercentileMD) {
        this.highPercentileMD = highPercentileMD;
    }

    public double getCorrectMinPercentileA() {
        return correctMinPercentileA;
    }

    public void setCorrectMinPercentileA(double correctMinPercentileA) {
        this.correctMinPercentileA = correctMinPercentileA;
    }

    public double getCorrectMaxPercentileA() {
        return correctMaxPercentileA;
    }

    public void setCorrectMaxPercentileA(double correctMaxPercentileA) {
        this.correctMaxPercentileA = correctMaxPercentileA;
    }

    public double getCorrectMinPercentileB() {
        return correctMinPercentileB;
    }

    public void setCorrectMinPercentileB(double correctMinPercentileB) {
        this.correctMinPercentileB = correctMinPercentileB;
    }

    public double getCorrectMaxPercentileB() {
        return correctMaxPercentileB;
    }

    public void setCorrectMaxPercentileB(double correctMaxPercentileB) {
        this.correctMaxPercentileB = correctMaxPercentileB;
    }

    public double getCorrectMinPercentileH() {
        return correctMinPercentileH;
    }

    public void setCorrectMinPercentileH(double correctMinPercentileH) {
        this.correctMinPercentileH = correctMinPercentileH;
    }

    public double getCorrectMaxPercentileH() {
        return correctMaxPercentileH;
    }

    public void setCorrectMaxPercentileH(double correctMaxPercentileH) {
        this.correctMaxPercentileH = correctMaxPercentileH;
    }

    public double getCorrectMinPercentileMD() {
        return correctMinPercentileMD;
    }

    public void setCorrectMinPercentileMD(double correctMinPercentileMD) {
        this.correctMinPercentileMD = correctMinPercentileMD;
    }

    public double getCorrectMaxPercentileMD() {
        return correctMaxPercentileMD;
    }

    public void setCorrectMaxPercentileMD(double correctMaxPercentileMD) {
        this.correctMaxPercentileMD = correctMaxPercentileMD;
    }

    public boolean isFilterOutAlienSegments() {
        return filterOutAlienSegments;
    }

    public void setFilterOutAlienSegments(boolean filterOutAlienSegments) {
        this.filterOutAlienSegments = filterOutAlienSegments;
    }

    public int getAlienMaxWindowSize() {
        return alienMaxWindowSize;
    }

    public void setAlienMaxWindowSize(int alienMaxWindowSize) {
        this.alienMaxWindowSize = alienMaxWindowSize;
    }

    public double getAlienMaxRF() {
        return alienMaxRF;
    }

    public void setAlienMaxRF(double alienMaxRF) {
        this.alienMaxRF = alienMaxRF;
    }

    public int getSlopeCutoff() {
        return slopeCutoff;
    }

    public void setSlopeCutoff(int slopeCutoff) {
        this.slopeCutoff = slopeCutoff;
    }

    public int getMinAlienSize() {
        return minAlienSize;
    }

    public void setMinAlienSize(int minAlienSize) {
        this.minAlienSize = minAlienSize;
    }

    public int getMaxStartAlienOffset() {
        return maxStartAlienOffset;
    }

    public void setMaxStartAlienOffset(int maxStartAlienOffset) {
        this.maxStartAlienOffset = maxStartAlienOffset;
    }

    public MappingFunction getMappingFunction() {
        return mappingFunction;
    }

    public void setMappingFunction(MappingFunction mappingFunction) {
        this.mappingFunction = mappingFunction;
    }

    public long getBpPercM() {
        return bpPercM;
    }

    public void setBpPercM(long bpPercM) {
        this.bpPercM = bpPercM;
    }

    public boolean isFillMissingDataBefore() {
        return fillMissingDataBefore;
    }

    public void setFillMissingDataBefore(boolean fillMissingDataBefore) {
        this.fillMissingDataBefore = fillMissingDataBefore;
    }

    public boolean isUseHtzinSSD() {
        return useHtzinSSD;
    }

    public void setUseHtzinSSD(boolean useHtzinSSD) {
        this.useHtzinSSD = useHtzinSSD;
    }
    
    public int getMinTransitions() {
        return minTransitions;
    }

    public void setMinTransitions(int minTransitions) {
        this.minTransitions = minTransitions;
    }

    public int getMaxTransitions() {
        return maxTransitions;
    }

    public void setMaxTransitions(int maxTransitions) {
        this.maxTransitions = maxTransitions;
    }

    public double getLowPercentileTrans() {
        return lowPercentileTrans;
    }

    public void setLowPercentileTrans(double lowPercentileTrans) {
        this.lowPercentileTrans = lowPercentileTrans;
    }

    public double getHighPercentileTrans() {
        return highPercentileTrans;
    }

    public void setHighPercentileTrans(double highPercentileTrans) {
        this.highPercentileTrans = highPercentileTrans;
    }

    public double getGenoLRthreshold() {
        return genoLRthreshold;
    }

    public void setGenoLRthreshold(double genoLRthreshold) {
        this.genoLRthreshold = genoLRthreshold;
    }

    public double getAlphaBreakpointLooseInterval() {
        return alphaBreakpointLooseInterval;
    }

    public void setAlphaBreakpointLooseInterval(double alphaBreakpointLooseInterval) {
        this.alphaBreakpointLooseInterval = alphaBreakpointLooseInterval;
    }

    public double getDropBreakpointSupportInterval() {
        return dropBreakpointSupportInterval;
    }

    public void setDropBreakpointSupportInterval(double dropBreakpointSupportInterval) {
        this.dropBreakpointSupportInterval = dropBreakpointSupportInterval;
    }

    public double getAlphaFillGaps() {
        return alphaFillGaps;
    }

    public void setAlphaFillGaps(double alphaFillGaps) {
        this.alphaFillGaps = alphaFillGaps;
    }

    public double getErrorA() {
        return errorA;
    }

    public void setErrorA(double errorA) {
        this.errorA = errorA;
    }

    public double getErrorB() {
        return errorB;
    }

    public void setErrorB(double errorB) {
        this.errorB = errorB;
    }

    public int getBreakpointHalfWindowSize() {
        return breakpointHalfWindowSize;
    }

    public void setBreakpointHalfWindowSize(int breakpointHalfWindowSize) {
        this.breakpointHalfWindowSize = breakpointHalfWindowSize;
    }    

    public boolean isRecalculateErrorsBeforeStep3() {
        return recalculateErrorsBeforeStep3;
    }

    public void setRecalculateErrorsBeforeStep3(boolean recalculateErrorsBeforeStep3) {
        this.recalculateErrorsBeforeStep3 = recalculateErrorsBeforeStep3;
    }

    public boolean isUsePhysicalDistanceToFindDE() {
        return usePhysicalDistanceToFindDE;
    }

    public void setUsePhysicalDistanceToFindDE(boolean usePhysicalDistanceToFindDE) {
        this.usePhysicalDistanceToFindDE = usePhysicalDistanceToFindDE;
    }

    public boolean isUsePhysicalDistanceToFindB() {
        return usePhysicalDistanceToFindB;
    }

    public void setUsePhysicalDistanceToFindB(boolean usePhysicalDistanceToFindB) {
        this.usePhysicalDistanceToFindB = usePhysicalDistanceToFindB;
    }

    public boolean isImputeStep1() {
        return imputeStep1;
    }

    public void setImputeStep1(boolean imputeStep1) {
        this.imputeStep1 = imputeStep1;
    }

    public boolean isUseJointProbas() {
        return useJointProbas;
    }

    public void setUseJointProbas(boolean useJointProbas) {
        this.useJointProbas = useJointProbas;
    }

    public boolean isImputeStep2() {
        return imputeStep2;
    }

    public void setImputeStep2(boolean imputeStep2) {
        this.imputeStep2 = imputeStep2;
    }

    public boolean isImputeStep3() {
        return imputeStep3;
    }

    public void setImputeStep3(boolean imputeStep3) {
        this.imputeStep3 = imputeStep3;
    }

    public double getChunkEnvironmentMultiplier() {
        return chunkEnvironmentMultiplier;
    }

    public void setChunkEnvironmentMultiplier(double chunkEnvironmentMultiplier) {
        this.chunkEnvironmentMultiplier = chunkEnvironmentMultiplier;
    }

    public int getBreakpointProbaCalcMethod() {
        return breakpointProbaCalcMethod;
    }

    public void setBreakpointProbaCalcMethod(int breakpointProbaCalcMethod) {
        this.breakpointProbaCalcMethod = breakpointProbaCalcMethod;
    }

    public List<String> getExcludedRegions() {
        return excludedRegions;
    }

    public void setExcludedRegions(List<String> excludedRegions) {
        this.excludedRegions = excludedRegions;
    }

    public String getRegionsFile() {
        return regionsFile;
    }

    public void setRegionsFile(String regionsFile) {
        this.regionsFile = regionsFile;
    }

    public String getExcludedRegionsFile() {
        return excludedRegionsFile;
    }

    public void setExcludedRegionsFile(String excludedRegionsFile) {
        this.excludedRegionsFile = excludedRegionsFile;
    } 

    public boolean isNewAlienMethod() {
        return newAlienMethod;
    }

    public void setNewAlienMethod(boolean newAlienMethod) {
        this.newAlienMethod = newAlienMethod;
    }

    public int getTransitionNbMinForHS() {
        return transitionsNbMinForHS;
    }

    public void setTransitionNbMinForHS(int transitionNbMinForHS) {
        this.transitionsNbMinForHS = transitionNbMinForHS;
    }     

    public int getTransitionsNbMinForHS() {
        return transitionsNbMinForHS;
    }

    public void setTransitionsNbMinForHS(int transitionsNbMinForHS) {
        this.transitionsNbMinForHS = transitionsNbMinForHS;
    }

    public double getMinInflationcM() {
        return minInflationcM;
    }

    public void setMinInflationcM(double minInflationcM) {
        this.minInflationcM = minInflationcM;
    }

    public double getMinInflationPercent() {
        return minInflationPercent;
    }

    public void setMinInflationPercent(double minInflationPercent) {
        this.minInflationPercent = minInflationPercent;
    }
    
    
    
    public Map<String, List<long[]>> getRegionsMap() throws FileNotFoundException, IOException {
        Map<String, List<long[]>> map = new HashMap<>();
        if (!regions.isEmpty()) { //reading regions          
            for (String region : regions) {
                try {
                    String[] arrayStr = region.split(":");
                    String chr = arrayStr[0];
                    if (map.get(chr)== null) {
                        map.put(chr, new ArrayList());
                    }
                    if (arrayStr.length > 1) {
                        long[] positions = new long[2];
                        String[] str = arrayStr[1].split("-");
                        if (arrayStr[1].startsWith("-")) {
                            positions[0] = 0;
                            positions[1] = Long.parseLong(arrayStr[1].split("-")[1]);
                        } else if (arrayStr[1].endsWith("-")) {
                            positions[0] = Long.parseLong(arrayStr[1].split("-")[0]);
                            positions[1] = 0;
                        } else {
                            positions = Arrays.stream(arrayStr[1].split("-")).mapToLong(Long::parseLong).toArray();                
                        }
                        map.get(chr).add(positions);
                    }   
                } catch (Exception e) {
                    System.out.println("ERROR - can't read region " + region);
                }
                               
            }            
        } 
        
        if (!regionsFile.isEmpty()) {
            BufferedReader reader = new BufferedReader(new FileReader(Paths.get(regionsFile).toString()));
            String line;
            while ((line = reader.readLine()) != null) {
                String[] l = line.split("\t");
                String chr = l[0];
                long[] positions = {Long.parseLong(l[1]), Long.parseLong(l[2])};
                if (map.get(chr) == null) {
                    map.put(chr, new ArrayList<>());
                }
                map.get(chr).add(positions);
            }            
        }
        return map;
    }
    
    public Map<String, List<long[]>> getExcludedRegionsMap() throws FileNotFoundException, IOException {
        Map<String, List<long[]>> map = new HashMap<>();
        if (!excludedRegions.isEmpty()) { //reading regions          
            for (String region : excludedRegions) {
                String[] arrayStr = region.split(":");
                String chr = arrayStr[0];
                if (map.get(chr)== null) {
                    map.put(chr, new ArrayList());
                }
                if (arrayStr.length > 1) {
                    long[] positions = Arrays.stream(arrayStr[1].split("-")).mapToLong(Long::parseLong).toArray();                     
                    map.get(chr).add(positions);
                }                
                               
            }            
        }
        
        if (!excludedRegionsFile.isEmpty()) {
            BufferedReader reader = new BufferedReader(new FileReader(Paths.get(excludedRegionsFile).toString()));
            String line;
            while ((line = reader.readLine()) != null) {
                String[] l = line.split("\t");
                String chr = l[0];
                long[] positions = {Long.parseLong(l[1]), Long.parseLong(l[2])};
                if (map.get(chr) == null) {
                    map.put(chr, new ArrayList<>());
                }
                map.get(chr).add(positions);
            }            
        }
        return map;
    }
    
}
