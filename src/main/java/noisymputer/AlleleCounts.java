/*******************************************************************************
 * NOISYmputer
 * Distributed under the GNU Affero General Public License V3.0
 * See <https://www.gnu.org/licenses/agpl-3.0.en.html> for details
 ******************************************************************************/
package noisymputer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import static noisymputer.AllDataInBytes.genHTZ;
import static noisymputer.AllDataInBytes.genMIS;
import static noisymputer.AllDataInBytes.genPA1;
import static noisymputer.AllDataInBytes.genPA2;

/**
 *
 * @author boizet
 */
public class AlleleCounts {
    int nbA;
    int nbB;
    int nbH;
    int nbMD;

    public AlleleCounts() {
        nbA = 0;
        nbB = 0;
        nbH = 0;
        nbMD = 0;
    }

    public AlleleCounts(int nbA, int nbB, int nbH, int nbMD) {
        this.nbA = nbA;
        this.nbB = nbB;
        this.nbH = nbH;
        this.nbMD = nbMD;
    }
    
    public AlleleCounts(byte[] snp) {
        this.nbA = 0;
        this.nbB = 0;
        this.nbH = 0;
        this.nbMD = 0;
        for (int s = 0; s < snp.length; s++) {
            byte myAllele = snp[s];
            switch(myAllele) {
                case genPA1:
                    nbA++;                                       
                    break;
                case genPA2:
                    nbB++;
                    break;
                case genHTZ:
                    nbH++;
                    break;
                case genMIS:
                    nbMD++;
            }
        }
    }
    
    //used to copy
    public AlleleCounts(AlleleCounts ac) {
        this.nbA = ac.getNbA();
        this.nbB = ac.getNbB();
        this.nbH = ac.getNbH();
        this.nbMD = ac.getNbMD();
    }

    public int getNbA() {
        return nbA;
    }

    public void setNbA(int nbA) {
        this.nbA = nbA;
    }

    public int getNbB() {
        return nbB;
    }

    public void setNbB(int nbB) {
        this.nbB = nbB;
    }

    public int getNbH() {
        return nbH;
    }

    public void setNbH(int nbH) {
        this.nbH = nbH;
    }

    public int getNbMD() {
        return nbMD;
    }

    public void setNbMD(int nbMD) {
        this.nbMD = nbMD;
    }
    
    @JsonIgnore
    public int getTotalCount() {
        return nbA + nbB + nbH + nbMD;
    }

    public void updateWindow(AlleleCounts removedAllCnt, AlleleCounts newAllCnt) {
        if (newAllCnt != null) {
            this.nbA = this.nbA + newAllCnt.getNbA() - removedAllCnt.getNbA();
            this.nbB = this.nbB + newAllCnt.getNbB() - removedAllCnt.getNbB();
            this.nbH = this.nbH + newAllCnt.getNbH() - removedAllCnt.getNbH();
            this.nbMD = this.nbMD + newAllCnt.getNbMD() - removedAllCnt.getNbMD();
        } else {
            this.nbA = 0;
            this.nbB = 0;
            this.nbH = 0;
            this.nbMD = 0;
        }
           
    }
    
    public void updateAfterFillingMD(byte newAllele) {
        if (newAllele == genPA1) {
            this.nbMD--;
            this.nbA++;
        } else if (newAllele == genPA2) {
            this.nbMD--;
            this.nbB++;
        } else if (newAllele == genHTZ) {
            this.nbMD--;
            this.nbH++;
        }
        
    }
    
    public void changeHtoA() {
        this.nbH--;
        this.nbA++;
    }
    
    public void changeHtoB() {
        this.nbH--;
        this.nbB++;
    }  

    public void update(byte allele, byte newAllele) {
        if (allele == genPA1) {
            if (newAllele == genPA2) {
                this.nbA--;
                this.nbB++;
            } else if (newAllele == genHTZ) {
                this.nbA--;
                this.nbH++;
            } else if (newAllele == genMIS) {
                this.nbA--;
                this.nbMD++;
            }
        } else if (allele == genPA2) {
            if (newAllele == genPA1) {
                this.nbB--;
                this.nbA++;
            } else if (newAllele == genHTZ) {
                this.nbB--;
                this.nbH++;
            } else if (newAllele == genMIS) {
                this.nbB--;
                this.nbMD++;
            }        
            
        } else if (allele == genHTZ) {
            if (newAllele == genPA1) {
                this.nbH--;
                this.nbA++;
            } else if (newAllele == genPA2) {
                this.nbH--;
                this.nbB++;
            } else if (newAllele == genMIS) {
                this.nbH--;
                this.nbMD++;
            }
            
        } else if (allele == genMIS) {
            if (newAllele == genPA1) {
                this.nbMD--;
                this.nbA++;
            } else if (newAllele == genPA2) {
                this.nbMD--;
                this.nbB++;
            } else if (newAllele == genHTZ) {
                this.nbMD--;
                this.nbH++;
            }
            
        }
    }
    
    @Override
    public Object clone() throws CloneNotSupportedException {
        try {
            return (AlleleCounts) super.clone();
        } catch (CloneNotSupportedException e) {
            return new AlleleCounts(nbA, nbB, nbH, nbMD);
                  
        }
    }
    
}
