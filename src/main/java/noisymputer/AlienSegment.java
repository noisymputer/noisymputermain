/*******************************************************************************
 * NOISYmputer
 * Distributed under the GNU Affero General Public License V3.0
 * See <https://www.gnu.org/licenses/agpl-3.0.en.html> for details
 ******************************************************************************/
package noisymputer;

/**
 *
 * @author boizet
 */
public class AlienSegment {
    private String chr;
    private int start;
    private long startPosition;
    private int stop;
    private long stopPosition;
    private int highSlopeSNP;
    private int offset;    
    private int HS1; 
    private int HS2;
    private double minRF;
    
    public AlienSegment(String chr, int start, long startPosition, int stop, long stopPosition, int highSlopeSNP, int offset, int HS1, int HS2, double minRF) {
        this.chr = chr;
        this.start = start;
        this.startPosition = startPosition;
        this.stop = stop;
        this.stopPosition = stopPosition;
        this.offset = offset;
        this.highSlopeSNP = highSlopeSNP;
        this.HS1 = HS1;
        this.HS2 = HS2;
        this.minRF = minRF;
    }    

    public String getChr() {
        return chr;
    }

    public void setChr(String chr) {
        this.chr = chr;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public long getStartPosition() {
        return startPosition;
    }

    public void setStartPosition(long startPosition) {
        this.startPosition = startPosition;
    }

    public int getStop() {
        return stop;
    }

    public void setStop(int stop) {
        this.stop = stop;
    }

    public long getStopPosition() {
        return stopPosition;
    }

    public void setStopPosition(long stopPosition) {
        this.stopPosition = stopPosition;
    }

    public int getHighSlopeSNP() {
        return highSlopeSNP;
    }

    public void setHighSlopeSNP(int highSlopeSNP) {
        this.highSlopeSNP = highSlopeSNP;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getHS1() {
        return HS1;
    }

    public void setHS1(int HS1) {
        this.HS1 = HS1;
    }

    public int getHS2() {
        return HS2;
    }

    public void setHS2(int HS2) {
        this.HS2 = HS2;
    }

    public double getMinRF() {
        return minRF;
    }

    public void setMinRF(double minRF) {
        this.minRF = minRF;
    } 
    
}
