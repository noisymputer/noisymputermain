/*******************************************************************************
 * NOISYmputer
 * Distributed under the GNU Affero General Public License V3.0
 * See <https://www.gnu.org/licenses/agpl-3.0.en.html> for details
 ******************************************************************************/
package noisymputer;


import java.util.List;

/**
 *
 * @author boizet
 */
public interface VariantWrapper {
    public String getName();
    
    public String getGenotype(String individual);
    
    public List<String> getSampleNames();
    
    public int getNSamples();
    
    public int getDP(String individual);

    public long getPosition();
}
