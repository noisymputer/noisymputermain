/*******************************************************************************
 * NOISYmputer
 * Distributed under the GNU Affero General Public License V3.0
 * See <https://www.gnu.org/licenses/agpl-3.0.en.html> for details
 ******************************************************************************/
package noisymputer;

/**
 *
 * @author boizet
 */
public class Zone {
    private byte genotype;    
    private int start;
    private int end;
    private long startPosition;
    private long endPosition;
    
    public Zone(byte genotype, int start, int end, long startPosition, long endPosition) {
        this.genotype = genotype;
        this.start = start;
        this.end = end;
        this.startPosition = startPosition;
        this.endPosition = endPosition;
    }    

    public byte getGenotype() {
        return genotype;
    }

    public void setGenotype(byte genotype) {
        this.genotype = genotype;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public long getStartPosition() {
        return startPosition;
    }

    public void setStartPosition(long startPosition) {
        this.startPosition = startPosition;
    }

    public long getEndPosition() {
        return endPosition;
    }

    public void setEndPosition(long endPosition) {
        this.endPosition = endPosition;
    }
    
}
