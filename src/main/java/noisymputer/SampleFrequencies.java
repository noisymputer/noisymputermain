/*******************************************************************************
 * NOISYmputer
 * Distributed under the GNU Affero General Public License V3.0
 * See <https://www.gnu.org/licenses/agpl-3.0.en.html> for details
 ******************************************************************************/
package noisymputer;

/**
 *
 * @author boizet
 */
public class SampleFrequencies {
    private double freqAinA = -1, freqBinA = -1, freqHinA = -1;
    private double minFreqAinA = -1, minFreqBinA = -1, minFreqHinA = -1;
    private double maxFreqAinA = -1, maxFreqBinA = -1, maxFreqHinA = -1;
    private double freqAinB = -1, freqBinB = -1, freqHinB = -1;
    private double minFreqAinB = -1, minFreqBinB = -1, minFreqHinB = -1;
    private double maxFreqAinB = -1, maxFreqBinB = -1, maxFreqHinB = -1;
    private double freqAinH = -1, freqBinH = -1, freqHinH = -1;
    private double minFreqAinH = -1, minFreqBinH = -1, minFreqHinH = -1;
    private double maxFreqAinH = -1, maxFreqBinH = -1, maxFreqHinH = -1;
    
    public double getFreqAinA() {
        return freqAinA;
    }

    public void setFreqAinA(double freqAinA) {
        this.freqAinA = freqAinA;
    }

    public double getFreqBinA() {
        return freqBinA;
    }

    public void setFreqBinA(double freqBinA) {
        this.freqBinA = freqBinA;
    }

    public double getFreqHinA() {
        return freqHinA;
    }

    public void setFreqHinA(double freqHinA) {
        this.freqHinA = freqHinA;
    }

    public double getMinFreqAinA() {
        return minFreqAinA;
    }

    public void setMinFreqAinA(double minFreqAinA) {
        this.minFreqAinA = minFreqAinA;
    }

    public double getMinFreqBinA() {
        return minFreqBinA;
    }

    public void setMinFreqBinA(double minFreqBinA) {
        this.minFreqBinA = minFreqBinA;
    }

    public double getMinFreqHinA() {
        return minFreqHinA;
    }

    public void setMinFreqHinA(double minFreqHinA) {
        this.minFreqHinA = minFreqHinA;
    }

    public double getMaxFreqAinA() {
        return maxFreqAinA;
    }

    public void setMaxFreqAinA(double maxFreqAinA) {
        this.maxFreqAinA = maxFreqAinA;
    }

    public double getMaxFreqBinA() {
        return maxFreqBinA;
    }

    public void setMaxFreqBinA(double maxFreqBinA) {
        this.maxFreqBinA = maxFreqBinA;
    }

    public double getMaxFreqHinA() {
        return maxFreqHinA;
    }

    public void setMaxFreqHinA(double maxFreqHinA) {
        this.maxFreqHinA = maxFreqHinA;
    }

    public double getFreqAinB() {
        return freqAinB;
    }

    public void setFreqAinB(double freqAinB) {
        this.freqAinB = freqAinB;
    }

    public double getFreqBinB() {
        return freqBinB;
    }

    public void setFreqBinB(double freqBinB) {
        this.freqBinB = freqBinB;
    }

    public double getFreqHinB() {
        return freqHinB;
    }

    public void setFreqHinB(double freqHinB) {
        this.freqHinB = freqHinB;
    }

    public double getMinFreqAinB() {
        return minFreqAinB;
    }

    public void setMinFreqAinB(double minFreqAinB) {
        this.minFreqAinB = minFreqAinB;
    }

    public double getMinFreqBinB() {
        return minFreqBinB;
    }

    public void setMinFreqBinB(double minFreqBinB) {
        this.minFreqBinB = minFreqBinB;
    }

    public double getMinFreqHinB() {
        return minFreqHinB;
    }

    public void setMinFreqHinB(double minFreqHinB) {
        this.minFreqHinB = minFreqHinB;
    }

    public double getMaxFreqAinB() {
        return maxFreqAinB;
    }

    public void setMaxFreqAinB(double maxFreqAinB) {
        this.maxFreqAinB = maxFreqAinB;
    }

    public double getMaxFreqBinB() {
        return maxFreqBinB;
    }

    public void setMaxFreqBinB(double maxFreqBinB) {
        this.maxFreqBinB = maxFreqBinB;
    }

    public double getMaxFreqHinB() {
        return maxFreqHinB;
    }

    public void setMaxFreqHinB(double maxFreqHinB) {
        this.maxFreqHinB = maxFreqHinB;
    }

    public double getFreqAinH() {
        return freqAinH;
    }

    public void setFreqAinH(double freqAinH) {
        this.freqAinH = freqAinH;
    }

    public double getFreqBinH() {
        return freqBinH;
    }

    public void setFreqBinH(double freqBinH) {
        this.freqBinH = freqBinH;
    }

    public double getFreqHinH() {
        return freqHinH;
    }

    public void setFreqHinH(double freqHinH) {
        this.freqHinH = freqHinH;
    }

    public double getMinFreqAinH() {
        return minFreqAinH;
    }

    public void setMinFreqAinH(double minFreqAinH) {
        this.minFreqAinH = minFreqAinH;
    }

    public double getMinFreqBinH() {
        return minFreqBinH;
    }

    public void setMinFreqBinH(double minFreqBinH) {
        this.minFreqBinH = minFreqBinH;
    }

    public double getMinFreqHinH() {
        return minFreqHinH;
    }

    public void setMinFreqHinH(double minFreqHinH) {
        this.minFreqHinH = minFreqHinH;
    }

    public double getMaxFreqAinH() {
        return maxFreqAinH;
    }

    public void setMaxFreqAinH(double maxFreqAinH) {
        this.maxFreqAinH = maxFreqAinH;
    }

    public double getMaxFreqBinH() {
        return maxFreqBinH;
    }

    public void setMaxFreqBinH(double maxFreqBinH) {
        this.maxFreqBinH = maxFreqBinH;
    }

    public double getMaxFreqHinH() {
        return maxFreqHinH;
    }

    public void setMaxFreqHinH(double maxFreqHinH) {
        this.maxFreqHinH = maxFreqHinH;
    }
    
}
