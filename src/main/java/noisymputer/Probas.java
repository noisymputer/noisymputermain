/*******************************************************************************
 * NOISYmputer
 * Distributed under the GNU Affero General Public License V3.0
 * See <https://www.gnu.org/licenses/agpl-3.0.en.html> for details
 ******************************************************************************/
package noisymputer;

/**
 *
 * @author boizet
 */
public class Probas {
    double[][] pA;
    double[][] pB;
    double[][] pH;

    public Probas() {
    }

    public double[][] getpA() {
        return pA;
    }

    public void setpA(double[][] pA) {
        this.pA = pA;
    }

    public double[][] getpB() {
        return pB;
    }

    public void setpB(double[][] pB) {
        this.pB = pB;
    }

    public double[][] getpH() {
        return pH;
    }

    public void setpH(double[][] pH) {
        this.pH = pH;
    }
    
    
}
