/*******************************************************************************
 * NOISYmputer
 * Distributed under the GNU Affero General Public License V3.0
 * See <https://www.gnu.org/licenses/agpl-3.0.en.html> for details
 ******************************************************************************/
package noisymputer;

/**
 *
 * @author boizet
 */
public class GeneticMap {
    private String chr;
    private String snpName;
    private long position;
    private double f1Rec;
    private double rf;
    private double cM;
    private double cMbp;
    private double slope;
    private double slope2;

    public GeneticMap(String chr, String snpName, long position, double f1Rec, double rf, double cM, double cMbp, double slope, double slope2) {
        this.chr = chr;
        this.snpName = snpName;
        this.position = position;
        this.f1Rec = f1Rec;
        this.rf = rf;
        this.cM = cM;
        this.cMbp = cMbp;
        this.slope = slope;
        this.slope2 = slope2;
    }

    public String getChr() {
        return chr;
    }

    public void setChr(String chr) {
        this.chr = chr;
    }

    public String getSnpName() {
        return snpName;
    }

    public void setSnpName(String snpName) {
        this.snpName = snpName;
    }

    public long getPosition() {
        return position;
    }

    public void setPosition(long position) {
        this.position = position;
    }

    public double getF1Rec() {
        return f1Rec;
    }

    public void setF1Rec(double f1Rec) {
        this.f1Rec = f1Rec;
    }

    public double getRf() {
        return rf;
    }

    public void setRf(double rf) {
        this.rf = rf;
    }

    public double getcM() {
        return cM;
    }

    public void setcM(double cM) {
        this.cM = cM;
    }

    public double getcMbp() {
        return cMbp;
    }

    public void setcMbp(double cMbp) {
        this.cMbp = cMbp;
    }

    public double getSlope() {
        return slope;
    }

    public void setSlope(double slope) {
        this.slope = slope;
    }

    public double getSlope2() {
        return slope2;
    }

    public void setSlope2(double slope2) {
        this.slope2 = slope2;
    }
    
}
