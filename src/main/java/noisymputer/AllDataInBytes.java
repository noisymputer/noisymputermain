/*******************************************************************************
 * NOISYmputer
 * Distributed under the GNU Affero General Public License V3.0
 * See <https://www.gnu.org/licenses/agpl-3.0.en.html> for details
 ******************************************************************************/
package noisymputer;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import noisymputer.Exceptions.WindowSizeException;
import noisymputer.Exceptions.EmptySnpListException;
import noisymputer.Exceptions.BreakpointWindowException;
import static java.lang.Math.abs;
import static java.lang.Math.log;
import static java.lang.Math.log10;
import static java.lang.Math.round;
import static java.lang.StrictMath.exp;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.IntStream;
import noisymputer.SnpSelectionCriteria.MappingFunction;
import static noisymputer.SnpSelectionCriteria.MappingFunction.HALDANE;
import static noisymputer.SnpSelectionCriteria.MappingFunction.KOSAMBI;
import static noisymputer.SnpSelectionCriteria.MappingFunction.NONE;
import noisymputer.SnpSelectionCriteria.PopType;
import static noisymputer.SnpSelectionCriteria.PopType.F2;
import static noisymputer.SnpSelectionCriteria.PopType.SSD;

/**
 * 
 * @author boizet
 */
public class AllDataInBytes {
    
    public static final byte genPA1 = 1;
    public static final byte genPA2 = 3;
    public static final byte genHTZ = 2;
    public static final byte genMIS = 9;
    
    private ArrayList<String> samplesNames;
    private Set<Integer> parentIndexes;
    
    private ArrayList<Snp> snpData;
    private ArrayList<Snp> snpDataBeforeImputation = null;
    private ArrayList<Snp> snpDataCollapsed = null;
    
    private Map<Integer, List<BreakPoint>> bkps;
    private ArrayList<Integer> samplesTransitionsNb;
    private ArrayList<GeneticMap> geneticMap;
    private Map<Integer, List<Chunk>> chunks;
    private ArrayList<AlienSegment> aliens;
    //private ArrayList<AlleleCounts[]> windowsFreq = null;
    private ArrayList<SampleFrequencies> samplesFreq = null;
    private Probas probaTable = new Probas();
    private Probas proba2 = new Probas();
    private int[][] wlNbReadsAarray;
    private int[][] wlNbReadsBarray;
    private int[][] wrNbReadsAarray;
    private int[][] wrNbReadsBarray;
    private List<List<Zone>> ABHzones;
    
    private double minFreqMD;
    private double maxFreqMD;
    private double minFreqA;
    private double maxFreqA;
    private double minFreqB;
    private double maxFreqB;
    private double minFreqH; 
    private double maxFreqH;
    
    private double errorA;
    private double errorB;
       
    private double probaAgivenA = 0;
    private double probaBgivenA = 0;
    private double probaAgivenB = 0;
    private double probaBgivenB = 0;
    private double probaAgivenH = 0;
    private double probaBgivenH = 0;
    
    public AllDataInBytes() {
        this.samplesNames = new ArrayList<>();   
        this.parentIndexes = new HashSet<>();
        this.snpData = new ArrayList();
    }
    
    public ArrayList<Snp> getSnpData() {
        return snpData;
    }

    public void setSnpData(ArrayList<Snp> snpData) {
        this.snpData = snpData;
    }

    public ArrayList<Snp> getSnpDataBeforeImputation() {
        return snpDataBeforeImputation;
    }

    public void setSnpDataBeforeImputation(ArrayList<Snp> snpDataBeforeImputation) {
        this.snpDataBeforeImputation = snpDataBeforeImputation;
    }

    public ArrayList<Snp> getSnpDataCollapsed() {
        return snpDataCollapsed;
    }

    public void setSnpDataCollapsed(ArrayList<Snp> snpDataCollapsed) {
        this.snpDataCollapsed = snpDataCollapsed;
    }

    public ArrayList<GeneticMap> getGeneticMap() {
        return geneticMap;
    }

    public void setGeneticMap(ArrayList<GeneticMap> geneticMap) {
        this.geneticMap = geneticMap;
    }
    
    public ArrayList<String> getSamplesNames() {
        return samplesNames;
    }

    public void setSamplesNames(ArrayList<String> samplesNames) {
        this.samplesNames = samplesNames;
    }

    public Map<Integer, List<BreakPoint>> getBkps() {
        return bkps;
    }

    public void setBkps(Map<Integer, List<BreakPoint>> bps) {
        this.bkps = bps;
    }

    public ArrayList<Integer> getSamplesTransitionsNb() {
        return samplesTransitionsNb;
    }

    public void setSamplesTransitionsNb(ArrayList<Integer> samplesTransitionsNb) {
        this.samplesTransitionsNb = samplesTransitionsNb;
    }

    public ArrayList<AlienSegment> getAliens() {
        return aliens;
    }

    public void setAliens(ArrayList<AlienSegment> aliens) {
        this.aliens = aliens;
    }

    public ArrayList<SampleFrequencies> getSamplesFreq() {
        return samplesFreq;
    }

    public void setSamplesFreq(ArrayList<SampleFrequencies> samplesFreq) {
        this.samplesFreq = samplesFreq;
    }    

    public Probas getProbas() {
        return probaTable;
    }

    public void setProbas(Probas probas) {
        this.probaTable = probas;
    }

    public List<List<Zone>> getABHzones() {
        return ABHzones;
    }

    public void setABHzones(List<List<Zone>> ABHzones) {
        this.ABHzones = ABHzones;
    }    

    public int[][] getWlNbReadsAarray() {
        return wlNbReadsAarray;
    }

    public void setWlNbReadsAarray(int[][] wlNbReadsAarray) {
        this.wlNbReadsAarray = wlNbReadsAarray;
    }

    public int[][] getWlNbReadsBarray() {
        return wlNbReadsBarray;
    }

    public void setWlNbReadsBarray(int[][] wlNbReadsBarray) {
        this.wlNbReadsBarray = wlNbReadsBarray;
    }

    public int[][] getWrNbReadsAarray() {
        return wrNbReadsAarray;
    }

    public void setWrNbReadsAarray(int[][] wrNbReadsAarray) {
        this.wrNbReadsAarray = wrNbReadsAarray;
    }

    public int[][] getWrNbReadsBarray() {
        return wrNbReadsBarray;
    }

    public void setWrNbReadsBarray(int[][] wrNbReadsBarray) {
        this.wrNbReadsBarray = wrNbReadsBarray;
    }

    public Map<Integer, List<Chunk>> getChunks() {
        return chunks;
    }

    public void setChunks(Map<Integer, List<Chunk>> chunks) {
        this.chunks = chunks;
    }

    public Set<Integer> getParentIndexes() {
        return parentIndexes;
    }

    public void setParentIndexes(Set<Integer> parentIndexes) {
        this.parentIndexes = parentIndexes;
    }

    public double getErrorA() {
        return errorA;
    }

    public void setErrorA(double errorA) {
        this.errorA = errorA;
    }

    public double getErrorB() {
        return errorB;
    }

    public void setErrorB(double errorB) {
        this.errorB = errorB;
    }
    
    /**
     * Will keep variants that are in the given regions and/or remove variants that are in the given excludedRegions.
     * @param chr
     * @param regions
     * @param excludedRegions 
     */
    public void filterVariantsOnRegions(String chr, List<long[]> regions, List<long[]> excludedRegions) {        
        if (regions != null) {
            if (regions.isEmpty()) { //only chromosome given
                //keep all chromosome
            } else {
                ArrayList<Snp> filteredSnpData= new ArrayList<>();
                //look for snps are inside the regions
                //using binary search algo
                for (long[] reg:regions) {
                    long start = reg[0] == 0 ? snpData.get(0).getPosition() : reg[0];
                    long end = reg[1] == 0 ? snpData.get(snpData.size()-1).getPosition() : reg[1];
                    int low = 0;
                    int high = snpData.size() - 1;
                    
                    int startIndex = 0;
                    while (low <= high) {
                        int mid = low + ((high - low) / 2);
                        if (snpData.get(mid).getPosition() < start) {
                            low = mid + 1;
                        } else if (snpData.get(mid).getPosition() > start) {
                            high = mid - 1;
                        } else if (snpData.get(mid).getPosition() == start) {
                            startIndex = mid;
                            break;
                        }
                        startIndex = mid;
                    }
                    
                    
                    low = startIndex;
                    high = snpData.size() - 1;
                    int endIndex = startIndex;
                    while (low <= high) {
                        int mid = low + ((high - low) / 2);
                        if (snpData.get(mid).getPosition() < end) {
                            low = mid + 1;
                        } else if (snpData.get(mid).getPosition() > end) {
                            high = mid - 1;
                        } else if (snpData.get(mid).getPosition() == end) {
                            endIndex = mid + 1;
                            break;
                        }
                        endIndex = mid + 1;
                    }
                    
                    filteredSnpData.addAll(snpData.subList(startIndex, endIndex));                    
                }
                snpData = filteredSnpData;                
            }            
            
        }
        
        if (excludedRegions != null) {
            if (excludedRegions.isEmpty()) { //only chromosome given
                //remove all chr
                snpData = new ArrayList<>();
            } else {
                ArrayList<Snp> filteredSnpData= new ArrayList<>();
                //look for snps are inside the regions
                //using binary search algo
                for (long[] reg:excludedRegions) {
                    long start = reg[0] == 0 ? snpData.get(0).getPosition() : reg[0];
                    long end = reg[1] == 0 ? snpData.get(snpData.size()-1).getPosition() : reg[1];
                    int low = 0;
                    int high = snpData.size() - 1;
                    
                    int startIndex = 0;
                    while (low <= high) {
                        int mid = low + ((high - low) / 2);
                        if (snpData.get(mid).getPosition() < start) {
                            low = mid + 1;
                        } else if (snpData.get(mid).getPosition() > start) {
                            high = mid - 1;
                        } else if (snpData.get(mid).getPosition() == start) {
                            startIndex = mid;
                            break;
                        }
                        startIndex = mid;
                    }
                    
                    
                    low = startIndex;
                    high = snpData.size() - 1;
                    int endIndex = startIndex;
                    while (low <= high) {
                        int mid = low + ((high - low) / 2);
                        if (snpData.get(mid).getPosition() < end) {
                            low = mid + 1;
                        } else if (snpData.get(mid).getPosition() > end) {
                            high = mid - 1;
                        } else if (snpData.get(mid).getPosition() == end) {
                            endIndex = mid + 1;
                            break;
                        }
                        endIndex = mid + 1;
                    }
                          
                    snpData.removeAll(snpData.subList(startIndex, endIndex));        
                }                
            }
        }
    }
    
    /**
     * Will remove SNP for which the total number of reads is not included in minReadsVariant and maxReadsVariant.
     * @param ssc 
     */
    public void filterOnReadsSNP(SnpSelectionCriteria ssc) {
        ArrayList<Snp> filteredSnps = new ArrayList<>();
        
        for (int v = 0; v < snpData.size(); v++) {
            int snpNbReads = 0;
            for (int s = 0; s < samplesNames.size(); s++) {
                int nbReads = snpData.get(v).getNbReadsA()[s] + snpData.get(v).getNbReadsB()[s];
                snpNbReads += nbReads;                          
            }                        
            if ( !((ssc.getMinReadsVariant() != -1 && snpNbReads < ssc.getMinReadsVariant()) || (ssc.getMaxReadsVariant() != -1 && snpNbReads > ssc.getMaxReadsVariant())) ) {
                filteredSnps.add(snpData.get(v));
            }
        }

        snpData = filteredSnps; 
        System.out.println("Filtering SNPs on reads: " + filteredSnps.size() + " SNPs were kept");
    }
    
    /**
     * Will remove sites for which the number of reads is not included in minReadsDP and maxReadsDP.
     * "Removing" here means that the site is set to missing data and the corresponding reads number are set to 0
     * @param ssc
     * @throws Exception 
     */
    public List<int[]> filterOnReadsDP(SnpSelectionCriteria ssc) throws Exception { 
        List<Integer> maxReadsPerSamples = null;
        if (ssc.getMaxReadsDP() == -1) {
            maxReadsPerSamples = calcMaxReadsPerSamples();
        }       
        
        int totalFilteredDP = 0;
        int total = 0;
        int totalDP = 0;
        List<int[]> filteredPerSample = new ArrayList<>();
        for (int s = 0; s < samplesNames.size(); s++) {
            int filteredDP = 0;
            int numberOfDP = 0;
            int numberOfSnps = 0;
            int minReads = ssc.getMinReadsDP() == -1 ? 1 : ssc.getMinReadsDP();
            int maxReads = ssc.getMaxReadsDP() == -1 ? maxReadsPerSamples.get(s) : ssc.getMaxReadsDP();
            for (int v = 0; v < snpData.size(); v++) {
                int nbReads = snpData.get(v).getNbReadsA()[s] + snpData.get(v).getNbReadsB()[s];                

                if (snpData.get(v).getGenotypes()[s] != genMIS) {
                    if (nbReads < minReads || nbReads > maxReads)  {
                        //System.out.println("sample " + samplesNames.get(s) + ", snp "+ snpData.get(v).getName() +" : " + minReads + " | " + maxReads + " | " + nbReads);
                        byte allele = snpData.get(v).getGenotypes()[s];
                        snpData.get(v).getGenotypes()[s] = genMIS;
                        snpData.get(v).getAlleleCounts().update(allele, genMIS);
                        snpData.get(v).getNbReadsA()[s] = 0;
                        snpData.get(v).getNbReadsB()[s] = 0;
                        filteredDP++;                    
                    }     
                    numberOfDP++;
                }
                numberOfSnps++;
                
            }
            int[] res = {filteredDP, numberOfDP, numberOfSnps};            
            filteredPerSample.add(res);
            totalFilteredDP = totalFilteredDP + filteredDP;
            total = total + numberOfSnps;
            totalDP = totalDP + numberOfDP;
            //System.out.println("sample " + samplesNames.get(s) + ": " + filteredDP);
        }        
        System.out.println("Filtering data points on reads: " + totalFilteredDP + " data points were set as missing data on a total of " + totalDP + " datapoints");    
        return filteredPerSample;
    }

    /**
     * Filter the SNPs on genotypic frequencies.
     * 1. Genotypic Frequencies of all SNPs are first calculated
     * 2. The filtered values can either be given by the user or are derived from the extreme percentiles of the frequency distribution
     * @param criterias
     * @throws EmptySnpListException 
     */
    public void filterSnpsOnGenotypicFrequencies(SnpSelectionCriteria criterias, boolean writeOutput) throws EmptySnpListException {

        //1.2 calculate percentiles to get min and max allele frequencies        
        int snpNb = snpData.size();
        double[] MDfrequencies = new double[snpNb];
        double[] Afrequencies = new double[snpNb];
        double[] Bfrequencies = new double[snpNb];
        double[] Hfrequencies = new double[snpNb];
        
        if (writeOutput) {
            System.out.println("1. Determine min and max frequencies"); 
            System.out.println("");
        }
        
        updateAlleleCounts(true);
        for (int i = 0; i < snpData.size(); i++) {
            AlleleCounts act = snpData.get(i).getAlleleCounts();
            
            MDfrequencies[i] = (double) act.getNbMD()/act.getTotalCount();            
            Afrequencies[i] = (double) act.getNbA()/act.getTotalCount();
            Bfrequencies[i] = (double) act.getNbB()/act.getTotalCount();
            Hfrequencies[i] = (double) act.getNbH()/act.getTotalCount();
        }
        
        minFreqMD = Functions.percentile(MDfrequencies, criterias.getLowPercentileMD());
        maxFreqMD = Functions.percentile(MDfrequencies, criterias.getHighPercentileMD());
        minFreqA = Functions.percentile(Afrequencies, criterias.getLowPercentileA());
        maxFreqA = Functions.percentile(Afrequencies, criterias.getHighPercentileA());
        minFreqB = Functions.percentile(Bfrequencies, criterias.getLowPercentileB());
        maxFreqB = Functions.percentile(Bfrequencies, criterias.getHighPercentileB());
        minFreqH = Functions.percentile(Hfrequencies, criterias.getLowPercentileH());
        maxFreqH = Functions.percentile(Hfrequencies, criterias.getHighPercentileH());
        
        if (writeOutput) {
            System.out.format("+------------------+--------------+--------------+--------------+--------------+%n");
            System.out.format("%18s | %12s | %12s | %12s | %12s | %n","SNP STATISTICS","Percent_MISS", "Percent_HTZ", "Percent_P1", "Percent_P2");
            System.out.format("+------------------+--------------+--------------+--------------+--------------+%n");
            System.out.format("%18s | %12s | %12s | %12s | %12s | %n", "Low percentile", criterias.getLowPercentileMD(), criterias.getLowPercentileH(), criterias.getLowPercentileA(), criterias.getLowPercentileB());
            System.out.format("%18s | %12s | %12s | %12s | %12s | %n", "High percentile", criterias.getHighPercentileMD(), criterias.getHighPercentileH(), criterias.getHighPercentileA(), criterias.getHighPercentileB());
            System.out.format("%18s | %12s | %12s | %12s | %12s | %n", "Percentile (low)", String.format(Locale.US, "%.3f", minFreqMD), String.format(Locale.US, "%.3f", minFreqH), String.format(Locale.US, "%.3f", minFreqA), String.format(Locale.US, "%.3f", minFreqB));
            System.out.format("%18s | %12s | %12s | %12s | %12s | %n", "Percentile (high)", String.format(Locale.US, "%.3f", maxFreqMD), String.format(Locale.US, "%.3f", maxFreqH), String.format(Locale.US, "%.3f", maxFreqA), String.format(Locale.US, "%.3f", maxFreqB));
        }
        minFreqMD = minFreqMD < criterias.getCorrectMinPercentileMD() ? criterias.getCorrectMinPercentileMD() : minFreqMD;        
        maxFreqMD = maxFreqMD > criterias.getCorrectMaxPercentileMD() ? criterias.getCorrectMaxPercentileMD() : maxFreqMD;       
        minFreqA = minFreqA < criterias.getCorrectMinPercentileA() ? criterias.getCorrectMinPercentileA() : minFreqA;
        maxFreqA = maxFreqA > criterias.getCorrectMaxPercentileA() ? criterias.getCorrectMaxPercentileA() : maxFreqA;
        minFreqB = minFreqB < criterias.getCorrectMinPercentileB() ? criterias.getCorrectMinPercentileB() : minFreqB;
        maxFreqB = maxFreqB > criterias.getCorrectMaxPercentileB() ? criterias.getCorrectMaxPercentileB() : maxFreqB;
        if (!criterias.getPopType().equals(PopType.SSD)) {
            minFreqH = minFreqH < criterias.getCorrectMinPercentileH() ? criterias.getCorrectMinPercentileH() : minFreqH;
        } 
        maxFreqH = maxFreqH > criterias.getCorrectMaxPercentileH() ? criterias.getCorrectMaxPercentileH() : maxFreqH; 
        
        if (criterias.getMinFreqA() != -1) {
            minFreqA = criterias.getMinFreqA();
        }
        if (criterias.getMaxFreqA() != -1) {
            maxFreqA = criterias.getMaxFreqA();
        }
        if (criterias.getMinFreqB() != -1) {
            minFreqB = criterias.getMinFreqB();
        }
        if (criterias.getMaxFreqB() != -1) {
            maxFreqB = criterias.getMaxFreqB();
        }
        if (criterias.getMinFreqH() != -1) {
            minFreqH = criterias.getMinFreqH();
        }
        if (criterias.getMaxFreqH() != -1) {
            maxFreqH = criterias.getMaxFreqH();
        }
        if (criterias.getMinFreqMD() != -1) {
            minFreqMD = criterias.getMinFreqMD();
        }
        if (criterias.getMaxFreqMD() != -1) {
            maxFreqMD = criterias.getMaxFreqMD();
        } 
        
        if (writeOutput) {
            System.out.println("Frequencies used for filtering :");        
            System.out.format("+----------+-----------------------+%n");
            System.out.format("%10s | %21s | %n","minFreqMD",minFreqMD);
            System.out.format("%10s | %21s | %n","maxFreqMD",maxFreqMD);
            System.out.format("%10s | %21s | %n","minFreqA",minFreqA);
            System.out.format("%10s | %21s | %n","maxFreqA",maxFreqA);
            System.out.format("%10s | %21s | %n","minFreqB",minFreqB);
            System.out.format("%10s | %21s | %n","maxFreqB",maxFreqB);
            System.out.format("%10s | %21s | %n","minFreqH",minFreqH);
            System.out.format("%10s | %21s | %n","maxFreqH",maxFreqH);
            System.out.format("+----------+-----------------------+%n");
        }
        
        if (writeOutput) {
            System.out.println(" "); 
            System.out.println("2. Filtering snps"); 
        }

        ArrayList<Snp> filteredSnps = new ArrayList<>();
        
        for (int v = 0; v < snpData.size(); v++) {
            double freqA = Afrequencies[v];
            double freqB = Bfrequencies[v];
            double freqH = Hfrequencies[v];
            double freqMD = MDfrequencies[v];
            
            if (freqMD >= minFreqMD && freqMD <= maxFreqMD
                    && freqH >= minFreqH && freqH <= maxFreqH
                    && freqA >= minFreqA && freqA <= maxFreqA
                    && freqB >= minFreqB && freqB <= maxFreqB) {
                filteredSnps.add(snpData.get(v));
            }
        }
        
        if (filteredSnps.isEmpty()) {
            throw new EmptySnpListException();
        }
        if (writeOutput) {
            System.out.println(snpData.size() - filteredSnps.size() + " excluded SNPs");
            System.out.println(filteredSnps.size() + " kept SNPs");
        }
                
        snpData = filteredSnps; 
    }
    
    /**
     * Will remove completely linked loci that contain the same genotypic information
     */
    public void filterOutRedundant() {
        ArrayList<Snp> nonRedundantSnps = new ArrayList<>();
        
        boolean[] keptSnps = new boolean[snpData.size()];
        keptSnps[0] = true; //first marker declared unique
        byte memoryMK;
        
        for (int s = 0; s < samplesNames.size(); s++) {
            memoryMK = snpData.get(0).getGenotypes()[s];
            for (int v = 1; v < snpData.size(); v++) {
                if (snpData.get(v).getGenotypes()[s] != genMIS && snpData.get(v).getGenotypes()[s] != memoryMK) {
                    if (!keptSnps[v]) {
                        keptSnps[v] = true;
                    }
                    memoryMK = snpData.get(v).getGenotypes()[s];
                }
                v++;
            }
            
        }
        
        for (int v = 0; v < keptSnps.length; v++ ) {
            if (keptSnps[v]) {
                nonRedundantSnps.add(snpData.get(v));
            }
        }
        
        System.out.println(snpData.size() - nonRedundantSnps.size() + " excluded SNPs");
        System.out.println(nonRedundantSnps.size() + " kept SNPs");

        snpData = nonRedundantSnps;

    }
    
    /**
     * Correct obvious erroneous heterozygote calls before filling.
     * @throws WindowSizeException
     * @throws CloneNotSupportedException 
     */
    public void correctErroneousHTZ() throws WindowSizeException, CloneNotSupportedException {
        int windowSize = 7;
        if (windowSize*2+1 > snpData.size()) {
            throw new WindowSizeException(windowSize, snpData.size());            
        }
        FreqDoubleSlidingWindow slidingWindow = new FreqDoubleSlidingWindow(windowSize, samplesNames.size());
        long nbSNPImputed = 0;
        ArrayList<Snp> correctedSNPs = new ArrayList<>();
        
        for (int i = 0; i < snpData.size() + windowSize; i++) {
        
            if (i < snpData.size()) {
                slidingWindow.put(snpData.get(i).getGenotypes(), i);
            } else {
                slidingWindow.put(null, i);
            }
            
            if (slidingWindow.rightWindow.size() == windowSize || slidingWindow.leftWindow.size() == windowSize) {
                byte[] snp = Arrays.copyOf(slidingWindow.midSnp, slidingWindow.midSnp.length);
                
                for (int j = 0; j < slidingWindow.leftAlleleCountBySamples.length; j++) {
                    int Nwindow = slidingWindow.leftAlleleCountBySamples[j].getTotalCount() + slidingWindow.rightAlleleCountBySamples[j].getTotalCount() + 1;
                    int wNbA = slidingWindow.leftAlleleCountBySamples[j].getNbA() + slidingWindow.rightAlleleCountBySamples[j].getNbA();
                    int wNbB = slidingWindow.leftAlleleCountBySamples[j].getNbB() + slidingWindow.rightAlleleCountBySamples[j].getNbB();
                    byte myAllele = snp[j];
                    switch (myAllele) {
                        case genPA1:
                            wNbA++;
                            break;
                        case genPA2:
                            wNbB++;
                            break;
                    }

                    if (Nwindow != 0) { // Nwindow == 0 can happen when many consecutive missing data
                        //CORRECT OBVIOUS ERRONEOUS HETEROZYGOTE CALLS
                        if (wNbA >= Nwindow -1 && wNbA > 4 && myAllele == genHTZ) {
                            snp[j] = genPA1;
                            snpData.get(slidingWindow.midSnpIndex).getAlleleCounts().changeHtoA();   //alleleCounts updates don't impact the existing data used in sliding window
                            nbSNPImputed++;
                        } else if (wNbB >= Nwindow -1 && wNbB > 4 && myAllele == genHTZ) {
                            snp[j] = genPA2;
                            snpData.get(slidingWindow.midSnpIndex).getAlleleCounts().changeHtoB();
                            nbSNPImputed++;
                        }
                        
                    }
                }
                Snp newSnpData = (Snp) snpData.get(i).clone();
                newSnpData.setGenotypes(snp);
                correctedSNPs.add(newSnpData);
                
            }
            
        }
        
        System.out.println(nbSNPImputed + " HTZ calls corrected");

        snpData = correctedSNPs;
    }
    
    
    /** 
     * Fill missing data between 2 chunks of the same genotype with this genotype.
     * @param message to print in the console
     */
    public void fillingMissingData(String message) {        
        long nbSNPImputed = 0;
        for (int i = 0; i < samplesNames.size(); i++) {
            for (int snp = 1; snp < snpData.size() - 1; snp++) { 
                byte allele = snpData.get(snp).getGenotypes()[i];
                byte previousAllele = snpData.get(snp-1).getGenotypes()[i];
                if (allele == genMIS && previousAllele != genMIS) {
                    for (int snp2 = snp + 1; snp2 < snpData.size(); snp2++) {
                        byte allele2 = snpData.get(snp2).getGenotypes()[i];
                        if (allele2 != genMIS) {
                            if (allele2 == previousAllele) {
                                for (int snp3 = snp; snp3 < snp2; snp3++) {
                                    snpData.get(snp3).getGenotypes()[i] = previousAllele;
                                    snpData.get(snp3).getAlleleCounts().updateAfterFillingMD(previousAllele);
                                    nbSNPImputed++;
                                }
                                snp = snp2; //iterate after snp2                                
                            }
                            break;
                        }
                    }
                }
            }
        }
        System.out.println(message + nbSNPImputed + " filled missing data");
    }
    
    
    /**
     * Calculate chi2 on Genotype frequencies (number of AA, number of BB, number of AB in the population in a SNPs windows).
     * @param snpAlleleCounts
     * @param wNbA
     * @param wNbB
     * @param wNbH
     * @param popType
     * @param otherPhase
     * @return 
     */ 
    private double calcChi2onGenotypeFrequencies(long[] snpAlleleCounts, long wNbA, long wNbB, long wNbH, PopType popType, boolean otherPhase) {
        long wNpop = wNbA + wNbB + wNbH;
        long Npop = snpAlleleCounts[0] + snpAlleleCounts[1] + snpAlleleCounts[2];
        double chi2SNP;
        if (!otherPhase) { 
            if (popType.equals(SnpSelectionCriteria.PopType.SSD) || popType.equals(SnpSelectionCriteria.PopType.DH) ) {
                chi2SNP = Math.pow((snpAlleleCounts[0] - wNbA*Npop/(double) wNpop), 2)/(wNbA*Npop/(double) wNpop)
                        + Math.pow((snpAlleleCounts[1] - wNbB*Npop/(double) wNpop), 2)/(wNbB*Npop/(double) wNpop);
            } else {
                chi2SNP = Math.pow((snpAlleleCounts[0] - wNbA*Npop/(double) wNpop), 2)/(wNbA*Npop/(double) wNpop)
                        + Math.pow((snpAlleleCounts[1] - wNbB*Npop/(double) wNpop), 2)/(wNbB*Npop/(double) wNpop)
                        + Math.pow((snpAlleleCounts[2] - wNbH*Npop/(double) wNpop), 2)/(wNbH*Npop/(double) wNpop);                        
            }
        } else { //try to save the SNP calculating the opposite chi2 like if the SNP was recoded (ABH- to BHA-) before marking it as incoherent
            if (popType.equals(SnpSelectionCriteria.PopType.SSD) || popType.equals(SnpSelectionCriteria.PopType.DH) ) {
                chi2SNP = Math.pow((snpAlleleCounts[1] - wNbA*Npop/(double) wNpop), 2)/(wNbA*Npop/(double) wNpop)
                        + Math.pow((snpAlleleCounts[0] - wNbB*Npop/(double) wNpop), 2)/(wNbB*Npop/(double) wNpop);
            } else {
                chi2SNP = Math.pow((snpAlleleCounts[1] - wNbA*Npop/(double) wNpop), 2)/(wNbA*Npop/(double) wNpop)
                        + Math.pow((snpAlleleCounts[0] - wNbB*Npop/(double) wNpop), 2)/(wNbB*Npop/(double) wNpop)
                        + Math.pow((snpAlleleCounts[2] - wNbH*Npop/(double) wNpop), 2)/(wNbH*Npop/(double) wNpop);                        
            }
        }
        return chi2SNP;
    }
    
    /**
     * Calculate chi2 on reads frequencies (number of reads A, number of reads B in the population in a SNPs window)
     * @param snpReadsCounts
     * @param wNbReadsA
     * @param wNbReadsB
     * @return 
     */
    private double calcChi2onReadsFrequencies(long[] snpReadsCounts, long wNbReadsA, long wNbReadsB) {
        long wNpop = wNbReadsA + wNbReadsB;
        long Npop = snpReadsCounts[0] + snpReadsCounts[1];
        double chi2SNP = Math.pow((snpReadsCounts[0] - wNbReadsA*Npop/(double) wNpop), 2)/(wNbReadsA*Npop/(double) wNpop)
                    + Math.pow((snpReadsCounts[1] - wNbReadsB*Npop/(double) wNpop), 2)/(wNbReadsB*Npop/(double) wNpop);
        return chi2SNP;
    }
    
    /**
     * Remove incoherent SNPs.
     * We define a window of n SNPs before and after the SNP
     * For each window/SNP couple, it calculates the A, B and H frequencies across the population
     * from the genotypes called in the VCF and compares the SNP and the window segregations using a chi-squared test
     * It also calculates reads A and B frequencies across the population for each window/SNP couple
     * if one of two chi-squared tests is significant, then the SNP is said incoherent
     * @param ssc
     * @throws WindowSizeException 
     */
    public void detectIncoherentSNPs(SnpSelectionCriteria ssc) throws WindowSizeException {    
        int windowSize = ssc.getIncoherentHalfWindowSize();
        
        System.out.println("windowSize=" + windowSize);
        
        double chi2thresholdGenotypes = Functions.getChi2Threshold(2, ssc.getChi2alpha());
        double chi2thresholdReads =  Functions.getChi2Threshold(1, ssc.getChi2alpha());
        System.out.println("chi2thresholdGenotypes=" + chi2thresholdGenotypes);
        System.out.println("chi2thresholdReads=" + chi2thresholdReads);
        boolean tryOtherPhase = ssc.isTryOtherPhase();
        PopType popType = ssc.getPopType();
        
        ArrayList<Snp> coherentSNPs = new ArrayList();
        
        if (windowSize*2+1 > snpData.size()) {
            throw new WindowSizeException(windowSize, snpData.size());            
        }
        //FreqDoubleSlidingWindow slidingWindow = new FreqDoubleSlidingWindow(windowSize, samplesNames.size());
        
        SumLongSlidingWindow leftWinAlleleCount = new SumLongSlidingWindow(windowSize, 3);
        SumLongSlidingWindow rightWinAlleleCount = new SumLongSlidingWindow(windowSize, 3);
        SumLongSlidingWindow leftWinReadsCount= new SumLongSlidingWindow(windowSize, 2);
        SumLongSlidingWindow rightWinReadsCount = new SumLongSlidingWindow(windowSize, 2);
        long[] midAlleleCount = null;
        long[] midReadsCount = null;
        int midEltIndex = 0;
        
        
        for (int v = 0; v < snpData.size() + windowSize; v++) {            
            long[] eltAlleleCounts = null;
            long[] eltReadsCount =  null;
            if (v < snpData.size()) {
                AlleleCounts alleleCounts = new AlleleCounts(snpData.get(v).getGenotypes());
                ReadsCount readsCount = new ReadsCount(snpData.get(v));
                eltAlleleCounts = new long[3];
                eltAlleleCounts[0] = alleleCounts.getNbA();
                eltAlleleCounts[1] = alleleCounts.getNbB();
                eltAlleleCounts[2] = alleleCounts.getNbH();
                eltReadsCount = new long[2];
                eltReadsCount[0] = readsCount.getNbReadsA(); 
                eltReadsCount[1] = readsCount.getNbReadsB(); 
            }
            if (v == 0) { //initiate
                midAlleleCount = eltAlleleCounts;
                midReadsCount = eltReadsCount;
                midEltIndex = v;
            } else if (v <= windowSize) { //fill right window
                rightWinAlleleCount.put(eltAlleleCounts); 
                rightWinReadsCount.put(eltReadsCount); 
            } else { //right window is full, so begin to calc
                long wNbA = leftWinAlleleCount.getSum()[0] + rightWinAlleleCount.getSum()[0];
                long wNbB = leftWinAlleleCount.getSum()[1] + rightWinAlleleCount.getSum()[1];
                long wNbH = leftWinAlleleCount.getSum()[2] + rightWinAlleleCount.getSum()[2];
                long Npop = midAlleleCount[0] + midAlleleCount[1] + midAlleleCount[2];

                long wNbReadsA = leftWinReadsCount.getSum()[0] + rightWinReadsCount.getSum()[0];
                long wNbReadsB = leftWinReadsCount.getSum()[1] + rightWinReadsCount.getSum()[1];
                long readsNpop = midReadsCount[0] + midReadsCount[1];
                
                

                if (Npop > 0 && readsNpop > 0) {
                    double chi2onGenotypes = calcChi2onGenotypeFrequencies(midAlleleCount, wNbA, wNbB, wNbH, popType, false);
                    double chi2onReads = calcChi2onReadsFrequencies(midReadsCount, wNbReadsA, wNbReadsB);

                    if (chi2onGenotypes > chi2thresholdGenotypes || chi2onReads > chi2thresholdReads) {  //significant 
                        if (tryOtherPhase) { //try to save the SNP calculating the opposite chi2 like if the SNP was recoded (ABH- to BHA-) before marking it as incoherent
                            chi2onGenotypes = calcChi2onGenotypeFrequencies(midAlleleCount, wNbA, wNbB, wNbH, popType, true); 

                            if (chi2onGenotypes < chi2thresholdGenotypes) { //the Chi2 is not significant, we keep the SNP after recoding it
                                for (int s = 0; s < snpData.get(midEltIndex).getGenotypes().length; s++) {
                                    if (snpData.get(midEltIndex).getGenotypes()[s] == genPA1) {
                                        snpData.get(midEltIndex).getGenotypes()[s] = genPA2;
                                        //snpData.get(midEltIndex).getAlleleCounts().update(genPA1, genPA2);
                                    } else if (snpData.get(midEltIndex).getGenotypes()[s] == genPA2) {
                                        snpData.get(midEltIndex).getGenotypes()[s] = genPA1;
                                        //snpData.get(midEltIndex).getAlleleCounts().update(genPA2, genPA1);
                                    }
                                }
                                coherentSNPs.add(snpData.get(midEltIndex));
                                //System.out.println(midEltIndex);

                            }
                        }
                    } else { //not significant, we keep the snp                
                        coherentSNPs.add(snpData.get(midEltIndex));
                        //System.out.println(midEltIndex);
                    }
                }
                //Update windows with new elt
                leftWinAlleleCount.put(midAlleleCount);
                leftWinReadsCount.put(midReadsCount);
                midAlleleCount = rightWinAlleleCount.put(eltAlleleCounts);
                midReadsCount = rightWinReadsCount.put(eltReadsCount);
                midEltIndex++;
            }
            
        }
        
        System.out.println(snpData.size() - coherentSNPs.size() + " incoherent SNPs");
        snpData = coherentSNPs;

        System.out.println(snpData.size() + " kept SNPs");
        
    }
    
    /**
     * NOT USED ANYMORE.
     * A singleton is an isolated data surrounded by a different allele, for instance the “B” in “AABAA” is a singleton.  
     * The procedure replaces a data at locus x if the genotypes of the two previous (x-1 and x-2) 
     * and the two next loci (x+1 and x+2) are identical and different to the genotype at locus x. 
     * For instance, “AABAA” is replaced by “AAAAA”. This operation can be run before Imputation Step 1.
     * @param criterias
     * @throws WindowSizeException 
     */   
    public void correctSingletons(SnpSelectionCriteria criterias) throws WindowSizeException {

        //correct singletons
        long nbSNPImputed = 0;
        ArrayList<Snp> correctSingletonSnps = new ArrayList<>();
        
        if (!criterias.getPopType().equals(SnpSelectionCriteria.PopType.F2)) {
            for (int i=0; i < samplesNames.size(); i++) {
                for (int snp=2; snp < snpData.size()-2; snp++) {
                    byte myAllele = snpData.get(snp).getGenotypes()[i];
                    byte myAllele1 = snpData.get(snp-2).getGenotypes()[i];
                    byte myAllele2 = snpData.get(snp-1).getGenotypes()[i];
                    byte myAllele3 = snpData.get(snp+1).getGenotypes()[i];
                    byte myAllele4 = snpData.get(snp+2).getGenotypes()[i];
                    if (myAllele != myAllele1 && myAllele1 == myAllele2 && myAllele3 == myAllele4) {
                        snpData.get(snp).getGenotypes()[i] = myAllele1;
                        snpData.get(snp).getAlleleCounts().update(myAllele, myAllele1);
                        nbSNPImputed++;
                        
                    }
                }
            }
        } else {
            int singletonWindowSize = 20;
            int minSNPwindowHalf = 9;
            if (singletonWindowSize*2+1 > snpData.size()) {
                throw new WindowSizeException(singletonWindowSize, snpData.size());            
            }
            FreqDoubleSlidingWindow slidingWindow = new FreqDoubleSlidingWindow(singletonWindowSize, samplesNames.size());
            
            for (int snp = 0; snp < snpData.size() + singletonWindowSize; snp++) {

                if (snp < snpData.size()) {
                    slidingWindow.put(snpData.get(snp).getGenotypes(), snp);
                } else {
                    slidingWindow.put(null, snp);
                }
                
                if (snp < singletonWindowSize) {
                    correctSingletonSnps.add(snpData.get(snp));
                }
                
                if (slidingWindow.rightWindow.size() == singletonWindowSize && slidingWindow.leftWindow.size() == singletonWindowSize) {
                    
                    
                    byte[] mySnp = Arrays.copyOf(slidingWindow.midSnp, samplesNames.size());
                    
                    for (int i = 0; i< mySnp.length; i++) {
                        
                        int rightWindowNpop = slidingWindow.rightAlleleCountBySamples[i].getNbA() + slidingWindow.rightAlleleCountBySamples[i].getNbB()
                                            + slidingWindow.rightAlleleCountBySamples[i].getNbH();
        
                        int leftWindowNpop = slidingWindow.leftAlleleCountBySamples[i].getNbA() + slidingWindow.leftAlleleCountBySamples[i].getNbB()
                                            + slidingWindow.leftAlleleCountBySamples[i].getNbH();                            
                    
                        if (rightWindowNpop > minSNPwindowHalf && leftWindowNpop > minSNPwindowHalf+1) {
                            double leftFreqA = slidingWindow.leftAlleleCountBySamples[i].getNbA() / (double) leftWindowNpop;
                            double leftFreqB = slidingWindow.leftAlleleCountBySamples[i].getNbB() / (double) leftWindowNpop;
                            
                            double rightFreqA = slidingWindow.rightAlleleCountBySamples[i].getNbA() / (double) rightWindowNpop;
                            double rightFreqB = slidingWindow.rightAlleleCountBySamples[i].getNbB() / (double) rightWindowNpop;

                            if (leftFreqA > criterias.getMinFreqA() && rightFreqA > criterias.getMinFreqA()) {
                                if (mySnp[i] != genPA1) {
                                    snpData.get(slidingWindow.midSnpIndex).getAlleleCounts().update(mySnp[i], genPA1);
                                    mySnp[i] = genPA1;                                    
                                    nbSNPImputed++;
                                }
                            } else if (leftFreqB > criterias.getMinFreqB() && rightFreqB > criterias.getMinFreqB()) {
                                if (mySnp[i] != genPA2) {
                                    snpData.get(slidingWindow.midSnpIndex).getAlleleCounts().update(mySnp[i], genPA2);
                                    mySnp[i] = genPA2;
                                    nbSNPImputed++;
                                }
                            }

                        }
                    
                    }
                    Snp newSnpData = (Snp) snpData.clone();
                    newSnpData.setGenotypes(mySnp);
                    correctSingletonSnps.add(newSnpData);

                }
                
            }
            for (int v = snpData.size() - singletonWindowSize; v < snpData.size(); v++) {
                correctSingletonSnps.add(snpData.get(v)); 
            } 
            
        }

        snpData = correctSingletonSnps;
        System.out.println("Singletons correction - " + nbSNPImputed + " imputed data");

    }
    
    /**
     * copy snpData into another object snpDataBeforeImputation (deep copy) to keep raw data before imputation
     * in order to remove aliens from data after the first imputation
     * @throws CloneNotSupportedException 
     */
    public void saveSnpGenotypesBeforeImputation() throws CloneNotSupportedException {
        //copy snpGenotypes into snpDataBeforeImputation
        snpDataBeforeImputation = new ArrayList<>();
        for (int v = 0; v < snpData.size(); v++) {
            snpDataBeforeImputation.add((Snp) snpData.get(v).clone()); 
        }
    }
    
    /**
     * deep copy of snpDataBeforeImputation into snpData.
     * @throws CloneNotSupportedException 
     */
    public void copySnpGenotypes() throws CloneNotSupportedException {
        //copy snpDataBeforeImputation into snpData
        snpData = new ArrayList<>();
        for (int v = 0; v < snpDataBeforeImputation.size(); v++) {
            snpData.add((Snp) snpDataBeforeImputation.get(v).clone()); 
        }
    }
    
    /**
     * run imputation step 1 on all samples.
     * @param ssc
     * @return 
     */
    public long imputeAllSamplesStep1(SnpSelectionCriteria ssc) {
        
        //probaTable is reused in step3, to find k position
        probaTable.setpA(new double[samplesNames.size()][snpData.size()]);
        probaTable.setpB(new double[samplesNames.size()][snpData.size()]);
        probaTable.setpH(new double[samplesNames.size()][snpData.size()]);
        
        setProbaErrors();

//        long nbDataImputed = 0;
//        for (int s = 0; s < samplesNames.size(); s++) {
//            System.out.print("Imputing sample " + s + "\r");
//            long nbSnpImputed;
//            if (ssc.isUseJointProbas()) {
//                nbSnpImputed = step1_b(ssc, s);
//            } else {
//                nbSnpImputed = step1(ssc, s);
//            }
//            nbDataImputed += nbSnpImputed;
//        }

        int numSamples = samplesNames.size();
        final long nbDataImputed = IntStream.range(0, numSamples)
                .parallel()
                .mapToLong(s -> {
                    //System.out.print("Imputing sample " + s + "\r");
                    return ssc.getPopType().equals(SnpSelectionCriteria.PopType.SSD)
                            ? step1_jointProbas_SSD(ssc, s)
                            : ssc.isUseJointProbas() ? step1_jointProbas(ssc, s) : step1(ssc, s);
                })
                .sum();

        return nbDataImputed;
    }
    
    /**
     * impute step 1 on one individual.
     * Use symetric window around the SNP to calculate the sum of nbReadsA and nbReadsB in order to calculate the probabilties that the window is A, B or H
     * For each snp called midElt, there is a leftWindow and a rightWindow which contains n=windowSize snp with data (nbReadsA + nbReadsB>0) 
     * we begin by filling midElt and rightWindow. When rightWindow is full, we shift to the right and midElt will be the last element of the leftWindow.
     * At the end of the chromosome, the right window will be empty
     * @param ssc
     * @param sampleIndex
     * @return 
     */
    private long step1(SnpSelectionCriteria ssc, int sampleIndex) {

        double gtIsAThreshold = ssc.getGenoLRthreshold(), gtIsBThreshold = ssc.getGenoLRthreshold(), gtIsHThreshold = ssc.getGenoLRthreshold();
        double probaGtIsA = 0, probaGtIsB = 0, probaGtIsH = 0;
        long nbSNPImputed = 0;
        
        int windowSize = ssc.getImputeHalfWindowSize();
        SumIntSlidingWindowMinData leftWindow = new SumIntSlidingWindowMinData(windowSize, 2);
        SumIntSlidingWindowMinData rightWindow = new SumIntSlidingWindowMinData(windowSize, 2);
        byte[] midElt = null;
        int midEltIndex = -1;
        for (int v = 0; v < snpData.size(); v++) {

            while (rightWindow.nbOfData == windowSize || (v == snpData.size() - 1 && midEltIndex < snpData.size())) {

                int wNbReadsA = leftWindow.sum[0] + snpData.get(midEltIndex).getNbReadsA()[sampleIndex] +  rightWindow.sum[0];
                int wNbReadsB = leftWindow.sum[1] + snpData.get(midEltIndex).getNbReadsB()[sampleIndex] +  rightWindow.sum[1];
//                this.wlNbReadsAarray[0][midEltIndex] = wNbReadsA;
//                this.wlNbReadsBarray[0][midEltIndex] = wNbReadsB;
                double lnpA = wNbReadsA * log(probaAgivenA) + wNbReadsB * log(probaBgivenA);
                double lnpB = wNbReadsB * log(probaBgivenB) + wNbReadsA * log(probaAgivenB);
                double lnpH = wNbReadsA * log(probaAgivenH) + wNbReadsB * log(probaBgivenH);

                double minL = -745; //limit under which exp(x)= NaN
                
                double min = Math.min(Math.min(lnpA, lnpB), lnpH); //minimum de lnpA, lnpB et lnpH
                if (min < minL) {
                    lnpA = lnpA * minL / min;
                    lnpB = lnpB * minL / min;
                    lnpH = lnpH * minL / min;
                }

                probaGtIsA = exp(lnpA) / ( exp(lnpA) + exp(lnpB) + exp(lnpH) ); 

                probaTable.getpA()[sampleIndex][midEltIndex] = probaGtIsA;

                if (probaGtIsA >= gtIsAThreshold) { // this is A
                    if (snpData.get(midEltIndex).getGenotypes()[sampleIndex] != genPA1) {
                        snpData.get(midEltIndex).getGenotypes()[sampleIndex] = genPA1;                        
                        nbSNPImputed++;
                    }

                } else {                        

                    probaGtIsB = exp(lnpB) / ( exp(lnpA) + exp(lnpB) + exp(lnpH) ); 
                    probaTable.getpB()[sampleIndex][midEltIndex] = probaGtIsB;

                    if (probaGtIsB >= gtIsBThreshold) { // this is B
                        if (snpData.get(midEltIndex).getGenotypes()[sampleIndex] != genPA2) {
                            snpData.get(midEltIndex).getGenotypes()[sampleIndex] = genPA2;
                            nbSNPImputed++;
                        }
                    } else {
                        probaGtIsH = exp(lnpH) / ( exp(lnpA) + exp(lnpB) + exp(lnpH) );
                        
                        probaTable.getpH()[sampleIndex][midEltIndex] = probaGtIsH;
                        if (probaGtIsH >= gtIsHThreshold) { // this is H
                            if (snpData.get(midEltIndex).getGenotypes()[sampleIndex] != genHTZ) {
                                snpData.get(midEltIndex).getGenotypes()[sampleIndex] = genHTZ;
                                nbSNPImputed++;
                            }
                        } else {
                            snpData.get(midEltIndex).getGenotypes()[sampleIndex] = genMIS;
                            nbSNPImputed++;
                        }
                    }
                }

                //Shift left window to the right
                leftWindow.add(midElt);
                // remove first elt of left windows if too many data
                while (leftWindow.nbOfData > windowSize) {
                    leftWindow.remove();
                }

                //for the end part of the chromosome
                if (!rightWindow.window.isEmpty()) {
                    midElt = rightWindow.remove();
                }

                midEltIndex++;

            }
            
            //shift right window to the right
            byte[] elt = {snpData.get(v).getNbReadsA()[sampleIndex], snpData.get(v).getNbReadsB()[sampleIndex]};
            if (midEltIndex == -1) { //initiate first midElt
                midElt = elt;
                midEltIndex = v;
            } else if (rightWindow.nbOfData < windowSize) {
                rightWindow.add(elt);                        
            }
        }    
        return nbSNPImputed;
    }
    
    /**
     * impute SNP for individual nb sampleIndex.
     * Use symetric window around each SNP to calculate probabilities that the window is A, B or H with joint probas
     * for each snp in the window, we calculate the probabilities that the snp is A, B or H and then we multiply those proba to determine the proba that the window is A, B or H.
     * For each snp called midElt, there is a leftWindow and a rightWindow which contains n=windowSize snp with data (nbReadsA + nbReadsB>0) 
     * we begin by filling midElt and rightWindow. When rightWindow is full, we shift to the right and midElt will be the last element of the leftWindow.
     * At the end of the chromosome, the right window will be empty
     * @param ssc
     * @param sampleIndex
     * @return 
     */
    private long step1_jointProbas(SnpSelectionCriteria ssc, int sampleIndex) {    
        double gtIsAThreshold = ssc.getGenoLRthreshold(), gtIsBThreshold = ssc.getGenoLRthreshold(), gtIsHThreshold = ssc.getGenoLRthreshold();
        double probaGtIsA = 0, probaGtIsB = 0, probaGtIsH = 0;
        long nbSNPImputed = 0;
        
        int windowSize = ssc.getImputeHalfWindowSize();
        SumSlidingWindow leftWindow = new SumSlidingWindow(windowSize, 3);
        SumSlidingWindow rightWindow = new SumSlidingWindow(windowSize, 3);
        double[] midElt = null;
        int midEltIndex = -1;
        for (int v = 0; v < snpData.size(); v++) {
            while (rightWindow.nbOfData == windowSize || (v == snpData.size() - 1 && midEltIndex < snpData.size())) {
                //windows are full, we can calculate probas for the snp number midEltIndex
                double sumLnpA = leftWindow.sum[0] + midElt[0] + rightWindow.sum[0];
                double sumLnpB = leftWindow.sum[1] + midElt[1] + rightWindow.sum[1];
                double sumLnpH = leftWindow.sum[2] + midElt[2] + rightWindow.sum[2];
                
                //To avoid getting NaN values 
                double minL = -745; //limit under which exp(x)= NaN                
                double min = Math.min(sumLnpA, sumLnpB);
                min = Math.min(min, sumLnpH); //minimum de lnpA, lnpB et lnpH
                if (min < minL) {
                    sumLnpA = sumLnpA * minL / min;
                    sumLnpB = sumLnpB * minL / min;
                    sumLnpH = sumLnpH * minL / min;
                }                
                
                probaGtIsA = exp(sumLnpA) / ( exp(sumLnpA) + exp(sumLnpB) + exp(sumLnpH) );
                    probaTable.getpA()[sampleIndex][midEltIndex] = probaGtIsA;
                probaGtIsB = exp(sumLnpB) / ( exp(sumLnpA) + exp(sumLnpB) + exp(sumLnpH) );
                    probaTable.getpB()[sampleIndex][midEltIndex] = probaGtIsB;
                probaGtIsH = exp(sumLnpH) / ( exp(sumLnpA) + exp(sumLnpB) + exp(sumLnpH) );
                probaTable.getpH()[sampleIndex][midEltIndex] = probaGtIsH;

                if (probaGtIsA >= gtIsAThreshold) { // this is A
                    if (snpData.get(midEltIndex).getGenotypes()[sampleIndex] != genPA1) {
                        snpData.get(midEltIndex).getGenotypes()[sampleIndex] = genPA1;                        
                        nbSNPImputed++;
                    }
                } else if (probaGtIsB >= gtIsBThreshold) { // this is B
                    if (snpData.get(midEltIndex).getGenotypes()[sampleIndex] != genPA2) {
                        snpData.get(midEltIndex).getGenotypes()[sampleIndex] = genPA2;
                        nbSNPImputed++;
                    }
                } else if (probaGtIsH >= gtIsHThreshold) { // this is H
                    if (snpData.get(midEltIndex).getGenotypes()[sampleIndex] != genHTZ) {
                        snpData.get(midEltIndex).getGenotypes()[sampleIndex] = genHTZ;
                        nbSNPImputed++;
                    }
                } else {
                    snpData.get(midEltIndex).getGenotypes()[sampleIndex] = genMIS;
                    nbSNPImputed++; 
                }

                //Shift left window to the right
                //move midElt to the end of left window
                leftWindow.add(midElt);
                //remove first elt of left window if too many data
                while (leftWindow.nbOfData > windowSize) {
                    leftWindow.remove();
                }
                //move first element of right window to midElt
                if (!rightWindow.window.isEmpty()) {
                    midElt = rightWindow.remove();
                }
                midEltIndex++;
            }
            
            //Fill right window or shift right window to the right
            int nbReadsA = snpData.get(v).getNbReadsA()[sampleIndex];
            int nbReadsB = snpData.get(v).getNbReadsB()[sampleIndex];
            double[] elt = {0, 0, 0};
            
            if (nbReadsA + nbReadsB > 0) {                    
                //log(P(G=A))
                elt[0] = log(( Math.pow(probaAgivenA, nbReadsA) * Math.pow(probaBgivenA, nbReadsB) )
                            / ( Math.pow(probaAgivenA, nbReadsA) * Math.pow(probaBgivenA, nbReadsB) 
                            + Math.pow(probaAgivenB, nbReadsA) * Math.pow(probaBgivenB, nbReadsB) 
                            + Math.pow(probaAgivenH, nbReadsA) * Math.pow(probaBgivenH, nbReadsB) )
                        );
                //log(P(G=B))
                elt[1] = log(( Math.pow(probaAgivenB, nbReadsA) * Math.pow(probaBgivenB, nbReadsB) )
                            / ( Math.pow(probaAgivenA, nbReadsA) * Math.pow(probaBgivenA, nbReadsB) 
                            + Math.pow(probaAgivenB, nbReadsA) * Math.pow(probaBgivenB, nbReadsB) 
                            + Math.pow(probaAgivenH, nbReadsA) * Math.pow(probaBgivenH, nbReadsB) )
                        );
                //log(P(G=H))
                elt[2] = log(( Math.pow(probaAgivenH, nbReadsA) * Math.pow(probaBgivenH, nbReadsB) )
                            / ( Math.pow(probaAgivenA, nbReadsA) * Math.pow(probaBgivenA, nbReadsB) 
                            + Math.pow(probaAgivenB, nbReadsA) * Math.pow(probaBgivenB, nbReadsB) 
                            + Math.pow(probaAgivenH, nbReadsA) * Math.pow(probaBgivenH, nbReadsB) )
                        );

            }
            if (midEltIndex == -1) { //initiate first midElt
                midElt = elt;
                midEltIndex = v;
            } else if (rightWindow.nbOfData < windowSize) {
                rightWindow.add(elt);                        
            }
        }      
        return nbSNPImputed;
    }
    

    /**
    * impute step 1 on one individual in the case of SSD using joint probas.
    * Use symetric window around the SNP to calculate the sum of nbReadsA and nbReadsB in order to calculate the probabilties that the window is A or B
    * For each snp called midElt, there is a leftWindow and a rightWindow which contains n=windowSize snp with data (nbReadsA + nbReadsB>0) 
    * we begin by filling midElt and rightWindow. When rightWindow is full, we shift to the right and midElt will be the last element of the leftWindow.
    * At the end of the chromosome, the right window will be empty
    * @param ssc
    * @param sampleIndex
    * @return 
    */
    private long step1_jointProbas_SSD(SnpSelectionCriteria ssc, int sampleIndex) {
        double gtIsAThreshold = ssc.getGenoLRthreshold(), gtIsBThreshold = ssc.getGenoLRthreshold();
        double probaGtIsA = 0, probaGtIsB = 0;
        long nbSNPImputed = 0;
        
        int windowSize = ssc.getImputeHalfWindowSize();
        SumSlidingWindow leftWindow = new SumSlidingWindow(windowSize, 2);
        SumSlidingWindow rightWindow = new SumSlidingWindow(windowSize, 2);
        double[] midElt = null;
        int midEltIndex = -1;
        for (int v = 0; v < snpData.size(); v++) {
            while (rightWindow.nbOfData == windowSize || (v == snpData.size() - 1 && midEltIndex < snpData.size())) {
                //windows are full, we can calculate probas for the snp number midEltIndex
                double sumLnpA = leftWindow.sum[0] + midElt[0] + rightWindow.sum[0];
                double sumLnpB = leftWindow.sum[1] + midElt[1] + rightWindow.sum[1];

                probaGtIsA = exp(sumLnpA) / ( exp(sumLnpA) + exp(sumLnpB));
                probaTable.getpA()[sampleIndex][midEltIndex] = probaGtIsA;
                probaGtIsB = exp(sumLnpB) / ( exp(sumLnpA) + exp(sumLnpB));
                probaTable.getpB()[sampleIndex][midEltIndex] = probaGtIsB;

                if (probaGtIsA >= gtIsAThreshold) { // this is A
                    if (snpData.get(midEltIndex).getGenotypes()[sampleIndex] != genPA1) {
                        snpData.get(midEltIndex).getGenotypes()[sampleIndex] = genPA1;                        
                        nbSNPImputed++;
                    }
                } else if (probaGtIsB >= gtIsBThreshold) { // this is B
                    if (snpData.get(midEltIndex).getGenotypes()[sampleIndex] != genPA2) {
                        snpData.get(midEltIndex).getGenotypes()[sampleIndex] = genPA2;
                        nbSNPImputed++;
                    }                
                } else {
                    snpData.get(midEltIndex).getGenotypes()[sampleIndex] = genMIS;
                    nbSNPImputed++; 
                }

                //Shift left window to the right
                leftWindow.add(midElt);
                // remove first elt of left windows if too many data
                while (leftWindow.nbOfData > windowSize) {
                    leftWindow.remove();
                }

                //for the end part of the chromosome
                if (!rightWindow.window.isEmpty()) {
                    midElt = rightWindow.remove();
                }
                midEltIndex++;
            }
            
            //Fill right window or shift right window to the right
            int nbReadsA = snpData.get(v).getNbReadsA()[sampleIndex];
            int nbReadsB = snpData.get(v).getNbReadsB()[sampleIndex];
            double[] elt = {0, 0};
            
            if (nbReadsA + nbReadsB > 0) {                    
                //log(P(G=A))
                elt[0] = log(( Math.pow(probaAgivenA, nbReadsA) * Math.pow(probaBgivenA, nbReadsB) )
                            / ( Math.pow(probaAgivenA, nbReadsA) * Math.pow(probaBgivenA, nbReadsB) 
                            + Math.pow(probaAgivenB, nbReadsA) * Math.pow(probaBgivenB, nbReadsB))
                        );
                //log(P(G=B))
                elt[1] = log(( Math.pow(probaAgivenB, nbReadsA) * Math.pow(probaBgivenB, nbReadsB) )
                            / ( Math.pow(probaAgivenA, nbReadsA) * Math.pow(probaBgivenA, nbReadsB) 
                            + Math.pow(probaAgivenB, nbReadsA) * Math.pow(probaBgivenB, nbReadsB))
                        );
            }
            if (midEltIndex == -1) { //initiate first midElt
                midElt = elt;
                midEltIndex = v;
            } else if (rightWindow.nbOfData < windowSize) {
                rightWindow.add(elt);                        
            }
        }      
        return nbSNPImputed;
    }
    
    /**
     * run imputation step 2 on all samples.
     * @param ssc
     * @param chr
     * @return
     * @throws CloneNotSupportedException 
     */
    public long imputeAllSamplesStep2(SnpSelectionCriteria ssc, String chr) throws CloneNotSupportedException {
        calculateMap(chr, ssc);
        long nbDataImputed = 0;
        for (int s = 0; s < samplesNames.size(); s++) {
            //System.out.print("Imputing sample " + s + "\r");
            long nbSnpImputed = step2(s, ssc);
            nbDataImputed += nbSnpImputed;
        }
        return nbDataImputed;
    }
    
    
    /**
     * NOT USED ANYMORE.
     * imputation step 2 old version.
     * missing data chunks that are between 2 SNP with the same genotype are set to this genotype
     * @param sampleIndex 
     */
    private void step2_simpleMethod(int sampleIndex) {        
        int nbSNPImputed = 0;
        for (int snp = 1; snp < snpData.size() - 1; snp++) { 
            byte allele = snpData.get(snp).getGenotypes()[sampleIndex];
            byte previousAllele = snpData.get(snp-1).getGenotypes()[sampleIndex];
            if (allele == genMIS && previousAllele != genMIS) {
                for (int snp2 = snp + 1; snp2 < snpData.size(); snp2++) {
                    byte allele2 = snpData.get(snp2).getGenotypes()[sampleIndex];
                    if (allele2 != genMIS) {
                        if (allele2 == previousAllele) {
                            for (int snp3 = snp; snp3 < snp2; snp3++) {
                                snpData.get(snp3).getGenotypes()[sampleIndex] = previousAllele;
                                //snpAlleleCounts.get(snp3).updateAfterFillingMD(previousAllele);
                                nbSNPImputed++;
                            }
                            snp = snp2; //iterate after snp2                                
                        }
                        break;
                    }
                }
            }            
        }
    }
    
    /**
     * imputation step 2 new version.
     * after imputation 1, all the blanks between 2 snps with same genotype are filled with this genotype only if the missing data region is not too big. 
     * The maximum region size that is authorized for data filling is calculated using the local recombination rate
     * @param sampleIndex
     * @param ssc
     * @return 
     */
    private long step2(int sampleIndex, SnpSelectionCriteria ssc) {
        int nbSNPImputed = 0;
        for (int snp = 1; snp < snpData.size() - 1; snp++) { 
            byte allele = snpData.get(snp).getGenotypes()[sampleIndex];
            byte previousAllele = snpData.get(snp-1).getGenotypes()[sampleIndex];
            if (allele == genMIS && previousAllele != genMIS) {
                for (int snp2 = snp + 1; snp2 < snpData.size(); snp2++) {
                    byte allele2 = snpData.get(snp2).getGenotypes()[sampleIndex];
                    if (allele2 != genMIS) {
                        if (allele2 == previousAllele) {
                            double[] cMsegment = new double[snp2 - snp + 1];
                            
                            for (int snp3 = snp; snp3 <= snp2; snp3++) {
                                double cM = this.geneticMap.get(snp3).getcM();
                                cMsegment[snp3 - snp] = cM;
                            }
                            
                            double cMstart = cMsegment[0];
                            double cMend = cMsegment[cMsegment.length-1];
                            
                            double pDoubleRec = 0;
                            if (cMend - cMstart > 0) {
                                double middle = (double) (cMend - cMstart)/2 + cMstart;
                                int b = 0;
                                for (int v = 0; v < cMsegment.length - 1; v++) {
                                    if (cMsegment[v] <= middle && cMsegment[v+1] > middle) {
                                        b = snp + v;
                                        break;
                                    }
                                }
                                double r1 = 0;
                                double r2 = 0;

                                switch (ssc.getMappingFunction()) {
                                    case KOSAMBI:
                                        r1 = 0.5 * Math.tanh(2 * (geneticMap.get(b).getcM() - geneticMap.get(snp).getcM())/ 100);
                                        r2 = 0.5 * Math.tanh(2 * (geneticMap.get(snp2).getcM() - geneticMap.get(b).getcM())/ 100);
                                        break;
                                    case HALDANE:
                                        r1 = 0.5 * (1 - exp(-2 * (geneticMap.get(b).getcM() - geneticMap.get(snp).getcM()) / 100));
                                        r2 = 0.5 * (1 - exp(-2 * (geneticMap.get(snp2).getcM() - geneticMap.get(b).getcM()) / 100));
                                        break;
                                    case NONE:
                                        r1 = (geneticMap.get(b).getcM() - geneticMap.get(snp).getcM()) / 100;
                                        r2 = (geneticMap.get(snp2).getcM() - geneticMap.get(b).getcM()) / 100;
                                        break;
                                }
                                
                                
                                if (allele2 == genHTZ) {
                                    pDoubleRec = 2 * r1 * r2;
                                } else {
                                    pDoubleRec = r1 * r2;
                                }
                            } 
                            
                            if (pDoubleRec <= ssc.getAlphaFillGaps()) { //we can fill missing data 
                                for (int v = snp; v < snp2; v++) {     
                                    snpData.get(v).getGenotypes()[sampleIndex] = previousAllele;
                                    nbSNPImputed++;
                                }
                            }
                            
                            snp = snp2; //iterate after snp2                                
                        }
                        break;
                    }
                }
            }            
        }
        return nbSNPImputed;
    }
    
    /**
     * Get A, B, H, MD zones for one sample.
     * used in imputation step 3
     * @param sampleIndex
     * @param alien
     * @return 
     */
    private List<Zone> findABHzones(int sampleIndex, boolean alien) {        
        List<Zone> sampleABH = new ArrayList<>();
        int startZone = 0,  endZone = 0;        
        byte zone = snpData.get(0).getGenotypes()[sampleIndex];
        for (int v = 1; v < snpData.size(); v++) { 
            byte allele = snpData.get(v).getGenotypes()[sampleIndex];
            if (zone != allele) {
                endZone = v-1;
                if (alien) {
                    for (int a = 0; a < aliens.size(); a++) { //only keep zones outside aliens
                        if ((snpData.get(startZone).getPosition() < aliens.get(a).getStartPosition() && snpData.get(endZone).getPosition() >= aliens.get(a).getStartPosition())
                             ||  (snpData.get(startZone).getPosition() < aliens.get(a).getStopPosition() && snpData.get(endZone).getPosition() >= aliens.get(a).getStopPosition())) {
                            sampleABH.add(new Zone(zone, startZone, endZone, snpData.get(startZone).getPosition(), snpData.get(endZone).getPosition()));
                        }
                    }
                } else {
                    sampleABH.add(new Zone(zone, startZone, endZone, snpData.get(startZone).getPosition(), snpData.get(endZone).getPosition()));
                }                
                startZone = v; //begin new zone
                zone = allele;
            }                          
        }

        // Add last zone
        if (alien) {
            for (int a = 0; a < aliens.size(); a++) { //only keep zones inside aliens
                if ((snpData.get(startZone).getPosition() < aliens.get(a).getStartPosition() && snpData.get(endZone).getPosition() >= aliens.get(a).getStartPosition())
                     ||  (snpData.get(startZone).getPosition() < aliens.get(a).getStopPosition() && snpData.get(endZone).getPosition() >= aliens.get(a).getStopPosition())) {
                    sampleABH.add(new Zone(zone, startZone, endZone, snpData.get(startZone).getPosition(), snpData.get(endZone).getPosition()));
                }
            }
        } else {
            sampleABH.add(new Zone(zone, startZone, snpData.size() - 1, snpData.get(startZone).getPosition(), snpData.get(snpData.size() - 1).getPosition()));
        }
                    
        return sampleABH;        
    }    
    
    /**
     * Calculate the probabilities that the the SNP is HMZ or H.
     * Used for A-->H or H-->A or B-->H or H-->B transition
     * @param genHMZ
     * @param nbReadsA
     * @param nbReadsB
     * @return 
     */
    private double[] calcIndProbaBH(byte genHMZ, int nbReadsA, int nbReadsB) {
        double lpHMZ, lpH; 
        if (genHMZ == genPA1) {  //H---A
            lpHMZ = log(( Math.pow(probaAgivenA, nbReadsA) * Math.pow(probaBgivenA, nbReadsB) )
                        / ( Math.pow(probaAgivenA, nbReadsA) * Math.pow(probaBgivenA, nbReadsB)
                            + Math.pow(probaAgivenH, nbReadsA) * Math.pow(probaBgivenH, nbReadsB) )
                        );
            lpH = log(( Math.pow(probaAgivenH, nbReadsA) * Math.pow(probaBgivenH, nbReadsB) )
                        / ( Math.pow(probaAgivenA, nbReadsA) * Math.pow(probaBgivenA, nbReadsB) 
                            + Math.pow(probaAgivenH, nbReadsA) * Math.pow(probaBgivenH, nbReadsB) )
                        );
        } else {
            lpHMZ = log(( Math.pow(probaBgivenB, nbReadsB) * Math.pow(probaAgivenB, nbReadsA) )
                        / ( Math.pow(probaBgivenB, nbReadsB) * Math.pow(probaAgivenB, nbReadsA) 
                            + Math.pow(probaAgivenH, nbReadsA) * Math.pow(probaBgivenH, nbReadsB)) 
                        );
            lpH = log(( Math.pow(probaAgivenH, nbReadsA) * Math.pow(probaBgivenH, nbReadsB) )
                        / ( Math.pow(probaBgivenB, nbReadsB) * Math.pow(probaAgivenB, nbReadsA) 
                            + Math.pow(probaAgivenH, nbReadsA) * Math.pow(probaBgivenH, nbReadsB) )
                        );
        }
        
        double[] result = {lpHMZ, lpH};
        return result;
    }
    
    /** 
     * Calculate the probabilities that the SNP is A or B.
     * Used for A-->B or B-->A transition
     * @param nbReadsA
     * @param nbReadsB
     * @return 
     */
    private double[] calcIndProbaAB(int nbReadsA, int nbReadsB) {
        double lpA = log(( Math.pow(probaAgivenA, nbReadsA) * Math.pow(probaAgivenB, nbReadsB) )
                        / ( Math.pow(probaAgivenA, nbReadsA) * Math.pow(probaBgivenA, nbReadsB)
                            + Math.pow(probaAgivenB, nbReadsA) * Math.pow(probaBgivenB, nbReadsB) )
                        );
        double lpB = log(( Math.pow(probaAgivenB, nbReadsA) * Math.pow(probaBgivenB, nbReadsB) )
                        / ( Math.pow(probaAgivenA, nbReadsA) * Math.pow(probaBgivenA, nbReadsB) 
                            + Math.pow(probaAgivenB, nbReadsA) * Math.pow(probaBgivenB, nbReadsB) )
                        );
        double[] result = {lpA, lpB};
        return result;
    }
    
    /**
     * run imputation step 3 on all samples
     * @param ssc
     * @param chr
     * @param alienStep
     * @throws BreakpointWindowException 
     */
    public void imputeAllSamplesStep3(SnpSelectionCriteria ssc, String chr, boolean alienStep) throws BreakpointWindowException {
        bkps = new ConcurrentHashMap<>();
        int numSamples = samplesNames.size();
        final long nbDataImputed = IntStream.range(0, numSamples)
                .parallel()
                .mapToLong(s -> {
                    List<Zone> zones = findABHzones(s, alienStep);
                    //System.out.print("Imputing sample " + s + "\r");
                    try {                        
                        int nb = step3(ssc, zones, chr, s);                        
                        return nb;
                    } catch (BreakpointWindowException e) {
                        throw e;
                    }
                })
                .sum();

        System.out.println(nbDataImputed + " imputed data ");
    }
    
    

    /**
     * Imputation step 3 on one sample.
     * 1. Find transitions regions (A-->H, or B-->H, or A-->B, etc) with ABHzones
     * 2. Find loose interval between LB=k-2*windowSize (windowSize = nb of snps with data) and RB=k+2*windowSize
     * 3. Find breakpoint position in the loose interval:
     *  for each snp in loose interval we calculate the probability that the window before the snp (leftWindow) is B and the window after the snp (rightWindow) is H
     *  the snp is included in the leftWindow. 
     *  The probability is calculated with 2 different methods :
     *  For pBK1, we count the sums A reads and B reads over each window. Then, we calculate the binomial probability
     *  For pBK2, we calculating the probabilities pGi=B and pGi=H for each SNP of the window and then multiply them to get the probability of the window
     *  Then we get pBK = pBK1*pBK2
     *  And we look for the maximum position of pBK, that corresponds to the breakpoint position
     * @param ssc
     * @param ABHzones
     * @param chr
     * @param sampleIndex
     * @return 
     */
    private int step3(SnpSelectionCriteria ssc, List<Zone> ABHzones, String chr, int sampleIndex) {        
        int nbSNPImputed = 0;
        int transitionsNb = 0;
        int lastBkpRowAfter = 0;
        
        for (int z = 1; z < ABHzones.size(); z++) {
            Zone prevZone = ABHzones.get(z-1);
            Zone zone = ABHzones.get(z);
            

            byte genHMZ = 0; //A or B
            byte genOtherHMZ = 0; //A or B

            int windowSize = ssc.getImputeHalfWindowSize();

            if (zone.getGenotype() == genMIS) {
                continue;
            }
            
            if (prevZone.getGenotype() == genMIS) {
                if (z == 1) { //MD at the beginning
                    continue;
                } else {
                    prevZone = ABHzones.get(z - 2);                    
                }    
            }    
            
            if (prevZone.getGenotype() == zone.getGenotype()) { // A------A for example
                continue;
            }

            transitionsNb++;

            // find k position : k corresponds to the snp where p(B)~=p(H)
            int k = 0; double minProbaDiff = 1;

            for (int v = prevZone.getEnd(); v <= zone.getStart(); v++) {
                double pLeft = 0;
                double pRight = 0;
                switch (prevZone.getGenotype()) {
                    case genPA1:
                        pLeft = this.probaTable.getpA()[sampleIndex][v];
                        break;
                    case genPA2:                            
                        pLeft = this.probaTable.getpB()[sampleIndex][v];
                        break;
                    case genHTZ:                            
                        pLeft = this.probaTable.getpH()[sampleIndex][v];
                        break;
                }
                switch (zone.getGenotype()) {
                    case genPA1:
                        pRight = this.probaTable.getpA()[sampleIndex][v];
                        break;
                    case genPA2:                            
                        pRight = this.probaTable.getpB()[sampleIndex][v];
                        break;
                    case genHTZ:                            
                        pRight = this.probaTable.getpH()[sampleIndex][v];
                        break;
                }
                
                //System.out.println("pLeft=" + pLeft + " | " + "pRight=" + pRight);
                if (minProbaDiff > abs(pRight - pLeft)) {
                    minProbaDiff = abs(pRight - pLeft);
                    k = v;
                }

            }
            
            if (k== 0) { //put k in the middle
                k = prevZone.getEnd() + round((zone.getStart() - prevZone.getEnd()) / 2);
            }

            boolean reverse = false;
            String transitionType = "";
            boolean SSDType = ssc.getPopType().equals(PopType.SSD) ? true : false;
            //A---H (or B----H) 
            if ((prevZone.getGenotype() == genPA1 || prevZone.getGenotype() == genPA2) && zone.getGenotype() == genHTZ) {                                               
                genHMZ = prevZone.getGenotype();
                genOtherHMZ = genHMZ == genPA1 ? genPA2 : genPA1;
                transitionType = genHMZ == genPA1 ? "A => H" : "B => H";
            } else if (prevZone.getGenotype() == genHTZ && (zone.getGenotype() == genPA1 || zone.getGenotype() == genPA2) ) {
                reverse = true;
                genHMZ = zone.getGenotype();
                genOtherHMZ = genHMZ == genPA1 ? genPA2 : genPA1;
                transitionType = genHMZ == genPA1 ? "H => A" : "H => B";
            } else if ( (prevZone.getGenotype() == genPA1 && zone.getGenotype() == genPA2) || (prevZone.getGenotype() == genPA2 && zone.getGenotype() == genPA1) ) {
                genHMZ = prevZone.getGenotype();
                genOtherHMZ = genHMZ == genPA1 ? genPA2 : genPA1;
                transitionType = genHMZ == genPA1 ? "A => B" : "B => A";
                reverse = genHMZ == genPA2;
                SSDType = true;
            }            

            // FIND LOOSE SUPPORT BREAKPOINT INTERVAL
            // LB = k - 2 * windowSize (number of snps with data)
            int nbOfData = 0;
            int LB = prevZone.getStart();
            for (int i = k - 1;  i >= prevZone.getStart(); i--) {
                if (snpData.get(i).getNbReadsA()[sampleIndex] + snpData.get(i).getNbReadsB()[sampleIndex] > 0) {
                    nbOfData++;
                }
                if (nbOfData == 2 * windowSize) {
                    LB = i;
                    break;
                }
            }
            
            nbOfData = 0;
            int RB = zone.getEnd();
            for (int i = k + 1;  i < zone.getEnd(); i++) {
                if (snpData.get(i).getNbReadsA()[sampleIndex] + snpData.get(i).getNbReadsB()[sampleIndex] > 0) {
                    nbOfData++;
                }
                if (nbOfData == 2 * windowSize) {
                    RB = i;
                    break;
                }
            }
            
            int[] interval = findLooseSupportBkpInterval(windowSize, LB, RB, reverse, sampleIndex, genHMZ, ssc.getAlphaBreakpointLooseInterval(), SSDType);
            
            int startLooseInterval = interval[0];
            int stopLooseInterval = interval[1];
            boolean foundLooseInterval = interval[2] == 1;

            if (startLooseInterval != stopLooseInterval) {
                                
                // COUNT NB OF SNPS INSIDE LOOSE INTERVAL
                int nbDataInLooseInterval = 0;
                int nbSNPsInLooseInterval = 0;
                for (int v = startLooseInterval; reverse ? v>= stopLooseInterval : v <= stopLooseInterval; v += reverse ? -1 : 1) {
                    nbSNPsInLooseInterval++;
                    if (snpData.get(v).getNbReadsA()[sampleIndex] + snpData.get(v).getNbReadsB()[sampleIndex] > 0) {
                        nbDataInLooseInterval++;
                    }
                }

                // FIND BREAKPOINT POSITION
                
                // For each snp in support interval, calculate the probability that left window is B and right window is H
                // the snp is included in left window, not the right
                // P(G=BK) = P(G=B) * P(G=H)                
                
                int bkpRowBefore = 0;
                int bkpRowAfter = 0;
                long bkpPosition = 0;

                windowSize = ssc.getBreakpointHalfWindowSize();

                double[] probaBK1 = new double[abs(stopLooseInterval - startLooseInterval) + 1];
                double[] probaBK2 = new double[abs(stopLooseInterval - startLooseInterval) + 1];
                
                if (ssc.getBreakpointProbaCalcMethod() == 2 ) { //If only use PBK2, then PBK1 = 1 and so PBK = PBK2 
                    Arrays.fill(probaBK1, 1);
                }         
                if (ssc.getBreakpointProbaCalcMethod() == 1 ) { //If only use PBK1, then PBK2 = 1 and so PBK = PBK1 
                    Arrays.fill(probaBK2, 1);
                }    
                int[] snpsNbInLeftWindow = new int[abs(stopLooseInterval - startLooseInterval) + 1];
                int[] snpsNbInRightWindow = new int[abs(stopLooseInterval - startLooseInterval) + 1];

                double pBKmax1 = 0;
                SumIntSlidingWindowMinData leftWindowPBK1 = new SumIntSlidingWindowMinData(windowSize, 2);
                SumIntSlidingWindowMinData rightWindowPBK1 = new SumIntSlidingWindowMinData(windowSize, 2);

                double pBKmax2 = 0;
                SumSlidingWindow leftWindowPBK2 = new SumSlidingWindow(windowSize, 2);
                SumSlidingWindow rightWindowPBK2 = new SumSlidingWindow(windowSize, 2);

                //initiate first leftwindow (because don't know where to start exactly to have the right number of data)
                // we fill the window from the right to the left then we reverse it
                nbOfData = 0;
                List<byte[]> leftWindowPBK1Init = new ArrayList();                    
                int sumA = 0, sumB = 0;
                List<double[]> leftWindowPBK2Init = new ArrayList();
                double sumlpHMZ = 0, sumlpH = 0;

                int midEltIndex = startLooseInterval; //initiate first midEltIndex
                for (int v = midEltIndex; reverse ? v < snpData.size() : v > 0; v += reverse ? 1 : -1) {
                    
                    byte[] pBK1Elt = {1, 1};
                    if (ssc.getBreakpointProbaCalcMethod() != 2) {
                        pBK1Elt[0] = snpData.get(v).getNbReadsA()[sampleIndex];
                        pBK1Elt[1] = snpData.get(v).getNbReadsB()[sampleIndex];                        
                    }
                    leftWindowPBK1Init.add(pBK1Elt);                    

                    int nbReadsA = snpData.get(v).getNbReadsA()[sampleIndex];
                    int nbReadsB = snpData.get(v).getNbReadsB()[sampleIndex];
                    
                    sumA += nbReadsA;
                    sumB += nbReadsB;

                    double[] pBK2Elt = {0, 0}; // [lpHMZ, lpH]

                    if (nbReadsA + nbReadsB > 0) { 
                        if (ssc.getBreakpointProbaCalcMethod()!= 1) {
                            if (SSDType) {
                                pBK2Elt = calcIndProbaAB(nbReadsA, nbReadsB);
                            } else {
                                pBK2Elt = calcIndProbaBH(genHMZ, nbReadsA, nbReadsB);
                            }
                        }
                        nbOfData++;                            
                    }      
                    leftWindowPBK2Init.add(pBK2Elt);                        
                    sumlpHMZ += pBK2Elt[0];
                    sumlpH += pBK2Elt[1];

                    if (nbOfData == windowSize) {
                        break;
                    }          

                }                

                Collections.reverse(leftWindowPBK1Init);
                leftWindowPBK1.window = new ArrayDeque(leftWindowPBK1Init);
                leftWindowPBK1.nbOfData = nbOfData;
                leftWindowPBK1.sum[0] = sumA; leftWindowPBK1.sum[1] = sumB;

                Collections.reverse(leftWindowPBK2Init);
                leftWindowPBK2.window = new ArrayDeque(leftWindowPBK2Init);
                leftWindowPBK2.nbOfData = nbOfData;
                leftWindowPBK2.sum[0] = sumlpHMZ; leftWindowPBK2.sum[1] = sumlpH;

                for (int v = reverse ? startLooseInterval - 1 : startLooseInterval + 1; reverse ?  v >= 0: v < snpData.size(); v += reverse ? -1 : 1) {
                    
                    if (rightWindowPBK1.nbOfData < windowSize) {
                        byte nbReadsA = snpData.get(v).getNbReadsA()[sampleIndex];
                        byte nbReadsB = snpData.get(v).getNbReadsB()[sampleIndex];
                        
                        byte[] pBK1Elt = {1, 1}; //In case of not using PBK1 we need to add the element != {0,0} to the window, so that nbOfData is well updated 
                        if (ssc.getBreakpointProbaCalcMethod() != 2) {
                            pBK1Elt[0] = nbReadsA;
                            pBK1Elt[1] = nbReadsB;                           
                        }                          
                        rightWindowPBK1.add(pBK1Elt);             
                        
                        double[] eltPBK2 = {1, 1}; // [lpHMZ, lpH]
                        if (ssc.getBreakpointHalfWindowSize() != 1) {
                            if (nbReadsA + nbReadsB > 0) { 
                                if (SSDType) {
                                    eltPBK2 = calcIndProbaAB(nbReadsA, nbReadsB);
                                } else {
                                    eltPBK2 = calcIndProbaBH(genHMZ, nbReadsA, nbReadsB);                                
                                }
                            } else {
                                eltPBK2[0] = 0;
                                eltPBK2[1] = 0;
                            }
                        }
                        rightWindowPBK2.add(eltPBK2); 
                    } else  if (rightWindowPBK1.nbOfData > windowSize) {
                        break;
                    }

                    while (rightWindowPBK1.nbOfData == windowSize && (reverse ? midEltIndex >= stopLooseInterval : midEltIndex <= stopLooseInterval)) {                        
                        
                        double pBK1 = 1;
                        if (ssc.getBreakpointProbaCalcMethod() != 2) {
                            int lwNbReadsA = leftWindowPBK1.sum[0];
                            int lwNbReadsB = leftWindowPBK1.sum[1];
                            int rwNbReadsA = rightWindowPBK1.sum[0];
                            int rwNbReadsB = rightWindowPBK1.sum[1];    

                            double lpHMZ, lpH;
                            if (SSDType) {
                                if (genHMZ == genPA1) { //probabilities that left window is A and right window is B
                                    lpHMZ = lwNbReadsA * log(probaAgivenA) + lwNbReadsB * log(probaBgivenA);
                                    lpH = rwNbReadsA * log(probaAgivenB) + rwNbReadsB * log(probaBgivenB);
                                } else { //probabilities that left window is B and right window is A
                                    lpHMZ = lwNbReadsA * log(probaAgivenB) + lwNbReadsB * log(probaBgivenB);
                                    lpH = rwNbReadsA * log(probaAgivenA) + rwNbReadsB * log(probaBgivenA);
                                }
                            } else {
                                if (genHMZ == genPA1) {
                                    lpHMZ = lwNbReadsA * log(probaAgivenA) + lwNbReadsB * log(probaBgivenA);
                                    lpH = rwNbReadsA * log(probaAgivenH) + rwNbReadsB * log(probaBgivenH);

                                } else {             
                                    lpHMZ = lwNbReadsA * log(probaAgivenB) + lwNbReadsB * log(probaBgivenB);
                                    lpH = rwNbReadsA * log(probaAgivenH) + rwNbReadsB * log(probaBgivenH);
                                }
                            }
                            
                            double minL = -745; //limit under which exp(x)= NaN                
                            double min = Math.min(lpHMZ, lpH); //minimum de lnpA, lnpB et lnpH
                            if (min < minL) {
                                lpHMZ = lpHMZ * minL / min;
                                lpH = lpH * minL / min;
                            }

                            double pHMZ = exp(lpHMZ) / ( exp(lpHMZ) + exp(lpH) ); 
                            double pH = exp(lpH) / ( exp(lpHMZ) + exp(lpH) ); 

                            pBK1 = pHMZ * pH;
                        }

                        double pBK2 = 1;
                        if (ssc.getBreakpointProbaCalcMethod() != 1) {
                            double lwpHMZ = leftWindowPBK2.sum[0];
                            double rwpH = rightWindowPBK2.sum[1];
                            
                            double minL = -745; //limit under which exp(x)= NaN
                
                            if (lwpHMZ + rwpH < minL) {
                                double x = minL - lwpHMZ - rwpH;
                                pBK2 = exp(lwpHMZ + rwpH - x) / x;
                            } else {
                                pBK2 = exp(lwpHMZ + rwpH);
                            }
                        }

                        if (reverse) {
                            probaBK1[startLooseInterval - midEltIndex] = pBK1;
                            probaBK2[startLooseInterval - midEltIndex] = pBK2; 
                            snpsNbInLeftWindow[startLooseInterval - midEltIndex] = leftWindowPBK1.size();
                            snpsNbInRightWindow[startLooseInterval - midEltIndex] = rightWindowPBK1.size();
                        } else {
                            probaBK1[midEltIndex - startLooseInterval] = pBK1;
                            probaBK2[midEltIndex - startLooseInterval] = pBK2;
                            snpsNbInLeftWindow[midEltIndex - startLooseInterval] = leftWindowPBK1.size();
                            snpsNbInRightWindow[midEltIndex - startLooseInterval] = rightWindowPBK1.size();
                        }

                        if (pBK1 > pBKmax1) {
                            pBKmax1 = pBK1;
                        }

                        if (pBK2 > pBKmax2) {
                            pBKmax2 = pBK2;                                
                        }

                        //Shift left window to the right
                        if (!rightWindowPBK1.window.isEmpty()) {
                            byte[] midEltPBK1 = rightWindowPBK1.remove();
                            leftWindowPBK1.add(midEltPBK1);
                            double[] midEltPBK2 = rightWindowPBK2.remove();
                            leftWindowPBK2.add(midEltPBK2);

                        }
                        // remove first elt of left windows if too many data
                        while (leftWindowPBK1.nbOfData > windowSize) {
                            leftWindowPBK1.remove();
                            leftWindowPBK2.remove();
                        }
                        midEltIndex += reverse ? - 1 : 1;

                    }
                    
                }


                if (rightWindowPBK1.nbOfData < windowSize) {

                    bkpRowBefore = prevZone.getEnd();
                    bkpRowAfter = zone.getStart();
                    //System.out.println("can't fill right window to find breakpoint for sample " + samplesNames.get(sampleIndex) + " between " + bkpRowBefore + " and " + bkpRowAfter);
                    // can't find breakpoint, don't impute in this transition
                    bkpPosition = (int) round(snpData.get(bkpRowBefore).getPosition() + 0.5 * (snpData.get(bkpRowAfter).getPosition() - snpData.get(bkpRowBefore).getPosition()));
                    BreakPoint bkp = new BreakPoint(sampleIndex, chr, bkpPosition, 
                            bkpRowBefore,
                            bkpRowAfter,
                            false,
                            snpData.get(bkpRowBefore).getPosition(), 
                            snpData.get(bkpRowAfter).getPosition(), 
                            snpData.get(startLooseInterval).getPosition(), 
                            snpData.get(stopLooseInterval).getPosition(),
                            foundLooseInterval,
                            null,
                            null,
                            null, 
                            null,
                            null,
                            null,
                            null,
                            null,
                            transitionType,
                            snpData.get(prevZone.getEnd()).getPosition(),
                            snpData.get(zone.getStart()).getPosition()
                    );

                    bkps.computeIfAbsent(sampleIndex, ArrayList::new).add(bkp);
                    return 0;
                }
                

                double[] probaBK = new double[abs(stopLooseInterval - startLooseInterval) + 1];
                double pBKmax = 0;
                List<Integer> maxPositions = new ArrayList<>();
                int snpsNbInHMZwindow = 0, snpsNbInHwindow = 0;
                //Normalize probaBK1 and probaBK2 by dividing them by their maximum
                //3. Use the multiplication of the 2 probas to find the breakpoint
                for (int i = 0; i < probaBK.length; i++) {
                    probaBK1[i] = (double) probaBK1[i] / pBKmax1;
                    probaBK2[i] = (double) probaBK2[i] / pBKmax2;
                    probaBK[i] = probaBK1[i] * probaBK2[i];
                    if (probaBK[i] > pBKmax) {
                        pBKmax = probaBK[i];
                        maxPositions = new ArrayList<>();
                        maxPositions.add(reverse ? startLooseInterval - i : startLooseInterval + i);
                        snpsNbInHMZwindow = reverse ? snpsNbInRightWindow[i] : snpsNbInLeftWindow[i];
                        snpsNbInHwindow = reverse ? snpsNbInLeftWindow[i] : snpsNbInRightWindow[i];
                    } else if (probaBK[i] == pBKmax) {
                        maxPositions.add(midEltIndex);
                        maxPositions.add(reverse ? startLooseInterval - i : startLooseInterval + i);
                        snpsNbInHMZwindow = reverse ? snpsNbInRightWindow[i] : snpsNbInLeftWindow[i];
                        snpsNbInHwindow = reverse ? snpsNbInLeftWindow[i] : snpsNbInRightWindow[i];
                    }
                }

                //Find breakpoint position
                if (maxPositions.size() == 1) {
                    bkpRowBefore = reverse ? maxPositions.get(0) - 1 : maxPositions.get(0);
                    bkpRowAfter = bkpRowBefore + 1;                        
                } else if (maxPositions.size() > 1) {
                    bkpRowBefore = reverse ? maxPositions.get(maxPositions.size() - 1) : maxPositions.get(0);
                    bkpRowAfter = reverse ? maxPositions.get(0) : maxPositions.get(maxPositions.size() - 1);
                }
                
                //4. Determine support interval
                int startSupportInterval = reverse ? stopLooseInterval : startLooseInterval;
                for (int v = bkpRowBefore ; reverse ? v >= stopLooseInterval : v >= startLooseInterval; v--) {
                    int i = reverse ? startLooseInterval - v : v - startLooseInterval;
                    if (-log10(probaBK[i] / pBKmax) > ssc.getDropBreakpointSupportInterval()) {
                        startSupportInterval = v;
                        break;
                    }
                }

                int stopSupportInterval = reverse ? startLooseInterval : stopLooseInterval;
                for (int v = bkpRowAfter ; reverse ? v <= startLooseInterval : v <= stopLooseInterval; v++) {
                    int i = reverse ? startLooseInterval - v : v - startLooseInterval;
                    if (-log10(probaBK[i] / pBKmax) > ssc.getDropBreakpointSupportInterval()) {
                        stopSupportInterval = v;
                        break;
                    }
                }
                
                //COUNT NB OF SNPS INSIDE SUPPORT INTERVAL
                int nbSNPsInSupportInterval = 0;
                int nbDataInSupportInterval = 0;
                for (int v = startSupportInterval; v <= stopSupportInterval; v++) {
                    nbSNPsInSupportInterval++;
                    if (snpData.get(v).getNbReadsA()[sampleIndex] + snpData.get(v).getNbReadsB()[sampleIndex] > 0) {
                        nbDataInSupportInterval++;
                    }
                }
                
                boolean setInTheMiddle = false;
                
                LB = LB < prevZone.getStart() ? prevZone.getStart() : LB;
                RB = RB > zone.getEnd() ? zone.getEnd() : RB;
                LB = LB < lastBkpRowAfter ? lastBkpRowAfter : LB;
                
                if (bkpRowAfter > zone.getEnd()) {
                    bkpRowBefore = round((zone.getStart() - prevZone.getEnd())/2) + prevZone.getEnd();                
                    bkpRowAfter = bkpRowBefore + 1; 
                    setInTheMiddle = true;
                }

                if (bkpRowBefore >= lastBkpRowAfter) {
                    bkpPosition = (int) round(snpData.get(bkpRowBefore).getPosition() + 0.5 * (snpData.get(bkpRowAfter).getPosition() - snpData.get(bkpRowBefore).getPosition()));
  
                    BreakPoint bkp = new BreakPoint(sampleIndex, chr, bkpPosition, 
                            bkpRowBefore,
                            bkpRowAfter,
                            true,
                            snpData.get(bkpRowBefore).getPosition(), 
                            snpData.get(bkpRowAfter).getPosition(), 
                            snpData.get(reverse ? stopLooseInterval : startLooseInterval).getPosition(), 
                            snpData.get(reverse ? startLooseInterval : stopLooseInterval).getPosition(),
                            foundLooseInterval,
                            nbSNPsInLooseInterval,
                            nbDataInLooseInterval,
                            snpData.get(startSupportInterval).getPosition(), 
                            snpData.get(stopSupportInterval).getPosition(),
                            nbSNPsInSupportInterval,
                            nbDataInSupportInterval,
                            snpsNbInHMZwindow,
                            snpsNbInHwindow,
                            transitionType,
                            snpData.get(prevZone.getEnd()).getPosition(),
                            snpData.get(zone.getStart()).getPosition()
                    );

                    bkps.computeIfAbsent(sampleIndex, ArrayList::new).add(bkp);
                    lastBkpRowAfter = bkpRowAfter;
                } else {
                    //don't add the breakpoint and 
                    //the breakpoint "cancels" the previous one
                    if (bkps.get(sampleIndex) != null && !bkps.get(sampleIndex).isEmpty()) {
                        bkps.get(sampleIndex).remove(bkps.get(sampleIndex).size()-1);
                        if (!bkps.get(sampleIndex).isEmpty()) {
                            lastBkpRowAfter = bkps.get(sampleIndex).get(bkps.get(sampleIndex).size()-1).getBkpRowAfter();
                        }
                    }
                }
                
                 //5. Filling data inside LB and RB
                if (SSDType) {
                    for (int t = LB; t <= bkpRowBefore; t++) {
                        snpData.get(t).getGenotypes()[sampleIndex] = genHMZ;
                        nbSNPImputed++;
                    }
                    for (int t = bkpRowAfter; t <= RB ; t++) {
                        snpData.get(t).getGenotypes()[sampleIndex] = genOtherHMZ;
                        nbSNPImputed++;
                    }
                } else {
                    for (int t = LB; t <= bkpRowBefore; t++) {
                        snpData.get(t).getGenotypes()[sampleIndex] = reverse ? genHTZ : genHMZ;
                        nbSNPImputed++;
                    }
                    for (int t = bkpRowAfter; t <= RB ; t++) {
                        snpData.get(t).getGenotypes()[sampleIndex] = reverse ? genHMZ : genHTZ;
                        nbSNPImputed++;
                    }
                }
                
            } else { // if no support interval found, then the breakpoint is set in the middle of Missing data zone
                int bkpRowBefore = 0;
                int bkpRowAfter = 0;
                
                if (zone.getStart() - prevZone.getEnd() == 1) {
                   bkpRowBefore = prevZone.getEnd();
                } else {
                    bkpRowBefore = round((zone.getStart() - prevZone.getEnd())/2) + prevZone.getEnd();
                }
                bkpRowAfter = bkpRowBefore + 1;
                
                LB = LB < prevZone.getStart() ? prevZone.getStart() : LB;
                RB = RB > zone.getEnd() ? zone.getEnd() : RB;
                LB = LB < lastBkpRowAfter ? lastBkpRowAfter : LB;

                if (bkpRowBefore >= lastBkpRowAfter) {
                    long bkpPosition = (int) round(snpData.get(bkpRowBefore).getPosition() + 0.5 * (snpData.get(bkpRowAfter).getPosition() - snpData.get(bkpRowBefore).getPosition())); 
                
                    BreakPoint bkp = new BreakPoint(sampleIndex, chr, bkpPosition, 
                            bkpRowBefore,
                            bkpRowAfter,
                            true,
                            snpData.get(bkpRowBefore).getPosition(), 
                            snpData.get(bkpRowAfter).getPosition(), 
                            snpData.get(reverse ? stopLooseInterval : startLooseInterval).getPosition(), 
                            snpData.get(reverse ? startLooseInterval : stopLooseInterval).getPosition(),
                            foundLooseInterval,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            transitionType,
                            snpData.get(prevZone.getEnd()).getPosition(),
                            snpData.get(zone.getStart()).getPosition());

                    bkps.computeIfAbsent(sampleIndex, ArrayList::new).add(bkp);
                    lastBkpRowAfter = bkpRowAfter;
                    
                    
                } else {
                    //don't add the breakpoint and 
                    //the breakpoint "cancels" the previous one
                    if (!bkps.get(sampleIndex).isEmpty()) {
                        bkps.get(sampleIndex).remove(bkps.get(sampleIndex).size()-1);
                        if (!bkps.get(sampleIndex).isEmpty()) {
                            lastBkpRowAfter = bkps.get(sampleIndex).get(bkps.get(sampleIndex).size()-1).getBkpRowAfter();
                        }
                    }
                }
                
                //5. Filling data inside LB and RB
                if (SSDType) {
                    for (int t = LB; t <= bkpRowBefore; t++) {
                        snpData.get(t).getGenotypes()[sampleIndex] = genHMZ;
                        nbSNPImputed++;
                    }
                    for (int t = bkpRowAfter; t <= RB ; t++) {
                        snpData.get(t).getGenotypes()[sampleIndex] = genOtherHMZ;
                        nbSNPImputed++;
                    }
                } else {
                    for (int t = LB; t <= bkpRowBefore; t++) {
                        snpData.get(t).getGenotypes()[sampleIndex] = reverse ? genHTZ : genHMZ;
                        nbSNPImputed++;
                    }
                    for (int t = bkpRowAfter; t <= RB ; t++) {
                        snpData.get(t).getGenotypes()[sampleIndex] = reverse ? genHMZ : genHTZ;
                        nbSNPImputed++;
                    }
                }
            }
        }            
        
        //int nbBK = bkps.get(sampleIndex) == null ? 0 : bkps.get(sampleIndex).size();

        return nbSNPImputed;
    }
      
    /**
     * Estimate new values for errorA and errorB by comparing the observed data with the imputed regions. 
     * We count the proportion of A reads in BB-imputed segments, and the proportion of B reads in AA-imputed segments.
     * @param ssc 
     */
    public void calculateErrors(SnpSelectionCriteria ssc) {
        long nbReadsAinAA = 0, nbReadsBinAA = 0, nbReadsAinBB = 0, nbReadsBinBB = 0;
        for (int s = 0; s < samplesNames.size(); s++) {
            if (!parentIndexes.contains(s)) {
                for (int v = 0; v < snpData.size(); v++) {
                    byte allele = snpData.get(v).getGenotypes()[s];
                    if (allele == genPA1) {
                        nbReadsAinAA += snpData.get(v).getNbReadsA()[s];
                        nbReadsBinAA += snpData.get(v).getNbReadsB()[s];
                    } else if (allele == genPA2) {
                        nbReadsAinBB += snpData.get(v).getNbReadsA()[s];
                        nbReadsBinBB += snpData.get(v).getNbReadsB()[s];
                    }

                }
            }
        } 
        this.errorA = (double) nbReadsBinAA / (nbReadsAinAA + nbReadsBinAA);
        this.errorB = (double) nbReadsAinBB / (nbReadsBinBB + nbReadsAinBB);
        System.out.println("Estimated errors : errorA = " + errorA + " errorB = " + errorB);

        //update probaErrors
        setProbaErrors();
    }
    
    /**
     * Detect alien segments.
     * Entire chromosome segments can be misplaced that correspond due to genomic structural variation
     * Alien segments are easily detected, as they produce severe map expansion. 
     * The procedure analyzes rapid changes in the slope of the cumulated centimorgans of the genetic map calculated from the imputed matrix. 
     * When a marker is producing such change, the procedure searches for the next SNP that is closely linked to the SNP just before the slope change.
     * @param chr
     * @param ssc
     * @return
     * @throws CloneNotSupportedException 
     */
    public int detectAlienSegments(String chr, SnpSelectionCriteria ssc) throws CloneNotSupportedException {
        //saveSnpGenotypesBeforeImputation();  //used to update breakpoints after alien removal
        
        aliens = new ArrayList<>();
        int alienMaxWinSize = ssc.getAlienMaxWindowSize();
        double alienMaxRF = ssc.getAlienMaxRF();
        int slopeCutoff = ssc.getSlopeCutoff();
        int minAlienSize = ssc.getMinAlienSize();

        for (int locus1 = 0; locus1 < snpDataCollapsed.size() - 1; locus1++) {     
            Double thisSlope = geneticMap.get(locus1).getSlope();
            if (thisSlope > slopeCutoff) { //found high slope
                int myRow2 = locus1 + alienMaxWinSize; 
                if (myRow2 >= snpDataCollapsed.size() - 1) {
                    myRow2 = snpDataCollapsed.size() - 1;
                }
                
                //test offset from 1 to 10 and stop when r < 0.01
                for (int offset = 1 ; offset <= ssc.getMaxStartAlienOffset(); offset++) {
                    int testLocus = locus1 - offset;
                    if (testLocus < 0) {
                        testLocus = 0;
                    }
                    int startLocus = locus1 + minAlienSize;
                    if (startLocus >= snpDataCollapsed.size()) {
                        startLocus = snpDataCollapsed.size();
                    }
                    boolean foundAlienEnd = false;
                    int endAlienSNP = 0;
                    int foundOffset = 0;
                    for (int locus2 = locus1 + 1; locus2 <= myRow2; locus2++) { //search for alien end in window                       
                        
                        double f1RecFreq = calcRecFreq(ssc, testLocus, locus2, true)[0];
                        if (f1RecFreq <= alienMaxRF) { //found a marker with very low RF, indicating end of alien segment
                            foundAlienEnd = true;
                            endAlienSNP = locus2;
                            foundOffset = offset;
                            offset = ssc.getMaxStartAlienOffset() + 1; //to exit the offset loop
                            break;
                        }
                    }

                    int startAlienSNP = testLocus + 1;
                    if (foundAlienEnd && endAlienSNP - startAlienSNP >= minAlienSize) {
                        AlienSegment seg = new AlienSegment(chr, startAlienSNP + 1, snpDataCollapsed.get(startAlienSNP).getPosition(), endAlienSNP + 1, snpDataCollapsed.get(endAlienSNP).getPosition(), locus1, foundOffset,0,0,0);
                        aliens.add(seg);
                        locus1 = endAlienSNP + 1;
                    }
                }
            }
        }
        
        return aliens.size();
    }
    
    /**
     * Detect alien segments - 2nd method.
     * An alien is a region that is highly recombinant at its borders, and potentially has (slightly) different genotypic frequencies
     * Also, the sites that are immediately LEFT to the alien and RIGHT to the alien show no or very little recombination
     * Sometimes the aliens are also highly recombinant along their entire region, which makes the identification of their bounds difficult
     * So we can search for recombination hotspots (RHS) that are not "too far away" from each other and test if they demilit an alien
     * by testing the RF between the sites to the left of the first RHS and to the right of the second RHS
     * The best statistic seems to be the count of transitions between adjacent pairs of sites, since it does not depend on the distance in bp
     * The statistic cM/Mbp is way too dependent on the physical distance
     * So we start from Stats_sites sheet and look at the column 'trans'

     * @param chr
     * @param ssc
     * @return
     * @throws CloneNotSupportedException 
     */
    public int detectAlienSegments2(String chr, SnpSelectionCriteria ssc) throws CloneNotSupportedException {
        aliens = new ArrayList<>();
        int alienMaxWinSize = ssc.getAlienMaxWindowSize();
        double alienMaxRF = ssc.getAlienMaxRF();
        int slopeCutoff = ssc.getSlopeCutoff();
        int minAlienSize = ssc.getMinAlienSize();
        int transNbMin = ssc.getTransitionNbMinForHS();
        double minInflation_cM = ssc.getMinInflationcM();
        double minInflation_percent = ssc.getMinInflationPercent(); 
        
        int[] transitionsNbPerSite = this.calcTransitionsNbPerSite(true);

        int HS1 = 0;
        int HS2 = 0;
        for (int snp = 0; snp < snpDataCollapsed.size() - 1; snp++) {     
            //Double thisSlope = geneticMap.get(snp).getSlope();
            double minRF = 1000000;
            int startAlien = 0;
            int endAlien = 0;            
            if (transitionsNbPerSite[snp] > transNbMin) { //found Hot spot 1
                HS1 = snp;
                int alienHS2 = 0;
                int myRow2 = snp + alienMaxWinSize; 
                if (myRow2 >= snpDataCollapsed.size() - 1) {
                    myRow2 = snpDataCollapsed.size() - 1;
                }                
                for (int snp2 = snp + 1; snp2 < myRow2; snp2++) {
                    if (transitionsNbPerSite[snp2] > transNbMin) { //found Hot spot 2
                        HS2 = snp2;
                        if (HS2 - HS1 >= minAlienSize) {                            
                            //System.out.println("HS1-HS2" + Long.toString(snpDataCollapsed.get(HS1).getPosition()) + "-" + Long.toString(snpDataCollapsed.get(HS2).getPosition()));
                            int startLookOffsetHS1 = HS1 - ssc.getMaxStartAlienOffset() < 0 ? 0 : HS1 - ssc.getMaxStartAlienOffset();
                            for (int offsetHS1 = HS1-1; offsetHS1 >= startLookOffsetHS1; offsetHS1--) {
                                int endLookOffsetHS2 = HS2 + ssc.getMaxStartAlienOffset() > snpDataCollapsed.size() - 1 ? snpDataCollapsed.size() - 1 : HS2 + ssc.getMaxStartAlienOffset(); 
                                
                                for (int offsetHS2 = HS2 + 1; offsetHS2 <= endLookOffsetHS2; offsetHS2++) {
                                    double f1RecFreq = calcRecFreq(ssc, offsetHS1, offsetHS2, true)[0];    
                                    
                                    //System.out.println("offsetHS1-offsetHS2" + Long.toString(snpDataCollapsed.get(offsetHS1).getPosition()) + "-" + Long.toString(snpDataCollapsed.get(offsetHS2).getPosition()) + ": RF = " + Double.toString(f1RecFreq));
                                    if (f1RecFreq < minRF) {
                                        minRF = f1RecFreq;
                                        startAlien = offsetHS1 + 1;
                                        endAlien = offsetHS2 - 1;
                                        alienHS2 = HS2;
                                    }
                                }                                
                            }                                                 
                        }                        
                    }
                }
                if (minRF <= alienMaxRF) {
                    int middleSite = startAlien + round((endAlien - startAlien)/2);
                    double leftRF = calcRecFreq(ssc, startAlien - 1, middleSite, true)[0];    
                    double rightRF = calcRecFreq(ssc, middleSite, endAlien + 1, true)[0]; 
                    double mapInflation = (leftRF + rightRF - minRF)*100;
                    double percentMapInflation = -1;
                    if (minRF > 0) {                                    
                        percentMapInflation = mapInflation / minRF;
                    }
                    System.out.println("HS1/HS2=" + HS1 + "/" + alienHS2);
                    System.out.println("Alien=" + startAlien + "/" + endAlien);
                    System.out.println("minRF=" + minRF);
                    System.out.println("rightRF=" + rightRF);
                    System.out.println("leftRF=" + leftRF);
                    System.out.println("mapInflation=" + mapInflation);
                    System.out.println("percentMapInflation=" + percentMapInflation);

                    if (mapInflation >=  minInflation_cM && (percentMapInflation >=  minInflation_percent || percentMapInflation == -1)) {
                        AlienSegment seg = new AlienSegment(chr, startAlien + 1, snpDataCollapsed.get(startAlien).getPosition(), endAlien + 1, snpDataCollapsed.get(endAlien).getPosition(), 0, 0, HS1 + 1, alienHS2 + 1, minRF);
                        aliens.add(seg);
                        snp = endAlien + 1;
                    }                                
                }
            }
        }
        
        return aliens.size();
    }
    
    public void calculateMap(String chr, SnpSelectionCriteria ssc) throws CloneNotSupportedException {
        calculateMap(chr, ssc, false);
    }
    
    /**
     * Calculate genetic map.
     * @param chr
     * @param ssc
     * @param filterOnTransitionsNb
     * @throws CloneNotSupportedException 
     */
    public void calculateMap(String chr, SnpSelectionCriteria ssc, Boolean filterOnTransitionsNb) throws CloneNotSupportedException {
        long cMMbpRate = ssc.getBpPercM();
        MappingFunction mappingFunction = ssc.getMappingFunction(); //"Haldane", "Kosambi" or "none"

        ArrayList<Snp> snpBeforeMap = new ArrayList();
        for (int v = 0; v < snpData.size(); v++) {
            snpBeforeMap.add((Snp) snpData.get(v).clone()); 
        }
        
        if (filterOnTransitionsNb && (ssc.getMinTransitions() != -1 || ssc.getMaxTransitions() != -1)) {
            for (int s = 0; s < samplesNames.size(); s++) {                    
                int sampleTransitionsNb = 0;
                if (bkps.get(s) != null) {
                    sampleTransitionsNb = bkps.get(s).size();
                }
                if ((ssc.getMaxTransitions() != -1 && sampleTransitionsNb > ssc.getMaxTransitions()) 
                        || sampleTransitionsNb < ssc.getMinTransitions()) {
                    for (int v = 0; v < snpData.size(); v++) {
                        snpData.get(v).getGenotypes()[s] = genMIS;                        
                    }
                }
            }
            
        }
                
        geneticMap = new ArrayList();
        double mapsize = 0;
        for (int v = 0; v < snpData.size(); v++) {
            double cMbp = 0;
            double slope = 0;
            double slope2 = 0;
            
            GeneticMap elt = new GeneticMap(chr, snpData.get(v).getName(), snpData.get(v).getPosition(), 0, 0, 0, 0, 0, 0);
            geneticMap.add(elt);            
            
            if (v > 0) {
                double[] getRF = calcRecFreq(ssc, v-1, v);
                double f1RecFreq = getRF[0];
                double recFreq = getRF[1];
                elt.setF1Rec(f1RecFreq);
                elt.setRf(recFreq);

                switch (mappingFunction) {
                    case KOSAMBI:
                        mapsize = mapsize + Functions.mapping(true, f1RecFreq);
                        break;
                    case HALDANE:
                        mapsize = mapsize + Functions.mapping(false, f1RecFreq);
                        break;
                    case NONE:    
                        mapsize = mapsize + 100 * f1RecFreq;
                        break;
                    default:
                        break;
                }
                
                double cM = (double) Math.round(mapsize * 100) / 100; //'cM v          
                elt.setcM(cM);
                
                if (v>1) {
                    cMbp = (double) (geneticMap.get(v).getcM() - geneticMap.get(v-1).getcM()) / (snpData.get(v).getPosition() - snpData.get(v-1).getPosition()) * 1000000; 
                    geneticMap.get(v-1).setcMbp(cMbp);
                    slope = (geneticMap.get(v).getcM() - geneticMap.get(v-2).getcM()) / (snpData.get(v).getPosition() - snpData.get(v-2).getPosition()) * cMMbpRate;
                    geneticMap.get(v-1).setSlope(slope);                
                }
                if (v>2) {
                    slope2 = (geneticMap.get(v-1).getSlope() - geneticMap.get(v-3).getSlope()) / (snpData.get(v-1).getPosition() - snpData.get(v-3).getPosition()) * cMMbpRate;
                    geneticMap.get(v-2).setSlope2(slope2);
                }
            }
                  
        }

        snpData = snpBeforeMap;
    }
    
    /**
     * calculate genetic map on collapsing data.
     * @param chr
     * @param ssc
     * @param filterOnTransitionsNb
     * @throws CloneNotSupportedException 
     */
    public void calculateMapOnCollapsing(String chr, SnpSelectionCriteria ssc, Boolean filterOnTransitionsNb) throws CloneNotSupportedException {
        long cMMbpRate = ssc.getBpPercM();
        MappingFunction mappingFunction = ssc.getMappingFunction(); //"Haldane", "Kosambi" or "none"
        
        ArrayList<Snp> snpBeforeMap = new ArrayList();
        for (int v = 0; v < snpDataCollapsed.size(); v++) {
            snpBeforeMap.add((Snp) snpDataCollapsed.get(v).clone()); 
        }
        
        if (filterOnTransitionsNb && (ssc.getMinTransitions() != -1 || ssc.getMaxTransitions() != -1)) {
            for (int s = 0; s < samplesNames.size(); s++) {                    
                int sampleTransitionsNb = 0;
                if (bkps.get(s) != null) {
                    sampleTransitionsNb = bkps.get(s).size();
                }
                if ((ssc.getMaxTransitions() != -1 && sampleTransitionsNb > ssc.getMaxTransitions()) 
                        || sampleTransitionsNb < ssc.getMinTransitions()) {
                    for (int v = 0; v < snpDataCollapsed.size(); v++) {
                        snpDataCollapsed.get(v).getGenotypes()[s] = genMIS;                        
                    }
                }
            }
            
        }

        
        geneticMap = new ArrayList();
        double mapsize = 0;
        for (int v = 0; v < snpDataCollapsed.size(); v++) {
            double cMbp = 0;
            double slope = 0;
            double slope2 = 0;
            
            GeneticMap elt = new GeneticMap(chr, snpDataCollapsed.get(v).getName(), snpDataCollapsed.get(v).getPosition(), 0, 0, 0, 0, 0, 0);
            geneticMap.add(elt);            
            
            if (v > 0) {
                double[] getRF = calcRecFreq(ssc, v-1, v, true);
                double f1RecFreq = getRF[0];
                double recFreq = getRF[1];
                elt.setF1Rec(f1RecFreq);
                elt.setRf(recFreq);

                switch (mappingFunction) {
                    case KOSAMBI:
                        mapsize = mapsize + Functions.mapping(true, f1RecFreq);
                        break;
                    case HALDANE:
                        mapsize = mapsize + Functions.mapping(false, f1RecFreq);
                        break;
                    case NONE:    
                        mapsize = mapsize + 100 * f1RecFreq;
                        break;
                    default:
                        break;
                }
                
                double cM = (double) Math.round(mapsize * 100) / 100; //'cM v          
                elt.setcM(cM);
                
                if (v>1) {
                    cMbp = (double) (geneticMap.get(v).getcM() - geneticMap.get(v-1).getcM()) / (snpDataCollapsed.get(v).getPosition() - snpDataCollapsed.get(v-1).getPosition()) * 1000000; 
                    geneticMap.get(v-1).setcMbp(cMbp);
                    slope = (geneticMap.get(v).getcM() - geneticMap.get(v-2).getcM()) / (snpDataCollapsed.get(v).getPosition() - snpDataCollapsed.get(v-2).getPosition()) * cMMbpRate;
                    geneticMap.get(v-1).setSlope(slope);                
                }
                if (v>2) {
                    slope2 = (geneticMap.get(v-1).getSlope() - geneticMap.get(v-3).getSlope()) / (snpDataCollapsed.get(v-1).getPosition() - snpDataCollapsed.get(v-3).getPosition()) * cMMbpRate;
                    geneticMap.get(v-2).setSlope2(slope2);
                }
            }
                  
        }
        snpDataCollapsed = snpBeforeMap;

    }
    
    public double[] calcRecFreq(SnpSelectionCriteria ssc, int locus1, int locus2) {
        return calcRecFreq(ssc, locus1, locus2, false);
    }
    
    /**
     * calculate recombination frequency between 2 loci
     * @param ssc
     * @param locus1
     * @param locus2
     * @param collapsed
     * @return 
     */
    public double[] calcRecFreq(SnpSelectionCriteria ssc, int locus1, int locus2, boolean collapsed) {
        PopType popType = ssc.getPopType();
        boolean useHtzinSSD = true; //param use heterozygote in SSD
        double htzWeight = 0.5; 
        double f1RecFreq = 0;
        double recFreq = 0;
        if (popType.equals(PopType.SSD) || popType.equals(PopType.BCSSD)) {
            double Na = 0, Nb = 0, Nc = 0, Nd = 0;
            double loc1P1 = 0, loc1P2 = 0, loc2P1 = 0, loc2P2 = 0;
            double N = 0;
            if (useHtzinSSD) {
//               Genotypic classes:
//               A   H   B
//               A   a   e   b
//               H   f   g   h
//               B   c   i   d
//    
//               e = 0.5*b _> g is added to N with weight 0.5
//               f = 0.5*c _> g is added to N with weight 0.5
//               h = 0.5*b _> g is added to N with weight 0.5
//               i = 0.5*c _> g is added to N with weight 0.5
//               g = g _> g is added to N with weight 1
            
                double ssdNa = 0, ssdNb = 0, ssdNc = 0, ssdNd = 0, ssdNg = 0;
                for (int s = 0; s < samplesNames.size(); s++) {
                    
                    if (!parentIndexes.contains(s)) {
                    
                        byte gtLocus1 = collapsed ? snpDataCollapsed.get(locus1).getGenotypes()[s] : snpData.get(locus1).getGenotypes()[s];
                        byte gtLocus2 = collapsed ? snpDataCollapsed.get(locus2).getGenotypes()[s] : snpData.get(locus2).getGenotypes()[s];
                        if (gtLocus1 == genPA1) {
                            loc1P1 = loc1P1 + 1;
                            if (gtLocus2 == genPA1) { //AA
                                ssdNa++;
                            } else if (gtLocus2 == genPA2) { //AB
                                ssdNb++;
                            } else if (gtLocus2 == genHTZ) { // 'AH 'for SSD with htz, class e 
                                ssdNb = ssdNb + htzWeight;
                                ssdNg = ssdNg + htzWeight;
                            }
                        } else if (gtLocus1 == genPA2) {
                            loc1P2 = loc1P2 + 1;
                            if (gtLocus2 == genPA1) { //BA
                                ssdNc++;
                            } else if (gtLocus2 == genPA2) { //BB
                                ssdNd++;
                            } else if (gtLocus2 == genHTZ) { //BH for SSD with htz, class i
                                ssdNc = ssdNc + htzWeight;
                                ssdNg = ssdNg + htzWeight;
                            }
                        } else if (gtLocus1 == genHTZ) { //'H 'for SSD with htz
                            loc1P1 = loc1P1 + htzWeight;
                            loc1P2 = loc1P2 + htzWeight;
                            if (gtLocus2 == genPA1) { //HA, class f
                                ssdNc = ssdNc + htzWeight;
                                ssdNg = ssdNg + htzWeight;
                            } else if (gtLocus2 == genPA2) { //HB, class h
                                ssdNb = ssdNb + htzWeight;
                                ssdNg = ssdNg + htzWeight;
                            } else if (gtLocus2 == genHTZ) { //HH, class g
                                ssdNg = ssdNg + htzWeight * 2;
                            }
                        }
                        if (gtLocus2 ==  genPA1) {
                            loc2P1++;
                        } else if (gtLocus2 ==  genPA2) {
                            loc2P2++;
                        }
                    }
                }
                double ssdN = ssdNa + ssdNb + ssdNc + ssdNd + ssdNg;
                Na = ssdNa;
                Nb = ssdNb;
                Nc = ssdNc;
                Nd = ssdNd;
                N = Na + Nb + Nc + Nd;

            } else {

                for (int s = 0; s < samplesNames.size(); s++) {
                    if (!parentIndexes.contains(s)) {
                        byte gtLocus1 = collapsed ? snpDataCollapsed.get(locus1).getGenotypes()[s] : snpData.get(locus1).getGenotypes()[s];
                        byte gtLocus2 = collapsed ? snpDataCollapsed.get(locus2).getGenotypes()[s] : snpData.get(locus2).getGenotypes()[s];
                        if (gtLocus1 == genPA1) {
                            loc1P1 = loc1P1 + 1;
                            if (gtLocus2 == genPA1) { //AA
                                Na++;
                            } else if (gtLocus2 == genPA2) { //AB
                                Nb++;
                            }
                        } else if (gtLocus1 == genPA2) {
                            loc1P2++;
                            if (gtLocus2 == genPA1) { //BA
                                Nc++;
                            } else if (gtLocus2 == genPA2) { //BB
                                Nd++;
                            }
                        }
                        if (gtLocus2 ==  genPA1) {
                            loc2P1++;
                        } else if (gtLocus2 ==  genPA2) {
                            loc2P2++;
                        }
                    }
                }
                N = Na + Nb + Nc + Nd;

            }

            if (N > 0) {
                recFreq = (Nb + Nc) / N;
            } else {
                recFreq = -1;
            }

            if (recFreq >= 0.5 && recFreq <= 1) {
                recFreq = 0.5;
            } else {
                f1RecFreq = recFreq / (2 - 2 * recFreq);
                if (popType.equals(PopType.BCSSD)) {
                    f1RecFreq = f1RecFreq * 2;
                }
            }

        } else if (popType.equals(PopType.F2) || popType.equals(PopType.BC1F3)) {

            int nbAA = 0, nbAB = 0, nbAH = 0, nbBA = 0, nbBB = 0, nbBH = 0, nbMD = 0;
            int nbHA = 0, nbHB = 0, nbHH = 0, Npop = 0;

            for (int s = 0; s < samplesNames.size(); s++) {
                if (!parentIndexes.contains(s)) {
                    byte myAlleleSNP1 = collapsed ? snpDataCollapsed.get(locus1).getGenotypes()[s] : snpData.get(locus1).getGenotypes()[s];
                    byte myAlleleSNP2 = collapsed ? snpDataCollapsed.get(locus2).getGenotypes()[s] : snpData.get(locus2).getGenotypes()[s];
                    if (myAlleleSNP1 == genHTZ) {
                        if (myAlleleSNP2 == genHTZ) {
                            nbHH++;
                        } else if (myAlleleSNP2 == genPA1) {
                            nbHA++;
                        } else if (myAlleleSNP2 == genPA2) {
                            nbHB++;
                        }
                    } else if (myAlleleSNP1 == genPA1) {
                        if (myAlleleSNP2 == genHTZ) {
                            nbAH++;
                        } else if (myAlleleSNP2 == genPA1) {
                            nbAA++;
                        } else if (myAlleleSNP2 == genPA2) {
                            nbAB++;
                        }
                    } else if (myAlleleSNP1 == genPA2) {
                        if (myAlleleSNP2 == genHTZ) {
                            nbBH++;
                        } else if (myAlleleSNP2 == genPA1) {
                            nbBA++;
                        } else if (myAlleleSNP2 == genPA2) {
                            nbBB++;
                        }
                    } else {
                        nbMD++;
                    }
                }
            }
            Npop = nbAA + nbBB + nbHH + nbAB + nbBA + nbAH + nbBH + nbHA + nbHB + nbMD;

            if (Npop > 0) { //recombination fraction
                f1RecFreq = Functions.RecFreq_F2(nbAA, nbBB, nbHH, nbAB, nbBA, nbAH, nbBH, nbHA, nbHB, Npop);
            }
            recFreq = f1RecFreq;
            
            if (popType.equals(PopType.BC1F3) && f1RecFreq <= 0.3) {
                f1RecFreq  = -133.13 * Math.pow(f1RecFreq, 6) + 402.25 * Math.pow(f1RecFreq, 5) - 285.2 * Math.pow(f1RecFreq, 4) + 84.917 * Math.pow(f1RecFreq, 3) - 11.117 * Math.pow(f1RecFreq, 2) - 0.0017 * f1RecFreq + 0.9211;
            }
        } else {
            System.out.println("BC1 and DH population type not yet available");
        }
        double[] res = {f1RecFreq, recFreq};
        return res;
    }
    
    /**
     * NOT USED ANYMORE.
     * Impute singletons in breakpoint
     * @param popType 
     */
    public void imputeSingletons(PopType popType) {
        System.out.println("--- STEP 9 - IMPUTE SINGLETONS IN BREAKPOINTS ---");
        
        long nbSNPImputed = 0;

        if (popType.equals(SnpSelectionCriteria.PopType.F2) || popType.equals(SnpSelectionCriteria.PopType.BC1)) {            
            for (int i=0; i<samplesNames.size(); i++) {
                for (int snp = 1; snp < snpData.size()-1; snp++) {
                    byte allele = snpData.get(snp).getGenotypes()[i];                    
                    byte previousAllele = snpData.get(snp - 1).getGenotypes()[i];
                    byte nextAllele = snpData.get(snp + 1).getGenotypes()[i];
                    if ( (allele == genPA1 &&   // BAH or HAB
                                ((previousAllele == genPA2 && nextAllele == genHTZ) 
                                || (previousAllele == genHTZ && nextAllele == genPA2)))
                            || (allele == genPA2 &&   // ABH or HBA
                                ((previousAllele == genPA1 && nextAllele == genHTZ) 
                                || (previousAllele == genHTZ && nextAllele == genPA1))) ) {
                        long position = snpData.get(snp).getPosition();
                        long previousPosition = snpData.get(snp-1).getPosition();
                        long nextPosition = snpData.get(snp+1).getPosition();
                        if (position - previousPosition < nextPosition - position) {
                            snpData.get(snp).getAlleleCounts().update(snpData.get(snp).getGenotypes()[i], previousAllele);
                            snpData.get(snp).getGenotypes()[i] = previousAllele;  //becomes BBH or HHB
                            nbSNPImputed++;
                        } else {
                            snpData.get(snp).getAlleleCounts().update(snpData.get(snp).getGenotypes()[i], nextAllele);
                            snpData.get(snp).getGenotypes()[i] = nextAllele;  //becomes BHH or HBB
                            nbSNPImputed++;
                        }                        
                    }
                }
            }
        } else {
            for (int i=0; i<samplesNames.size(); i++) {
                for (int snp = 1; snp < snpData.size()-1; snp++) {
                    byte allele = snpData.get(snp).getGenotypes()[i];                    
                    byte previousAllele = snpData.get(snp - 1).getGenotypes()[i];
                    byte nextAllele = snpData.get(snp + 1).getGenotypes()[i]; 
                    if ( (allele == genHTZ || allele == genMIS) &&
                                ((previousAllele == genPA1 && nextAllele == genPA2)
                                || (previousAllele == genPA2 && nextAllele == genPA1)
                                || (previousAllele == genPA2 && nextAllele == genHTZ)
                                || (previousAllele == genHTZ && nextAllele == genPA2)
                                || (previousAllele == genPA1 && nextAllele == genHTZ)
                                || (previousAllele == genHTZ && nextAllele == genPA1)) 
                            || (allele == genPA1 &&
                                ( (previousAllele == genPA2 && nextAllele == genHTZ)
                                || (previousAllele == genHTZ && nextAllele == genPA2))) 
                            || (allele == genPA2 &&
                                ((previousAllele == genPA1 && nextAllele == genHTZ)
                                || (previousAllele == genHTZ && nextAllele == genPA1))) ) {
                        long position = snpData.get(snp).getPosition();
                        long previousPosition = snpData.get(snp-1).getPosition();
                        long nextPosition = snpData.get(snp+1).getPosition();
                        if (position - previousPosition < nextPosition - position) {
                            snpData.get(snp).getAlleleCounts().update(snpData.get(snp).getGenotypes()[i], previousAllele);
                            snpData.get(snp).getGenotypes()[i] = previousAllele;  //becomes BBH or HHB
                            nbSNPImputed++;
                        } else {
                            snpData.get(snp).getAlleleCounts().update(snpData.get(snp).getGenotypes()[i], nextAllele);
                            snpData.get(snp).getGenotypes()[i] = nextAllele;  //becomes BBH or HHB
                            nbSNPImputed++;
                        }
                    }
                }
            }
        }
        System.out.println(nbSNPImputed + " imputed data");
    }
    
    /**
     * Detect improbable chunks.
     * Consider 2 SNPs A and C that define the bounds of a region imputed as H and surrounded by regions imputed as A or B. 
     * Search for the SNP B that is the closest to the middle point between A and C (in cM). 
     * Also search for an SNP D before the SNP A so that dDA~dAB, and an SNP E after the SNP C so that dCE~dBC. 
     * Then, calculate the recombination fractions rDB and rBE from dDB and dBE using the inverse of the Kosambi mapping function. 
     * Then the maximum probability of the “chunk” to be different to the surrounding genotype is 
     * rABC=rDB*rBE
     * the chunks for which rABC≤α are restored with the surrounding genotype.
     * @param chr
     * @param ssc
     * @throws CloneNotSupportedException 
     */
    public void detectImprobableChunks(String chr, SnpSelectionCriteria ssc) throws CloneNotSupportedException {       

        chunks = new HashMap<>();
        
        int totalNbCorrectedChunks = 0;
        int nbCorrectedChunks = 1; 
        int nbSNPImputed = 0;
        int run = 1;
        while (nbCorrectedChunks > 0) {
            //System.out.print("pass " + run + " - Looking for chunks...\r");
            nbCorrectedChunks = 0;
            calculateMap(chr, ssc);               
        
            for (int s = 0; s < samplesNames.size(); s++) {            

                List<BreakPoint> sampleBps = bkps.get(s); 
                if (sampleBps == null) {
                    sampleBps = new ArrayList<>();
                }

                //System.out.print("sample " + s + "\r");

                for (int snp = 1; snp < snpData.size(); snp++) {
                    byte allele = snpData.get(snp).getGenotypes()[s];
                    byte previousAllele = snpData.get(snp - 1).getGenotypes()[s];
                    boolean chunkOfMissingData = false;
                    if (allele != previousAllele && previousAllele != genMIS) {  //possible start of a chunk detected                      
                        int A = snp - 1;

                        int startLookingForEnd = snp + 1;
                        if (allele == genMIS) { //the chunk begins with MD
                            for (int snp3 = snp + 1; snp3 < snpData.size() ;snp3++) {
                                if (snpData.get(snp3).getGenotypes()[s] != genMIS) {
                                    if (snpData.get(snp3).getGenotypes()[s] != previousAllele) { //actual data in the chunk
                                        allele = snpData.get(snp3).getGenotypes()[s];
                                        startLookingForEnd = snp3 + 1;                                    
                                    } else {
                                        chunkOfMissingData = true;
                                    }
                                    break;
                                }
                            } 
                        }
                        if (!chunkOfMissingData) {
                            for (int snp2 = startLookingForEnd; snp2 < snpData.size(); snp2++) {
                                byte allele2 = snpData.get(snp2).getGenotypes()[s]; 

                                if (allele2 != genMIS && allele != allele2) { //possible end of chunk detected
                                    if (allele2 == previousAllele) {

                                        int C = snp2;
                                        int chunkSize = snp2 - snp;

                                        if (chunkSize <= ssc.getSmallChunkMaxSize()) { //it's a chunk of acceptable size                                           
                                            
                                            //Check if chunk environment is homegeneous (a chunk environment is homogeneous if the chunk is surrounded by 2 segments of the same length with same genotype)
                                            int chunkEnvironmentSize = (int) (round(chunkSize * ssc.getChunkEnvironmentMultiplier()));
                                            boolean chunkEnvIsHomogeneous = true;
                                            for (int v = A - chunkEnvironmentSize + 1; v <= A; v++) { //verify homogeneity up
                                                if (v < 1) {
                                                    chunkEnvIsHomogeneous = false;
                                                    break;
                                                } else {
                                                   if (snpData.get(v).getGenotypes()[s] != allele2 && snpData.get(v).getGenotypes()[s] != genMIS) {
                                                        chunkEnvIsHomogeneous = false;
                                                   }
                                                }  
                                            }

                                            for (int v = C; v < C + chunkEnvironmentSize; v++) { //verify homogeneity down
                                                if (v >= snpData.size()) {
                                                    chunkEnvIsHomogeneous = false;
                                                    break;
                                                } else {
                                                   if (snpData.get(v).getGenotypes()[s] != allele2 && snpData.get(v).getGenotypes()[s] != genMIS) {
                                                        chunkEnvIsHomogeneous = false;
                                                   }
                                                }  
                                            }           
                                            
                                            if (chunkEnvIsHomogeneous) {                                            
                                                Chunk chunk;
                                                
                                                //find snp B in the middle in cM of A and C                                                
                                                double[] ACcMsegment = new double[C - A + 1];

                                                for (int v = A; v <= C; v++) {                                            
                                                    double cM = ssc.isUsePhysicalDistanceToFindB() ? this.snpData.get(v).getPosition() : this.geneticMap.get(v).getcM();
                                                    //double cM = this.geneticMap.get(v).getcM();
                                                    ACcMsegment[v - A] = cM;
                                                }
                                                double cMstart = ACcMsegment[0];
                                                double cMend = ACcMsegment[ACcMsegment.length-1];

                                                int B = 0;
                                                if (cMstart == cMend) {
                                                    B = round((C - A) / 2) + A;
                                                } else {
                                                    double middle = (double) (cMend - cMstart)/2 + cMstart;
                                                    for (int v = 0; v < ACcMsegment.length - 1; v++) {
                                                        if (ACcMsegment[v]<= middle && ACcMsegment[v+1]> middle) {
                                                            B = A + v;
                                                            break;
                                                        }
                                                    }
                                                }
                                                
                                                double dAB = ssc.isUsePhysicalDistanceToFindDE() ?  snpData.get(B).getPosition() - snpData.get(A).getPosition() : geneticMap.get(B).getcM() - geneticMap.get(A).getcM();
                                                double dBC = ssc.isUsePhysicalDistanceToFindDE() ?  snpData.get(C).getPosition() - snpData.get(B).getPosition() : geneticMap.get(C).getcM() - geneticMap.get(B).getcM();

                                                //find snp D as d(AB)~d(DA)
                                                int D = A;
                                                for (int v = A - 1; v >= 0; v--) {
                                                    double dDA = ssc.isUsePhysicalDistanceToFindDE() ? snpData.get(A).getPosition() - snpData.get(v).getPosition() : geneticMap.get(A).getcM() - geneticMap.get(v).getcM();
                                                    if (dDA >= dAB) {
                                                        D = v;
                                                        break;
                                                    }
                                                }

                                                //find snp E as d(BC)~d(CE)
                                                int E = C;
                                                for (int v = C + 1; v < snpData.size(); v++) {
                                                    double dCE = ssc.isUsePhysicalDistanceToFindDE() ? (double) snpData.get(v).getPosition() - snpData.get(C).getPosition() : geneticMap.get(v).getcM() - geneticMap.get(C).getcM();
                                                    if (dCE >= dBC) {
                                                        E = v;
                                                        break;
                                                    }
                                                }

                                                double dDB = geneticMap.get(B).getcM() - geneticMap.get(D).getcM();
                                                double dBE = geneticMap.get(E).getcM() - geneticMap.get(B).getcM();                                        

                                                double rDB = 0, rBE = 0;
                                                switch (ssc.getMappingFunction()) {
                                                    case KOSAMBI:
                                                        rDB = 0.5 * Math.tanh(2 * dDB/ 100);
                                                        rBE = 0.5 * Math.tanh(2 * dBE/ 100);
                                                        break;
                                                    case HALDANE:
                                                        rDB = 0.5 * (1 - exp(-2 * dDB / 100));
                                                        rBE = 0.5 * (1 - exp(-2 * dBE / 100));
                                                        break;
                                                    case NONE:
                                                        rDB = dDB / 100;
                                                        rBE = dBE / 100;
                                                        break;
                                                }

                                                double rDBE = rDB * rBE;

                                                if (rDBE <= ssc.getAlphaChunk()) { //the chunk is restored with the surrounding genotype
                                                    nbCorrectedChunks++;
                                                    for (int v = A + 1; v < C; v++) {
                                                        snpData.get(v).getGenotypes()[s] = previousAllele;
                                                        nbSNPImputed++;
                                                    }
                                                    
                                                    for (int bp = sampleBps.size() - 1; bp >= 0; bp--) {
                                                        if (sampleBps.get(bp).getStopBkpPosition() > snpData.get(A > 0 ? A : 0).getPosition() && sampleBps.get(bp).getStartBkpPosition() < snpData.get(C < snpData.size() - 1 ? C : snpData.size() - 1).getPosition()) {
                                                            sampleBps.remove(bp);
                                                        }
                                                    }

                                                    chunk = new Chunk(previousAllele, true, dDB, dBE, rDBE, allele, A + 1, C - 1, D, A, B, C, E);
                                                } else {
                                                    chunk = new Chunk(previousAllele, false, dDB, dBE, rDBE, allele, A + 1, C - 1, D, A, B, C, E);
                                                }
                                                if (chunks.get(s) == null) {
                                                    chunks.put(s, new ArrayList<>());
                                                }
                                                chunks.get(s).add(chunk);
                                            }
                                        }
                                    } 

                                    snp = snp2 - 1;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            //System.out.println("pass " + run + " - " + nbCorrectedChunks + " corrected chunks       "); 
            run++;
            totalNbCorrectedChunks += nbCorrectedChunks;
        }
        
        System.out.println(totalNbCorrectedChunks + " corrected chunks");
        Integer bpsNb = bkps.values().stream().mapToInt(List::size).sum();
        System.out.println(bpsNb + " breakpoints were kept (~" + bpsNb/samplesNames.size() + " breakpoints per individual)");
    }
    
    /** 
     * collapse the imputed matrix.
     * Keep onmy the SNPs that recombination breakpoints
     * @param keepBothBreakpointMarkers
     * @throws CloneNotSupportedException 
     */
    public void collapse(boolean keepBothBreakpointMarkers) throws CloneNotSupportedException {       

        snpDataCollapsed = new ArrayList<>();
        
        //keep first snp
        snpDataCollapsed.add((Snp) snpData.get(0).clone());
        
        int lastSnpIndexToKeep = 0;

        for (int snp = 1; snp < snpData.size() - 1; snp++) {
            boolean keepThisSnp = false;

            for (int i = 0; i < samplesNames.size(); i++) {
                byte allele = snpData.get(snp).getGenotypes()[i];
                byte previousAllele = snpData.get(snp-1).getGenotypes()[i];
                if (allele != previousAllele && allele != genMIS && previousAllele != genMIS) {
                    //this is a breakpoint, we keep the marker
                    keepThisSnp = true;                    
                    break;
                }
            }
            if (keepThisSnp) {
                if (keepBothBreakpointMarkers && lastSnpIndexToKeep != snp-1) {
                    snpDataCollapsed.add((Snp) snpData.get(snp-1).clone());
                }
                snpDataCollapsed.add((Snp) snpData.get(snp).clone());
                lastSnpIndexToKeep = snp;
            }
        }
        //keep last snp
        snpDataCollapsed.add((Snp) snpData.get(snpData.size()-1).clone());
        
        //System.out.println("Collapsing done - " + snpDataCollapsed.size() + " kept SNPs");
        
    }
    
    /**
     * FOR TESTING PURPOSE.
     * Extract breakpoints
     * used to extract breakpoints from imputed data by another tool
     * @param chr
     * @throws CloneNotSupportedException 
     */
    public void extractBreakpoints(String chr) throws CloneNotSupportedException {
        bkps = new HashMap<>();
        for (int i = 0; i < samplesNames.size(); i++) {
            byte previousAllele = snpData.get(0).getGenotypes()[i];
            for (int snp = 1; snp < snpData.size() - 1; snp++) {      
                
                byte allele = snpData.get(snp).getGenotypes()[i];               
                
                if (allele == genMIS) {
                    continue;
                }
                if (allele != previousAllele && allele != genMIS && previousAllele != genMIS) {
                    //this is a breakpoint
                    Integer bkpRowBefore = snp-1;
                    Integer bkpRowAfter = snp;
                    long bkpPosition = (int) round(snpData.get(bkpRowBefore).getPosition() + 0.5 * (snpData.get(bkpRowAfter).getPosition() - snpData.get(bkpRowBefore).getPosition())); 
                    
                    String transitionType = "";
                    //get transition type                   
                    if ((previousAllele == genPA1 || previousAllele == genPA2) && allele == genHTZ) {                                               
                        transitionType = previousAllele == genPA1 ? "A => H" : "B => H";
                    } else if (previousAllele == genHTZ && (allele == genPA1 || allele == genPA2) ) {
                        transitionType = allele == genPA1 ? "H => A" : "H => B";
                    } else if ( (previousAllele == genPA1 && allele == genPA2) || (previousAllele == genPA2 && allele == genPA1) ) {
                        transitionType = previousAllele == genPA1 ? "A => B" : "B => A";
                    }
                    
                    BreakPoint bkp = new BreakPoint(i, chr, bkpPosition, bkpRowBefore, bkpRowAfter, null, 
                            snpData.get(bkpRowBefore).getPosition(), snpData.get(bkpRowAfter).getPosition(), null, null, null, null, null, null, null, 
                            null, null, null, null, transitionType, null, null);   
                    
                    if (bkps.get(i) == null) {
                        bkps.put(i, new ArrayList<>());
                    }
                    bkps.get(i).add(bkp);
                }
                previousAllele = allele;
            }
        }
        System.out.println(bkps.values().stream().mapToInt(List::size).sum());
    }

    /**
     * NOT USED.
     * Another calculation of 
     * @param sampleIndex
     * @param snp3
     * @param popType
     * @param mappingFunction
     * @param nbOfParents
     * @return 
     */
    private double calcF1cM(int sampleIndex, int snp3, PopType popType, MappingFunction mappingFunction, int nbOfParents) {
        double recFrac = 0, F1RecFreq = 0, F1cM = 0;
        int nbAA = 0, nbAB = 0, nbAH = 0, nbBA = 0, nbBB = 0, nbBH = 0, 
                nbMD = 0, nbHA = 0, nbHB = 0, nbHH = 0,Npop = 0;

        for (int i2 = 0; i2 < samplesNames.size() - nbOfParents; i2++) {
            if (i2 != sampleIndex) { //we count genotypes but obviously the chunk itself is ignored
                byte alleleSNP1 = snpData.get(snp3).getGenotypes()[i2];
                byte alleleSNP2 = snpDataCollapsed.get(snp3 + 1).getGenotypes()[i2];
                if (alleleSNP1 == genHTZ) {
                    switch (alleleSNP2) {
                        case genHTZ:
                            nbHH++;
                            break;
                        case genPA1:
                            nbHA++;
                            break;
                        case genPA2:
                            nbHB++;
                            break;
                    }
                } else if (alleleSNP1 == genPA1) {
                    switch (alleleSNP2) {
                        case genHTZ:
                            nbAH++;
                            break;
                        case genPA1:
                            nbAA++;
                            break;
                        case genPA2:
                            nbAB++;
                            break;
                    }
                } else if (alleleSNP1 == genPA2) {
                    switch (alleleSNP2) {
                        case genHTZ:
                            nbBH++;
                            break;
                        case genPA1:
                            nbBA++;
                            break;
                        case genPA2:
                            nbBB++;
                            break;
                    }
                } else {
                    nbMD++;
                }
            }
        }

        if (popType.equals(SnpSelectionCriteria.PopType.F2)) {
            Npop = nbAA + nbBB + nbHH + nbAB + nbBA + nbAH + nbBH + nbHA + nbHB + nbMD;
        } else {
            Npop = nbAA + nbBB + nbAB + nbBA + nbMD;
        }

        if (Npop > 0) { //recombination factor
            switch (popType) {
                case SSD:
                    recFrac = (nbAB + nbBA) / (double) Npop; //for SSD, we could use the htz info like in Calculate_final_map 'this is the rec frac observed in SSD, not converted to F1 value
                    F1RecFreq = recFrac / (double) (2 - 2 * recFrac); //we could use Martin & Hospital correction but that wouldn't change much
                    break;
                case F2:  //EM algorithm
                    F1RecFreq = Functions.RecFreq_F2(nbAA, nbBB, nbHH, nbAB, nbBA, nbAH, nbBH, nbHA, nbHB, Npop);
                    break;
                default:
                    F1RecFreq = (nbAH + nbHA) / Npop;
                    break;
            }
        } else {
            F1RecFreq = 0; //in rare cases, there could be no common data between two consecutive loci
        }

        switch (mappingFunction) {
            case KOSAMBI:
                F1cM = Functions.mapping(true, F1RecFreq);
                break;
            case HALDANE:
                F1cM = Functions.mapping(false, F1RecFreq);
                break;
            case NONE:
                F1cM = 100 * F1RecFreq;
            default:                
                break;
        }
        return F1cM;
        
    }
    
    public void clear() {
        this.samplesNames = new ArrayList<>();
        this.snpData = new ArrayList<>();
        this.snpDataBeforeImputation = new ArrayList<>();  
    }
    
    /**
     * Calculate genotypic frequencies, and number of reads for each sample
     * @return 
     */
    public ArrayList<SampleStats> calcSamplesStats() {
        ArrayList<SampleStats> statsList = new ArrayList();
        for (int i=0; i < samplesNames.size(); i++) {
            int nbA = 0;
            int nbB = 0;
            int nbH = 0;
            int nbMD = 0;
            int nbTrans = 0;
            int sampleReads = 0;
            for (int v = 0; v < snpData.size(); v++) {
                byte allele = snpData.get(v).getGenotypes()[i];
                switch (allele) {
                    case genPA1:
                        nbA++;
                        break;
                    case genPA2:
                        nbB++;
                        break;
                    case genHTZ:
                        nbH++;
                        break;
                    case genMIS:
                        nbMD++;
                        break;
                }
                if (v < snpData.size() - 1) {
                    if (snpData.get(v).getGenotypes()[i] != snpData.get(v+1).getGenotypes()[i] && snpData.get(v).getGenotypes()[i] != genMIS && snpData.get(v+1).getGenotypes()[i] != genMIS) {
                        nbTrans++;
                    }                    
                }
                sampleReads = sampleReads + snpData.get(v).getNbReadsA()[i] + snpData.get(v).getNbReadsB()[i];
            }
            double freqA = (double) nbA / snpData.size();
            double freqB = (double) nbB / snpData.size();
            double freqH = (double) nbH / snpData.size();
            double freqMD = (double) nbMD / snpData.size();
            SampleStats stats = new SampleStats(freqA, freqB, freqH, freqMD, nbTrans, sampleReads);
            statsList.add(stats);
        }
        return statsList;
    }
    
    /**
     * Calculate genotypic frequencies of SNPs and print them in the console
     * @param ssc 
     */
    public void returnSnpSummaryStatistics(SnpSelectionCriteria ssc) {
        double[] MDfrequencies = new double[snpData.size()];
        double[] Afrequencies = new double[snpData.size()];
        double[] Bfrequencies = new double[snpData.size()];
        double[] Hfrequencies = new double[snpData.size()];
        
        for (int i = 0; i < snpData.size(); i++) {
            AlleleCounts act = snpData.get(i).getAlleleCounts();
//            if (act.getNbMD() != 0) {
//                System.out.println("snp " + i);
//            }
            MDfrequencies[i] = (double) act.getNbMD()/act.getTotalCount();
            Afrequencies[i] = (double) act.getNbA()/act.getTotalCount();
            Bfrequencies[i] = (double) act.getNbB()/act.getTotalCount();
            Hfrequencies[i] = (double) act.getNbH()/act.getTotalCount();
        }

        double meanFreqMD = Arrays.stream(MDfrequencies).sum() / MDfrequencies.length;
        double meanFreqA = Arrays.stream(Afrequencies).sum() / Afrequencies.length;
        double meanFreqB = Arrays.stream(Bfrequencies).sum() / Bfrequencies.length;
        double meanFreqH = Arrays.stream(Hfrequencies).sum() / Hfrequencies.length;

        double lowPerMD = Functions.percentile(MDfrequencies, ssc.getLowPercentileMD());
        double highPerMD = Functions.percentile(MDfrequencies, ssc.getHighPercentileMD());
        double lowPerA = Functions.percentile(Afrequencies, ssc.getLowPercentileA());
        double highPerA = Functions.percentile(Afrequencies, ssc.getHighPercentileA());
        double lowPerB = Functions.percentile(Bfrequencies, ssc.getLowPercentileB());
        double highPerB = Functions.percentile(Bfrequencies, ssc.getHighPercentileB());
        double lowPerH = Functions.percentile(Hfrequencies, ssc.getLowPercentileB());
        double highPerH = Functions.percentile(Hfrequencies, ssc.getHighPercentileH());
        
        String format = "%18s | %21s | %21s | %21s | %21s | %n";
        System.out.format("+------------------+-----------------------+-----------------------+-----------------------+-----------------------+%n");
        System.out.format(format,"SUMMARY STATISTICS","Percent_MISS", "Percent_HTZ", "Percent_P1", "Percent_P2");
        System.out.format("+------------------+-----------------------+-----------------------+-----------------------+-----------------------+%n");
        System.out.format(format, "Low percentile", ssc.getLowPercentileMD(), ssc.getLowPercentileH(), ssc.getLowPercentileA(), ssc.getLowPercentileB());
        System.out.format(format, "High percentile", ssc.getHighPercentileMD(), ssc.getHighPercentileH(), ssc.getHighPercentileA(), ssc.getHighPercentileB());
        System.out.format(format, "Expected", 0, 0.5, 0.025, 0.025);
        System.out.format(format, "Average", meanFreqMD, meanFreqH, meanFreqA, meanFreqB);
        System.out.format(format, "Percentile (low)", lowPerMD, lowPerA, lowPerB, lowPerH);
        System.out.format(format, "Percentile (high)", highPerMD, highPerA, highPerB, highPerH);
    }
    
    /**
     * Calculate genotypic frequencies of SNPs and print summary in the console
     * @param ssc 
     */
    public void returnGenotypesSummaryStatistics(SnpSelectionCriteria ssc) {
        ArrayList<SampleStats> sampleStats = calcSamplesStats();
        
        double[] MDfrequencies = new double[samplesNames.size()];
        double[] Afrequencies = new double[samplesNames.size()];
        double[] Bfrequencies = new double[samplesNames.size()];
        double[] Hfrequencies = new double[samplesNames.size()];
        double[] trans = new double[samplesNames.size()];

        for (int s=0; s<samplesNames.size(); s++) {
            SampleStats stats = sampleStats.get(s);
            MDfrequencies[s] = (double) stats.getFreqMD();
            Afrequencies[s] = (double) stats.getFreqA();
            Bfrequencies[s] = (double) stats.getFreqB();
            Hfrequencies[s] = (double) stats.getFreqH();
            trans[s] = (double) stats.getNbTrans();
        }

        double meanFreqMD = Arrays.stream(MDfrequencies).sum() / MDfrequencies.length;
        double meanFreqA = Arrays.stream(Afrequencies).sum() / Afrequencies.length;
        double meanFreqB = Arrays.stream(Bfrequencies).sum() / Bfrequencies.length;
        double meanFreqH = Arrays.stream(Hfrequencies).sum() / Hfrequencies.length;
        double meanTrans = (double) Arrays.stream(trans).sum() / trans.length;

        double lowPerMD = Functions.percentile(MDfrequencies, ssc.getLowPercentileMD());
        double highPerMD = Functions.percentile(MDfrequencies, ssc.getHighPercentileMD());
        double lowPerA = Functions.percentile(Afrequencies, ssc.getLowPercentileA());
        double highPerA = Functions.percentile(Afrequencies, ssc.getHighPercentileA());
        double lowPerB = Functions.percentile(Bfrequencies, ssc.getLowPercentileB());
        double highPerB = Functions.percentile(Bfrequencies, ssc.getHighPercentileB());
        double lowPerH = Functions.percentile(Hfrequencies, ssc.getLowPercentileB());
        double highPerH = Functions.percentile(Hfrequencies, ssc.getHighPercentileH());
        double lowPerTrans = Functions.percentile(trans, ssc.getLowPercentileTrans());
        double highPerTrans = Functions.percentile(trans, ssc.getHighPercentileTrans());

        String format = "%21s | %14s | %14s | %14s | %14s | %14s | %n";
        System.out.format("+---------------------+----------------+----------------+----------------+----------------+----------------+%n");
        System.out.format(format,"GENOTYPES STATISTICS","Percent_MISS", "Percent_HTZ", "Percent_P1", "Percent_P2", "Percent_Trans.");
        System.out.format("+---------------------+----------------+----------------+----------------+----------------+----------------+%n");
        System.out.format(format, "Low percentile", ssc.getLowPercentileMD(), ssc.getLowPercentileH(), ssc.getLowPercentileA(), ssc.getLowPercentileB(), ssc.getLowPercentileTrans());
        System.out.format(format, "High percentile", ssc.getHighPercentileMD(), ssc.getHighPercentileH(), ssc.getHighPercentileA(), ssc.getHighPercentileB(), ssc.getHighPercentileTrans());
        System.out.format(format, "Expected", 0, 0.5, 0.025, 0.025, 0);
        System.out.format(format, "Average", String.format(Locale.US, "%.3f", meanFreqMD), String.format(Locale.US, "%.3f", meanFreqH), String.format(Locale.US, "%.3f", meanFreqA), String.format(Locale.US, "%.3f", meanFreqB), String.format(Locale.US, "%.3f", meanTrans));
        System.out.format(format, "Percentile (low)", String.format(Locale.US, "%.3f", lowPerMD), String.format(Locale.US, "%.3f", lowPerA), String.format(Locale.US, "%.3f", lowPerB), String.format(Locale.US, "%.3f", lowPerH), String.format(Locale.US, "%.3f", lowPerTrans));
        System.out.format(format, "Percentile (high)", String.format(Locale.US, "%.3f", highPerMD), String.format(Locale.US, "%.3f", highPerA), String.format(Locale.US, "%.3f", highPerB), String.format(Locale.US, "%.3f", highPerH), String.format(Locale.US, "%.3f", highPerTrans));
        
    }
    
    /**
     * NOT USED ANYMORE.
     * Update breakpoints list after removing aliens
     * @param chr 
     */
    public void updateBreakPointsAfterAlienRemoval(String chr) {
        for (Integer s:bkps.keySet()) {
            List<BreakPoint> sampleBps = bkps.get(s);
            for (AlienSegment a:aliens) {
                int nbOfBkpInsideTheAlien = 0;
                int bkpIndex = 0;
                for (int bkp = sampleBps.size() - 1; bkp >= 0 ;bkp--) {                    
                    if (a.getStartPosition() <= sampleBps.get(bkp).getAverageBkpPosition() && a.getStopPosition() >= sampleBps.get(bkp).getAverageBkpPosition()) {
                        bkpIndex = bkp;
                        //breakpoint inside Alien
                        nbOfBkpInsideTheAlien++;
                        sampleBps.remove(bkp);
                    }
                }
                if (a.getStart() > 0 && a.getStop() < snpDataCollapsed.size() - 1 && nbOfBkpInsideTheAlien % 2 != 0) {
                    //if nbOfBkpInsideTheAlien is odd, then add a new breakpoint
                    byte allele1 = snpDataCollapsed.get(a.getStart() - 1).getGenotypes()[s];
                    byte allele2 = snpDataCollapsed.get(a.getStop() + 1).getGenotypes()[s];
                    String transitionType = "";
                    if ((allele1 == genPA1 || allele1 == genPA2) && allele2 == genHTZ) {                                               
                        transitionType = allele1 == genPA1 ? "A" : "B";
                        transitionType = transitionType + " => H";

                    } else if (allele1 == genHTZ && (allele2 == genPA1 || allele2 == genPA2) ) {
                        transitionType = allele2 == genPA1 ? "A" : "B";
                        transitionType = "H => " + transitionType;
                    }
                    
                    long startBkpPosition = snpDataCollapsed.get(a.getStart() - 1).getPosition();
                    long stopBkpPosition = snpDataCollapsed.get(a.getStop() + 1).getPosition();
                    long bkpPosition = (long) round(startBkpPosition + 0.5 * (stopBkpPosition - startBkpPosition));
                    BreakPoint newBkp = new BreakPoint(s, chr,                            
                            bkpPosition, 
                            a.getStart() - 1,
                            a.getStop() + 1,
                            false,
                            startBkpPosition, 
                            stopBkpPosition,
                            null,
                            null, 
                            null, 
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            null,
                            transitionType,
                            null, 
                            null);

                    sampleBps.add(bkpIndex, newBkp);                    
                }
            } 
        }
    }
    
    /**
     * Get transitions number for each sample
     */
    public void getTransitionsNb() {
        samplesTransitionsNb = new ArrayList();
        for (int s = 0; s < samplesNames.size(); s++) {
            int transitionNb = 0;
            for (int v = 1; v < snpData.size(); v++) {
                byte prevAllele = snpData.get(v-1).getGenotypes()[s];
                byte allele = snpData.get(v).getGenotypes()[s];
                if (prevAllele != genMIS && allele != prevAllele) {
                    // transition
                    transitionNb++;
                }
            }
            samplesTransitionsNb.add(transitionNb);
        }
    }
    
    /**
     * Get reads number for each sample
     * @return 
     */
    public ArrayList<Long> calcSamplesDP() {
        ArrayList<Long> sampleReads = new ArrayList();
        for (int s = 0; s < samplesNames.size();s++) {
            long reads = 0;
            for (Snp snp:snpData) {
                reads = reads + snp.getNbReadsA()[s] + snp.getNbReadsB()[s];
            }
            sampleReads.add(reads);
        }
        return sampleReads;
    }

    /**
     * Find Loose breakpoint interval (zone where presence of BKP is almost certain).
     * for each snp between k-2m to k+2m, we calculate P(G=B) in a window that goes from the snp (included) to the right (HMZ window)
     * and P(G=H) in a window that goes from the snp(included) to the left (H window)
     * for each snp progressing from k-2m to the right, we stop when (1 - P(G=B))*(1 - P(G=H)) > alpha
     * This defines the start of loose interval
     * we do the same starting from k+2m and progressing to the left to find the end of loose interval
     * @param windowSize
     * @param LB
     * @param RB
     * @param reverse
     * @param sampleIndex
     * @param genHMZ
     * @param alpha
     * @param SSDtype
     * @return 
     */
    private int[] findLooseSupportBkpInterval(int windowSize, int LB, int RB, boolean reverse, int sampleIndex, byte genHMZ, double alpha, boolean SSDtype) {  
        SumIntSlidingWindowMinData leftWindow = new SumIntSlidingWindowMinData(windowSize, 2);
        SumIntSlidingWindowMinData rightWindow = new SumIntSlidingWindowMinData(windowSize, 2);

        ArrayList<Integer> indexes = new ArrayList();
        ArrayList<double[]> probaBH = new ArrayList();

        //initiate first leftwindow (because don't know where to start exactly to have the right number of data)
        int v0 = reverse ? RB : LB;
        int nbOfData = 0;
        List<byte[]> leftWindowList = new ArrayList();
        int sumA = 0;
        int sumB = 0;
        int midEltIndex = reverse ? RB : LB; //initiate first midEltIndex
        for (int v = midEltIndex; reverse ? v < snpData.size() : v > 0; v += reverse ? 1 : -1) {
            byte[] elt = {snpData.get(v).getNbReadsA()[sampleIndex], snpData.get(v).getNbReadsB()[sampleIndex]};                
            leftWindowList.add(elt);
            sumA += snpData.get(v).getNbReadsA()[sampleIndex];
            sumB += snpData.get(v).getNbReadsB()[sampleIndex];
            if (snpData.get(v).getNbReadsA()[sampleIndex] + snpData.get(v).getNbReadsB()[sampleIndex] > 0) {
                nbOfData++;
            }
            if (nbOfData == windowSize) {
                //v0 = v;
                break;
            }

        }
        Collections.reverse(leftWindowList);
        leftWindow.window = new ArrayDeque(leftWindowList);
        leftWindow.nbOfData = nbOfData;
        leftWindow.sum[0] = sumA; leftWindow.sum[1] = sumB;

        byte[] midElt = {snpData.get(midEltIndex).getNbReadsA()[sampleIndex], snpData.get(midEltIndex).getNbReadsB()[sampleIndex]}; 

        for (int v = reverse ? midEltIndex - 1 : midEltIndex + 1; reverse ? v >= LB : v <= RB; v += reverse ? - 1 : 1) {
            double pHMZ = 0;
            double pH = 0;               

            while (rightWindow.nbOfData == windowSize || ((reverse ? v == LB + 1 : v == RB - 1) && (reverse ? midEltIndex >= LB : midEltIndex <= RB))) {
                int lwNbReadsA = leftWindow.sum[0] + snpData.get(midEltIndex).getNbReadsA()[sampleIndex];
                int lwNbReadsB = leftWindow.sum[1] + snpData.get(midEltIndex).getNbReadsB()[sampleIndex];
                int rwNbReadsA = snpData.get(midEltIndex).getNbReadsA()[sampleIndex] + rightWindow.sum[0];
                int rwNbReadsB = snpData.get(midEltIndex).getNbReadsB()[sampleIndex] + rightWindow.sum[1];  

                if (SSDtype) {
                    double pA = ( Math.pow(probaAgivenA, rwNbReadsA) * Math.pow(probaBgivenA, rwNbReadsB) )
                    / ( Math.pow(probaAgivenA, rwNbReadsA) * Math.pow(probaBgivenA, rwNbReadsB)
                        + Math.pow(probaBgivenB, rwNbReadsB) * Math.pow(probaAgivenB, rwNbReadsA)
                    );

                    double pB = ( Math.pow(probaBgivenB, lwNbReadsB) * Math.pow(probaAgivenB, lwNbReadsA) )
                    / ( Math.pow(probaAgivenA, lwNbReadsA) * Math.pow(probaBgivenA, lwNbReadsB)
                        + Math.pow(probaBgivenB, lwNbReadsB) * Math.pow(probaAgivenB, lwNbReadsA)
                    );    
                    pHMZ = pA;
                    pH = pB;               
                } else {
                    if (genHMZ == genPA1) {
                        pHMZ = ( Math.pow(probaAgivenA, rwNbReadsA) * Math.pow(probaBgivenA, rwNbReadsB) )
                            / ( Math.pow(probaAgivenA, rwNbReadsA) * Math.pow(probaBgivenA, rwNbReadsB)
                                + Math.pow(probaAgivenH, rwNbReadsA) * Math.pow(probaBgivenH, rwNbReadsB) 
                            );

                        pH = ( Math.pow(probaAgivenH, lwNbReadsA) * Math.pow(probaBgivenH, lwNbReadsB) )
                            / ( Math.pow(probaAgivenA, lwNbReadsA) * Math.pow(probaBgivenA, lwNbReadsB)
                                + Math.pow(probaAgivenH, lwNbReadsA) * Math.pow(probaBgivenH, lwNbReadsB) 
                            );

                    } else {                                        
                        pHMZ = ( Math.pow(probaBgivenB, rwNbReadsB) * Math.pow(probaAgivenB, rwNbReadsA) )
                                / ( Math.pow(probaBgivenB, rwNbReadsB) * Math.pow(probaAgivenB, rwNbReadsA) 
                                    + Math.pow(probaAgivenH, rwNbReadsA) * Math.pow(probaBgivenH, rwNbReadsB) 
                                );

                        pH = ( Math.pow(probaAgivenH, lwNbReadsA) * Math.pow(probaBgivenH, lwNbReadsB) )
                            / ( Math.pow(probaBgivenB, lwNbReadsB) * Math.pow(probaAgivenB, lwNbReadsA) 
                                + Math.pow(probaAgivenH, lwNbReadsA) * Math.pow(probaBgivenH, lwNbReadsB) 
                            );
                    }
                }

                indexes.add(midEltIndex);
                double[] wProbas = {pHMZ, pH};

                probaBH.add(wProbas);                                    

                //Shift left window to the right
                leftWindow.add(midElt);
                // remove first elt of left windows if too many data
                while (leftWindow.nbOfData > windowSize) {
                    leftWindow.remove();
                }

                //for the end part of the chromosome
                if (!rightWindow.window.isEmpty()) {
                    midElt = rightWindow.remove();
                }

                midEltIndex += reverse ? - 1 : 1;

            }

            //shift right window to the right
            byte[] elt = {snpData.get(v).getNbReadsA()[sampleIndex], snpData.get(v).getNbReadsB()[sampleIndex]};
            if (rightWindow.nbOfData < windowSize) {
                rightWindow.add(elt);                        
            }                               

        }

        int foundLooseInterval = 0;
        int startBkpInterval = reverse ? RB : LB;;  
        int stopBkpInterval = reverse ? LB : RB;; 
        if (!indexes.isEmpty()) {  
            // Looking for start position of breakpoint interval      
            for (int i = 0; i < indexes.size(); i++) {
                //double p = (1 - probaBH.get(i)[0]) * (1 - probaBH.get(i)[1]);
                if ((1 - probaBH.get(i)[0]) * (1 - probaBH.get(i)[1]) > alpha) {
                    startBkpInterval = indexes.get(i);
                    foundLooseInterval = 1;
                    break;
                }
            }
        
            // Looking for stop position of breakpoint interval
            for (int i = indexes.size() - 1; i >= 0; i--) {
                //double p = (1 - probaBH.get(i)[0]) * (1 - probaBH.get(i)[1]);
                if ((1 - probaBH.get(i)[0]) * (1 - probaBH.get(i)[1]) > alpha) {
                    stopBkpInterval = indexes.get(i);
                    break;
                }
            }
        }
        int[] interval = {startBkpInterval, stopBkpInterval, foundLooseInterval};
        return interval;
    }

    private void setProbaErrors() {
        probaAgivenA = 1 - errorA;
        probaBgivenA = errorA;
        probaAgivenB = errorB;
        probaBgivenB = 1 - errorB;
        probaAgivenH = 0.5 * (1 - errorA + errorB);
        probaBgivenH = 0.5 * (1 - errorB + errorA);
    }

    /**
     * Recalculate alleleCounts
     * @param noParent if true, parents will be excluded from alleleCounts
     */
    public void updateAlleleCounts(boolean noParent) {
        for (int v = 0; v < snpData.size(); v++) {
            int nbA = 0, nbB = 0, nbH = 0, nbMD = 0;
            for (int s = 0; s < samplesNames.size(); s++) {
                if (noParent && !parentIndexes.contains(s)) {
                    byte myAllele = snpData.get(v).getGenotypes()[s];
                    switch (myAllele) {
                        case genPA1:
                            nbA++;
                            break;
                        case genPA2:
                            nbB++;
                            break;
                        case genHTZ:
                            nbH++;
                            break;
                        case genMIS:
                            nbMD++;
                            break;
                    }
                }
            }
            snpData.get(v).getAlleleCounts().setNbA(nbA);
            snpData.get(v).getAlleleCounts().setNbB(nbB);
            snpData.get(v).getAlleleCounts().setNbH(nbH);
            snpData.get(v).getAlleleCounts().setNbMD(nbMD);
        }
    }

    /**
     * Calculate the maximum of reads number on sites for each samples
     * @return 
     */
    private List<Integer> calcMaxReadsPerSamples() {
        List<Integer> meanReadsPerSamples = new ArrayList<>();
        for (int s = 0; s < samplesNames.size(); s++) {
            long countReads = 0;
            long countSnpWithData = 0;
            for (int v = 0; v < snpData.size(); v++) {
                if (snpData.get(v).getGenotypes()[s] != genMIS) {
                    countReads += snpData.get(v).getNbReadsA()[s] + snpData.get(v).getNbReadsB()[s]; 
                    countSnpWithData++;
                }
            }
            int maxReads = (int) round((double) countReads / countSnpWithData * 10);
            meanReadsPerSamples.add(maxReads);
        }
        return meanReadsPerSamples;
    }    
    
    /**
     * Remove aliens from raw data.
     * Used to remove aliens before running again imputation.
     */
    public void removeAliens() {
        int startSnp = snpDataBeforeImputation.size() - 1;
        for (int a = aliens.size() - 1; a >= 0; a--) {
            long startAlien = aliens.get(a).getStartPosition();
            long endAlien = aliens.get(a).getStopPosition();

            for (int v = startSnp; v > 0; v--) {           
                long snpPosition = snpDataBeforeImputation.get(v).getPosition();
                if (startAlien <= snpPosition && snpPosition <= endAlien) {
                    //remove this snp
                    snpDataBeforeImputation.remove(v);
                    snpData.remove(v);
                } else if (snpPosition < startAlien) {
                    startSnp = v;
                    break;
                }
            }
        }
    }
    
    public int[] calcTransitionsNbPerSiteParallelized(boolean collapsed) {
        ArrayList<Snp> data;
        if (collapsed) {
            data = snpDataCollapsed;
        } else {
            data = snpData;
        }
        
        int[] transitionsNbPerSite = IntStream.range(0, data.size())
            .parallel()
            .map(v -> {
                int transNb = 0;
                for (int s:bkps.keySet()) {
                    for (BreakPoint bp:bkps.get(s)) {
                        if (data.get(v).getPosition() >= bp.getStartBkpPosition() && data.get(v).getPosition() < bp.getStopBkpPosition()) {
                            transNb++;
                            //System.out.println(snpData.get(v).getPosition() + " | " + transNb);
                        }
                    }
                }
                return transNb;
            })
            .toArray();
        return transitionsNbPerSite;
    }
    
    public int[] calcTransitionsNbPerSiteParallelized() {
        return calcTransitionsNbPerSiteParallelized(false);
    }
    
    public int[] calcTransitionsNbPerSite(boolean collapsed) {
        ArrayList<Snp> data;
        if (collapsed) {
            data = snpDataCollapsed;
        } else {
            data = snpData;
        }
        int[] transitionsNbPerSite = new int[snpData.size()];
        for (int v = 1; v < data.size(); v++) {
            int transNb = 0;
            for (int s = 0; s < samplesNames.size(); s++) {
                if (data.get(v-1).getGenotypes()[s] != data.get(v).getGenotypes()[s]) {
                    transNb++;
                }
            }
            transitionsNbPerSite[v] = transNb;
        }
        return transitionsNbPerSite;
    }
    
}
