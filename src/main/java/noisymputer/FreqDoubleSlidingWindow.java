/*******************************************************************************
 * NOISYmputer
 * Distributed under the GNU Affero General Public License V3.0
 * See <https://www.gnu.org/licenses/agpl-3.0.en.html> for details
 ******************************************************************************/
package noisymputer;

import java.util.ArrayDeque;
import static noisymputer.AllDataInBytes.genHTZ;
import static noisymputer.AllDataInBytes.genMIS;
import static noisymputer.AllDataInBytes.genPA1;
import static noisymputer.AllDataInBytes.genPA2;

/**
 *
 * @author boizet
 */
public class FreqDoubleSlidingWindow {    
    
    ArrayDeque<byte[]> leftWindow;
    ArrayDeque<byte[]> rightWindow;
    AlleleCounts[] leftAlleleCountBySamples;
    AlleleCounts[] rightAlleleCountBySamples;
    ArrayDeque<AlleleCounts> alleleCountBySnpsLeftWindow;
    ArrayDeque<AlleleCounts> alleleCountBySnpsRightWindow;
    ArrayDeque<Long[]> readsCountLeftWindow;
    ArrayDeque<Long[]> readsCountRightWindow;
    
    int halfSize;
    byte[] midSnp = null;
    AlleleCounts midSnpAlleleCounts;
    int midSnpIndex = 0;
    AlleleCounts leftWindowAlleleCounts;
    AlleleCounts rightWindowAlleleCounts;
    
    public FreqDoubleSlidingWindow(int halfSize, int nbSamples){
        this.midSnpAlleleCounts = null;
        this.halfSize = halfSize;
        this.leftWindow = new ArrayDeque();
        this.rightWindow = new ArrayDeque();
        this.leftAlleleCountBySamples = new AlleleCounts[nbSamples];
        this.rightAlleleCountBySamples = new AlleleCounts[nbSamples];
        
        for (int i=0; i < nbSamples; i++) {
            leftAlleleCountBySamples[i] = new AlleleCounts();
            rightAlleleCountBySamples[i] = new AlleleCounts();
        }
        
        this.alleleCountBySnpsLeftWindow = new ArrayDeque<>();
        this.alleleCountBySnpsRightWindow = new ArrayDeque<>();
        this.leftWindowAlleleCounts = new AlleleCounts();
        this.rightWindowAlleleCounts = new AlleleCounts();
    }
    
    public byte[] put(byte[] elt, int v) {
        byte[] removedElt = null;      

        if (elt != null) {
            AlleleCounts eltAlleleCounts = new AlleleCounts(elt);
        
            if (midSnp == null) {
                midSnp = elt;
                midSnpIndex = 0;
                midSnpAlleleCounts = eltAlleleCounts;
            } else {
                if (rightWindow.size() < halfSize) {
                    rightWindow.add(elt);
                    alleleCountBySnpsRightWindow.add(eltAlleleCounts);
                    rightWindowAlleleCounts.updateWindow(new AlleleCounts(), eltAlleleCounts);
                    addSnpToRightAlleleCountsBySample(elt);
                } else {                
                    if (leftWindow.size() < halfSize) {
                        leftWindow.add(midSnp);       
                        addSnpToLeftAlleleCountsBySample(midSnp);
                        alleleCountBySnpsLeftWindow.add(midSnpAlleleCounts);
                        leftWindowAlleleCounts.updateWindow(new AlleleCounts(), midSnpAlleleCounts);
                        midSnpIndex++;
                        midSnp = rightWindow.remove();
                        removeSnpToRightAlleleCountsBySample(midSnp);
                        rightWindow.add(elt);
                        addSnpToRightAlleleCountsBySample(elt);
                        midSnpAlleleCounts = alleleCountBySnpsRightWindow.remove();
                        alleleCountBySnpsRightWindow.add(eltAlleleCounts);
                        rightWindowAlleleCounts.updateWindow(midSnpAlleleCounts, eltAlleleCounts);
                        
                    } else {
                        removedElt = leftWindow.remove();
                        removeSnpToLeftAlleleCountsBySample(removedElt);
                        leftWindow.add(midSnp);
                        addSnpToLeftAlleleCountsBySample(midSnp);
                        midSnp = rightWindow.remove();
                        removeSnpToRightAlleleCountsBySample(midSnp);
                        midSnpIndex++;
                        rightWindow.add(elt);
                        addSnpToRightAlleleCountsBySample(elt);
                        AlleleCounts previousAllCt = alleleCountBySnpsLeftWindow.remove();
                        alleleCountBySnpsLeftWindow.add(midSnpAlleleCounts);
                        leftWindowAlleleCounts.updateWindow(previousAllCt, midSnpAlleleCounts);
                        midSnpAlleleCounts = alleleCountBySnpsRightWindow.remove();
                        alleleCountBySnpsRightWindow.add(eltAlleleCounts);
                        rightWindowAlleleCounts.updateWindow(midSnpAlleleCounts, eltAlleleCounts);
                    }

                }
            
            }
        } else {
            if (leftWindow.size() == halfSize) {
                removedElt = leftWindow.remove();
                removeSnpToLeftAlleleCountsBySample(removedElt);
            }
            
            leftWindow.add(midSnp);
            addSnpToLeftAlleleCountsBySample(midSnp);
            AlleleCounts previousAllCt = alleleCountBySnpsLeftWindow.remove();
            alleleCountBySnpsLeftWindow.add(midSnpAlleleCounts);
            leftWindowAlleleCounts.updateWindow(previousAllCt, midSnpAlleleCounts);
            midSnp = rightWindow.remove();
            removeSnpToRightAlleleCountsBySample(midSnp);
            midSnpAlleleCounts = alleleCountBySnpsRightWindow.remove();
            rightWindowAlleleCounts.updateWindow(midSnpAlleleCounts, new AlleleCounts());
            midSnpIndex++;
            
        }
        
        return removedElt;

    }
    
    

    
    public int getSize() {
        return leftWindow.size() + 1 + rightWindow.size();
    }
    
    public void addSnpToLeftAlleleCountsBySample(byte[] elt) {
        for (int i = 0; i < elt.length ; i++) {
            byte allele = elt[i];
            switch(allele) {
                case genPA1:
                    leftAlleleCountBySamples[i].nbA++;
                    break;
                case genPA2:
                    leftAlleleCountBySamples[i].nbB++;
                    break;
                case genHTZ:
                    leftAlleleCountBySamples[i].nbH++;
                    break;
                case genMIS:
                    leftAlleleCountBySamples[i].nbMD++;
            }
        }
    }
    
    public void removeSnpToLeftAlleleCountsBySample(byte[] removedElt) {
        for (int i = 0; i < removedElt.length ; i++) {
            byte allele = removedElt[i];
            switch(allele) {
                case genPA1:
                    leftAlleleCountBySamples[i].nbA--;
                    break;
                case genPA2:
                    leftAlleleCountBySamples[i].nbB--;
                    break;
                case genHTZ:
                    leftAlleleCountBySamples[i].nbH--;
                    break;
                case genMIS:
                    leftAlleleCountBySamples[i].nbMD--;
            }
        }
    }
    
    public void addSnpToRightAlleleCountsBySample(byte[] elt) {
        for (int i = 0; i < elt.length ; i++) {
            byte allele = elt[i];
            switch(allele) {
                case genPA1:
                    rightAlleleCountBySamples[i].nbA++;
                    break;
                case genPA2:
                    rightAlleleCountBySamples[i].nbB++;
                    break;
                case genHTZ:
                    rightAlleleCountBySamples[i].nbH++;
                    break;
                case genMIS:
                    rightAlleleCountBySamples[i].nbMD++;
            }
            }
        }
    
    public void removeSnpToRightAlleleCountsBySample(byte[] removedElt) {
        for (int i = 0; i < removedElt.length ; i++) {
            byte allele = removedElt[i];
            switch(allele) {
                case genPA1:
                    rightAlleleCountBySamples[i].nbA--;
                    break;
                case genPA2:
                    rightAlleleCountBySamples[i].nbB--;
                    break;
                case genHTZ:
                    rightAlleleCountBySamples[i].nbH--;
                    break;
                case genMIS:
                    rightAlleleCountBySamples[i].nbMD--;
            }
        }
    }

}
