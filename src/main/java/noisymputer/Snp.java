/*******************************************************************************
 * NOISYmputer
 * Distributed under the GNU Affero General Public License V3.0
 * See <https://www.gnu.org/licenses/agpl-3.0.en.html> for details
 ******************************************************************************/
package noisymputer;

import java.util.Arrays;

/**
 *
 * @author boizet
 */
public class Snp {
    private String name;
    private Long position;
    private RefAlleles alleles;
    private byte[] nbReadsA;
    private byte[] nbReadsB;
    private byte[] genotypes;
    private AlleleCounts alleleCounts;
    
    public Snp() {
        super();
    }

    public Snp(String name, Long position, RefAlleles alleles, byte[] nbReadsA, byte[] nbReadsB, byte[] genotypes, AlleleCounts allelesCounts) {
        this.name = name;
        this.position = position;
        this.alleles = alleles;
        this.nbReadsA = nbReadsA;
        this.nbReadsB = nbReadsB;
        this.alleleCounts = allelesCounts;
        this.genotypes = genotypes;
    }    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPosition() {
        return position;
    }

    public void setPosition(Long position) {
        this.position = position;
    }
    
    public RefAlleles getAlleles() {
        return alleles;
    }

    public void setAlleles(RefAlleles alleles) {
        this.alleles = alleles;
    }

    public byte[] getNbReadsA() {
        return nbReadsA;
    }

    public void setNbReadsA(byte[] nbReadsA) {
        this.nbReadsA = nbReadsA;
    }

    public byte[] getNbReadsB() {
        return nbReadsB;
    }

    public void setNbReadsB(byte[] nbReadsB) {
        this.nbReadsB = nbReadsB;
    }

    public byte[] getGenotypes() {
        return genotypes;
    }

    public void setGenotypes(byte[] genotypes) {
        this.genotypes = genotypes;
    }

    public AlleleCounts getAlleleCounts() {
        return alleleCounts;
    }

    public void setAlleleCounts(AlleleCounts alleleCounts) {
        this.alleleCounts = alleleCounts;
    }    

    @Override
    public Object clone() throws CloneNotSupportedException {
        try {
            return (Snp) super.clone();
        } catch (CloneNotSupportedException e) {
            return new Snp(
                    name, 
                    position, 
                    alleles, 
                    Arrays.copyOf(nbReadsA, nbReadsA.length), 
                    Arrays.copyOf(nbReadsB, nbReadsB.length),
                    Arrays.copyOf(genotypes, genotypes.length), 
                    (AlleleCounts) alleleCounts.clone());
        }
    }
    
}
