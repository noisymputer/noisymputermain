/*******************************************************************************
 * NOISYmputer
 * Distributed under the GNU Affero General Public License V3.0
 * See <https://www.gnu.org/licenses/agpl-3.0.en.html> for details
 ******************************************************************************/
package noisymputer;

import java.util.ArrayDeque;

/**
 *
 * @author boizet
 */
public class SumIntSlidingWindowMinData {
    int size;
    ArrayDeque<byte[]> window;
    int[] sum;
    int nbOfData;    
    

    public SumIntSlidingWindowMinData(int size, int nbOfSum) {
        this.size = size;
        this.window = new ArrayDeque<>();
        this.sum = new int[nbOfSum];
        this.nbOfData = 0;
    }
    
    public byte[] put(byte[] elt) {
        byte[] removedElt = null;
        this.add(elt);
        removedElt = this.remove();
        
        return removedElt;
    }    
    
    public int size() {
        return window.size();
    }
    
    public void add(byte[] elt) {
        this.window.add(elt);
        for (int i = 0; i < this.sum.length; i++) {
            this.sum[i] = this.sum[i] + elt[i];
        }
        if (elt[0] + elt[1] > 0) {
            nbOfData++;
        }
    }
    
    public byte[] remove() {
        byte[] removedElt = this.window.remove();
        for (int i = 0; i < this.sum.length; i++) {
            this.sum[i] = this.sum[i] - removedElt[i];
        }
        if (removedElt[0] + removedElt[1] > 0) {
            nbOfData--;
        }
        return removedElt;        
    }
}
