/*******************************************************************************
 * NOISYmputer
 * Distributed under the GNU Affero General Public License V3.0
 * See <https://www.gnu.org/licenses/agpl-3.0.en.html> for details
 ******************************************************************************/
package noisymputer.Exceptions;

/**
 *
 * @author boizet
 */
public class EmptySnpListException extends Exception {
    public EmptySnpListException() {
        super("0 SNPs kept, stopping the program");
    }
}
