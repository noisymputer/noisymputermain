/*******************************************************************************
 * NOISYmputer
 * Distributed under the GNU Affero General Public License V3.0
 * See <https://www.gnu.org/licenses/agpl-3.0.en.html> for details
 ******************************************************************************/
package noisymputer.Exceptions;

/**
 *
 * @author boizet
 */
public class WindowSizeException extends Exception {
    public WindowSizeException(int windowSize, int snpsNumber) {
        super("windowSize too big compared to number of snps, windowSize=" + windowSize + ", " + snpsNumber + " snps");
    }
}