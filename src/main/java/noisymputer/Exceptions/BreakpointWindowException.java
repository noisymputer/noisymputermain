/*******************************************************************************
 * NOISYmputer
 * Distributed under the GNU Affero General Public License V3.0
 * See <https://www.gnu.org/licenses/agpl-3.0.en.html> for details
 ******************************************************************************/
package noisymputer.Exceptions;

/**
 *
 * @author boizet
 */
public class BreakpointWindowException extends RuntimeException {
    private String sample;
    private long snpsPosition;
    
    public BreakpointWindowException(String sample, long snpsPosition) {
        super("Impossible to calculate breakpoint window probas because there is not enough data inside the window, around the snp " + snpsPosition + " for the sample " + sample);
    }

    BreakpointWindowException(BreakpointWindowException ex) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public String getSample() {
        return sample;
    }

    public void setSample(String sample) {
        this.sample = sample;
    }

    public long getSnpsPosition() {
        return snpsPosition;
    }

    public void setSnpsPosition(long snpsPosition) {
        this.snpsPosition = snpsPosition;
    }
    
}