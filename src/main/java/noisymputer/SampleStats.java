/*******************************************************************************
 * NOISYmputer
 * Distributed under the GNU Affero General Public License V3.0
 * See <https://www.gnu.org/licenses/agpl-3.0.en.html> for details
 ******************************************************************************/
package noisymputer;

/**
 *
 * @author boizet
 */
public class SampleStats {    
    private double freqA;
    private double freqB;
    private double freqH;
    private double freqMD;
    private int nbTrans;
    private int reads;

    public SampleStats() {
        this.freqA = 0;
        this.freqB = 0;
        this.freqH = 0;
        this.freqMD = 0;
        this.nbTrans = 0;
        this.reads = 0;
    }

    public SampleStats(double freqA, double freqB, double freqH, double freqMD, int nbTrans, int reads) {
        this.freqA = freqA;
        this.freqB = freqB;
        this.freqH = freqH;
        this.freqMD = freqMD;
        this.nbTrans = nbTrans;
        this.reads = reads;
    }

    public double getFreqA() {
        return freqA;
    }

    public void setFreqA(double freqA) {
        this.freqA = freqA;
    }

    public double getFreqB() {
        return freqB;
    }

    public void setFreqB(double freqB) {
        this.freqB = freqB;
    }

    public double getFreqH() {
        return freqH;
    }

    public void setFreqH(double freqH) {
        this.freqH = freqH;
    }

    public double getFreqMD() {
        return freqMD;
    }

    public void setFreqMD(double freqMD) {
        this.freqMD = freqMD;
    }

    public int getNbTrans() {
        return nbTrans;
    }

    public void setNbTrans(int nbTrans) {
        this.nbTrans = nbTrans;
    }

    public int getReads() {
        return reads;
    }

    public void setReads(int reads) {
        this.reads = reads;
    }
    
}
