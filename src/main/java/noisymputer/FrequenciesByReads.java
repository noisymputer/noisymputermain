/*******************************************************************************
 * NOISYmputer
 * Distributed under the GNU Affero General Public License V3.0
 * See <https://www.gnu.org/licenses/agpl-3.0.en.html> for details
 ******************************************************************************/
package noisymputer;

/**
 *
 * @author boizet
 */
public class FrequenciesByReads {
    
    private double reads;
    private double minFreqA;
    private double minFreqB;
    private double minFreqH;
    private double maxFreqA;
    private double maxFreqB;
    private double maxFreqH;

    public FrequenciesByReads(double reads, double minFreqA, double minFreqB, double minFreqH, double maxFreqA, double maxFreqB, double maxFreqH) {
        this.reads = reads;
        this.minFreqA = minFreqA;
        this.minFreqB = minFreqB;
        this.minFreqH = minFreqH;
        this.maxFreqA = maxFreqA;
        this.maxFreqB = maxFreqB;
        this.maxFreqH = maxFreqH;
    }
    
    

    public double getReads() {
        return reads;
    }

    public void setReads(double reads) {
        this.reads = reads;
    }

    public double getMinFreqA() {
        return minFreqA;
    }

    public void setMinFreqA(double minFreqA) {
        this.minFreqA = minFreqA;
    }

    public double getMinFreqB() {
        return minFreqB;
    }

    public void setMinFreqB(double minFreqB) {
        this.minFreqB = minFreqB;
    }

    public double getMinFreqH() {
        return minFreqH;
    }

    public void setMinFreqH(double minFreqH) {
        this.minFreqH = minFreqH;
    }

    public double getMaxFreqA() {
        return maxFreqA;
    }

    public void setMaxFreqA(double maxFreqA) {
        this.maxFreqA = maxFreqA;
    }

    public double getMaxFreqB() {
        return maxFreqB;
    }

    public void setMaxFreqB(double maxFreqB) {
        this.maxFreqB = maxFreqB;
    }

    public double getMaxFreqH() {
        return maxFreqH;
    }

    public void setMaxFreqH(double maxFreqH) {
        this.maxFreqH = maxFreqH;
    }
    
    
    
}
