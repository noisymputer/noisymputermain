/*******************************************************************************
 * NOISYmputer
 * Distributed under the GNU Affero General Public License V3.0
 * See <https://www.gnu.org/licenses/agpl-3.0.en.html> for details
 ******************************************************************************/
package noisymputer;

import java.util.ArrayDeque;

/**
 *
 * @author boizet
 */
public class SumLongSlidingWindow {
    int size;
    ArrayDeque<long[]> window;
    long[] sum;

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public ArrayDeque<long[]> getWindow() {
        return window;
    }

    public void setWindow(ArrayDeque<long[]> window) {
        this.window = window;
    }

    public long[] getSum() {
        return sum;
    }

    public void setSum(long[] sum) {
        this.sum = sum;
    }
    
    

    public SumLongSlidingWindow(int size, int nbOfSum) {
        this.size = size;
        this.window = new ArrayDeque<>();
        this.sum = new long[nbOfSum];
    }
    
    public long[] put(long[] elt) {
        long[] removedElt = null;
        if (elt != null) {
            if (this.window.size() == this.size) {
                removedElt = this.window.remove();
                for (int i = 0; i < this.sum.length; i++) {
                    this.sum[i] = this.sum[i] - removedElt[i];
                }
            }
        
            this.window.add(elt);
            for (int i = 0; i < this.sum.length; i++) {
                this.sum[i] = this.sum[i] + elt[i];
            }
        } else {
            removedElt = this.window.remove();
        }
        
        return removedElt;
    }    
    
    public int size() {
        return window.size();
    }
    
}
