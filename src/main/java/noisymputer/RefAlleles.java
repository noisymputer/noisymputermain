/*******************************************************************************
 * NOISYmputer
 * Distributed under the GNU Affero General Public License V3.0
 * See <https://www.gnu.org/licenses/agpl-3.0.en.html> for details
 ******************************************************************************/
package noisymputer;

import java.util.Arrays;

/**
 *
 * @author boizet
 */
public class RefAlleles {
    
    private String[] refAltAlleles;
    private String parent1Allele;
    private String parent2Allele;
    
    public RefAlleles() {
        super();
    }

    public RefAlleles(String[] refAltAlleles, String parent1Allele, String parent2Allele) {
        this.refAltAlleles = refAltAlleles;
        this.parent1Allele = parent1Allele;
        this.parent2Allele = parent2Allele;
    }
    
    

    public String[] getRefAltAlleles() {
        return refAltAlleles;
    }

    public void setRefAltAlleles(String[] refAltAlleles) {
        this.refAltAlleles = refAltAlleles;
    }

    public String getParent1Allele() {
        return parent1Allele;
    }

    public void setParent1Allele(String parent1Allele) {
        this.parent1Allele = parent1Allele;
    }

    public String getParent2Allele() {
        return parent2Allele;
    }

    public void setParent2Allele(String parent2Allele) {
        this.parent2Allele = parent2Allele;
    }
    
    @Override
    public Object clone() throws CloneNotSupportedException {
        try {
            return (RefAlleles) super.clone();
        } catch (CloneNotSupportedException e) {
            return new RefAlleles(
                    Arrays.copyOf(refAltAlleles, refAltAlleles.length),
                    parent1Allele, 
                    parent2Allele);
        }
    }
    
}
