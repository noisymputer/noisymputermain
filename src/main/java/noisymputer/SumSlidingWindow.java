/*******************************************************************************
 * NOISYmputer
 * Distributed under the GNU Affero General Public License V3.0
 * See <https://www.gnu.org/licenses/agpl-3.0.en.html> for details
 ******************************************************************************/
package noisymputer;

import static java.lang.Math.abs;
import java.util.ArrayDeque;

/**
 *
 * @author boizet
 */
public class SumSlidingWindow {
    int size;
    int nbOfData;
    ArrayDeque<double[]> window;
    double[] sum;
    

    public SumSlidingWindow(int size, int nbOfSum) {
        this.size = size;
        this.window = new ArrayDeque<>();
        this.sum = new double[nbOfSum];
        this.nbOfData = 0;
    }
    
    public double[] put(double[] elt) {
        double[] removedElt = null;
        this.add(elt);
        removedElt = this.remove();
        
        return removedElt;
    }    
    
    public int size() {
        return window.size();
    }
    
    public void add(double[] elt) {
        this.window.add(elt);
        for (int i = 0; i < this.sum.length; i++) {
            this.sum[i] = this.sum[i] + elt[i];
        }
        if (eltIsNotNull(elt)) {
            nbOfData++;
        }
    }
    
    public double[] remove() {
        double[] removedElt = this.window.remove();
        for (int i = 0; i < this.sum.length; i++) {
            this.sum[i] = this.sum[i] - removedElt[i];
        }
        if (eltIsNotNull(removedElt)) {
            nbOfData--;
        }
        return removedElt;        
    }
    
    private boolean eltIsNotNull(double[] elt) {
        int sumOfArray = 0;
        for (int i = 0; i < elt.length ;i++) {
            sumOfArray += abs(elt[i]);
        }
        return sumOfArray > 0;
    }
}
