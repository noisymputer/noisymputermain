/*******************************************************************************
 * NOISYmputer
 * Distributed under the GNU Affero General Public License V3.0
 * See <https://www.gnu.org/licenses/agpl-3.0.en.html> for details
 ******************************************************************************/
package noisymputer;

import static java.lang.Math.log;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static noisymputer.AllDataInBytes.genHTZ;
import static noisymputer.AllDataInBytes.genMIS;
import static noisymputer.AllDataInBytes.genPA1;
import static noisymputer.AllDataInBytes.genPA2;
import org.apache.commons.math.stat.descriptive.rank.Percentile;
import org.apache.commons.math3.distribution.ChiSquaredDistribution;

/**
 *
 * @author boizet
 */
public class Functions {
    
    public static Double percentile(List<Double> values, double percentile) {
        int index = (int) Math.ceil(percentile * values.size());
        return values.get(index-1);
    }
    
    public static Double percentile(double[] values, double percentile) {
        return new Percentile().evaluate(values, percentile*100);
    }
    
    public static double RecFreq_F2(int nbAA, int nbBB, int nbHH, int nbAB, int nbBA, int nbAH, int nbBH, int nbHA, int nbHB, int Npop) {
        //recombination fraction is not directly observed in F2s, so we use an EM algortihm
        double RecFreq_F2;
        if (Npop > 0) { 
            double invNpop = (double) 1/Npop;
            double p = (double) (2 * (nbAB + nbBA) + (nbAH + nbHA + nbHB + nbBH) / 2) * invNpop;
            double adjust = 1;
            double pNew = 0;
            while (adjust > 0.0001) {
                double t = (double) Math.pow(p,2) / (Math.pow(1 - p, 2) + Math.pow(p,2));
                pNew = (double) (2 * (nbAB + nbBA) + (nbAH + nbHA + nbHB + nbBH) + 2 * t * nbHH) * invNpop * 0.5;
                adjust = Math.abs((pNew - p));
                p = pNew;
            }
            RecFreq_F2 = pNew;
        } else {
            RecFreq_F2 = 0;
        }

        return RecFreq_F2;        
    }

    //'r2021 (Lorieux M., 2021. Direct fast estimator for recombination frequency in the F2 cross. BioRxiv)
    public static double RecFreq_F2_2021(int nbAA, int nbBB, int nbHH, int nbAB, int nbBA, int nbAH, int nbBH, int nbHA, int nbHB, int Npop) {
        long xR = nbAB + nbBA;
        long xH = nbAH + nbHA + nbBH + nbHB;
        double xD = 1 / (double) (2 * Npop);
        double xE = 4 * xD * xR + xD * xH;
        double xE_sqrd = xE * xE;
        
        double RecFreq_F2_2021;
        if (Npop > 0) {        
            RecFreq_F2_2021 = xD * (2 * xR + xH + 2 * nbHH * xE_sqrd / (double) (1 - 2 * xE + 2 * xE_sqrd));
        } else {
            RecFreq_F2_2021 = 0;
        }
        return RecFreq_F2_2021;        
    }

    /**
     * 
     * @param useKosambiFunction = false if Haldane, = true if Kosambi 
     * @param recombFraction
     * @return 
     */
    public static double mapping(boolean useKosambiFunction, double recombFraction) {       
        double result;
        if (recombFraction == -1) {
            result = -1;
        } else if (recombFraction == 0) {
            result = 0;
        } else if (recombFraction < 0.499) {
            if (useKosambiFunction) {
                result = 25 * log((1 + 2 * recombFraction) / (1 - 2 * recombFraction));
            } else {
                result = -50 * log(1 - 2 * recombFraction);
            }            
        } else {
            if (useKosambiFunction) {
                result = 25 * log((1 + 2 * 0.499) / (1 - 2 * 0.499));
            } else {
                result = -50 * log(1 - 2 * 0.499);
            }
        }
        return result;
    }

    public static String[] convertBytesToABH(byte[] snp) {
        Map<Byte, String> valuesMap = new HashMap<>();
        valuesMap.put(genPA1, "A");
        valuesMap.put(genPA2, "B");
        valuesMap.put(genHTZ, "H");
        valuesMap.put(genMIS, "-");

        String[] snpInABH = new String[snp.length];
        for (int i=0; i < snp.length; i++) {                
            snpInABH[i] = valuesMap.get(snp[i]);
        }
        return snpInABH;            
    }
    
    public static String convertByteToFlapJack(byte gt) {
        Map<Byte, String> valuesMap = new HashMap<>();
        valuesMap.put(genPA1, "G");
        valuesMap.put(genPA2, "T");
        valuesMap.put(genHTZ, "A");
        valuesMap.put(genMIS, "-");
        return valuesMap.get(gt);            
    }
    
    public static int getMaxNbOfSuccessiveHmzInH(double bInHProba, double alpha) {       
        int n = 0;
        double proba = 1;
        while (proba >= alpha) {
            n++;
            proba = (double) bInHProba*proba;
        }
        return n;
    }
    
    public static double getChi2Threshold(double degreesOfFreedom, double alpha) {
        ChiSquaredDistribution chi2 = new ChiSquaredDistribution(degreesOfFreedom);
        return chi2.inverseCumulativeProbability(1 - alpha);
    }
}
