/*******************************************************************************
 * NOISYmputer
 * Distributed under the GNU Affero General Public License V3.0
 * See <https://www.gnu.org/licenses/agpl-3.0.en.html> for details
 ******************************************************************************/
package noisymputer;

/**
 *
 * @author boizet
 */
public class ReadsCount {
    int nbReadsA = 0;
    int nbReadsB = 0;

    public ReadsCount(int nbReadsA, int nbReadsB) {
        this.nbReadsA = nbReadsA;
        this.nbReadsB = nbReadsB;
    }
    
    public ReadsCount(Snp snp) {        
       this.nbReadsA = 0;
        this.nbReadsB = 0;        
        for (int s = 0; s < snp.getNbReadsA().length; s++) {
            this.nbReadsA += snp.getNbReadsA()[s];
            this.nbReadsB += snp.getNbReadsB()[s];
        }
    }

    public int getNbReadsA() {
        return nbReadsA;
    }

    public void setNbReadsA(int nbReadsA) {
        this.nbReadsA = nbReadsA;
    }

    public int getNbReadsB() {
        return nbReadsB;
    }

    public void setNbReadsB(int nbReadsB) {
        this.nbReadsB = nbReadsB;
    }
    
    
}
