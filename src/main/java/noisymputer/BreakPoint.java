/*******************************************************************************
 * NOISYmputer
 * Distributed under the GNU Affero General Public License V3.0
 * See <https://www.gnu.org/licenses/agpl-3.0.en.html> for details
 ******************************************************************************/
package noisymputer;

/**
 *
 * @author boizet
 */
public class BreakPoint {
    private int sampleNumber;  
    private String chr;
    private Long averageBkpPosition;
    private Integer bkpRowBefore;
    private Integer bkpRowAfter;
    Boolean foundBkp;
    private Long startBkpPosition;
    private Long stopBkpPosition;
    private Long bkpLength;    
    private Long startLooseInterval; 
    private Long stopLooseInterval;
    private Boolean foundLooseInterval;
    private Integer snpsNbInLooseInterval;
    private Integer dataNbInLooseInterval;
    private Long startSupportInterval;
    private Long stopSupportInterval;    
    private Integer snpsNbInSupportInterval;
    private Integer dataNbInSupportInterval;
    private Integer snpsNbInHMZWindow;
    private Integer snpsNbInHWindow;
    private String transitionType;
    private Long endPrevZone;
    private Long startZone;

    public BreakPoint(
            int sampleNumber, 
            String chr, 
            Long averageBkpPosition, 
            Integer bkpRowBefore,
            Integer bkpRowAfter,
            Boolean foundBkp,
            Long startBkpPosition, 
            Long stopBkpPosition, 
            Long startLooseInterval, 
            Long stopLooseInterval, 
            Boolean foundLooseInterval,
            Integer snpsNbInLooseInterval,
            Integer dataNbInLooseInterval,
            Long startSupportInterval,
            Long stopSupportInterval,
            Integer snpsNbInSupportInterval,
            Integer dataNbInSupportInterval,
            Integer snpsNbInHMZWindow,
            Integer snpsNbInHWindow,
            String transitionType,
            Long endPrevZone,
            Long startZone) {
        this.sampleNumber = sampleNumber;
        this.chr = chr;
        this.averageBkpPosition = averageBkpPosition;
        this.startBkpPosition = startBkpPosition;
        this.stopBkpPosition = stopBkpPosition;        
        this.bkpLength = stopBkpPosition - startBkpPosition;
        this.startLooseInterval = startLooseInterval;
        this.stopLooseInterval = stopLooseInterval;
        this.snpsNbInLooseInterval = snpsNbInLooseInterval;
        this.dataNbInLooseInterval = dataNbInLooseInterval;
        this.startSupportInterval = startSupportInterval;
        this.stopSupportInterval = stopSupportInterval;
        this.snpsNbInSupportInterval = snpsNbInSupportInterval;
        this.dataNbInSupportInterval = dataNbInSupportInterval;
        this.snpsNbInHMZWindow = snpsNbInHMZWindow;
        this.snpsNbInHWindow = snpsNbInHWindow;
        this.dataNbInSupportInterval = dataNbInSupportInterval;
        this.transitionType = transitionType;
        this.bkpRowBefore = bkpRowBefore;
        this.bkpRowAfter = bkpRowAfter;
        this.endPrevZone = endPrevZone;
        this.startZone = startZone;
        this.foundLooseInterval = foundLooseInterval;
        this.foundBkp = foundBkp;
    }

    public int getSampleNumber() {
        return sampleNumber;
    }

    public void setSampleNumber(int sampleNumber) {
        this.sampleNumber = sampleNumber;
    }

    public String getChr() {
        return chr;
    }

    public void setChr(String chr) {
        this.chr = chr;
    }

    public Long getAverageBkpPosition() {
        return averageBkpPosition;
    }

    public void setAverageBkpPosition(Long averageBkpPosition) {
        this.averageBkpPosition = averageBkpPosition;
    }

    public Long getStartBkpPosition() {
        return startBkpPosition;
    }

    public void setStartBkpPosition(Long startBkpPosition) {
        this.startBkpPosition = startBkpPosition;
    }

    public Long getStopBkpPosition() {
        return stopBkpPosition;
    }

    public void setStopBkpPosition(Long stopBkpPosition) {
        this.stopBkpPosition = stopBkpPosition;
    }

    public Long getBkpLength() {
        return bkpLength;
    }

    public void setBkpLength(Long bkpLength) {
        this.bkpLength = bkpLength;
    }

    public Long getStartLooseInterval() {
        return startLooseInterval;
    }

    public void setStartLooseInterval(Long startLooseInterval) {
        this.startLooseInterval = startLooseInterval;
    }

    public Long getStopLooseInterval() {
        return stopLooseInterval;
    }

    public void setStopLooseInterval(Long stopLooseInterval) {
        this.stopLooseInterval = stopLooseInterval;
    }

    public Long getStartSupportInterval() {
        return startSupportInterval;
    }

    public void setStartSupportInterval(Long startSupportInterval) {
        this.startSupportInterval = startSupportInterval;
    }

    public Long getStopSupportInterval() {
        return stopSupportInterval;
    }

    public void setStopSupportInterval(Long stopSupportInterval) {
        this.stopSupportInterval = stopSupportInterval;
    }

    public Integer getSnpsNbInHMZWindow() {
        return snpsNbInHMZWindow;
    }

    public void setSnpsNbInHMZWindow(Integer snpsNbInHMZWindow) {
        this.snpsNbInHMZWindow = snpsNbInHMZWindow;
    }

    public Integer getSnpsNbInHWindow() {
        return snpsNbInHWindow;
    }

    public void setSnpsNbInHWindow(Integer snpsNbInHWindow) {
        this.snpsNbInHWindow = snpsNbInHWindow;
    }

    public String getTransitionType() {
        return transitionType;
    }

    public void setTransitionType(String transitionType) {
        this.transitionType = transitionType;
    }

    public Integer getBkpRowBefore() {
        return bkpRowBefore;
    }

    public void setBkpRowBefore(Integer bkpRowBefore) {
        this.bkpRowBefore = bkpRowBefore;
    }

    public Integer getBkpRowAfter() {
        return bkpRowAfter;
    }

    public void setBkpRowAfter(Integer bkpRowAfter) {
        this.bkpRowAfter = bkpRowAfter;
    }    

    public Integer getSnpsNbInLooseInterval() {
        return snpsNbInLooseInterval;
    }

    public void setSnpsNbInLooseInterval(Integer snpsNbInLooseInterval) {
        this.snpsNbInLooseInterval = snpsNbInLooseInterval;
    }

    public Integer getDataNbInLooseInterval() {
        return dataNbInLooseInterval;
    }

    public void setDataNbInLooseInterval(Integer dataNbInLooseInterval) {
        this.dataNbInLooseInterval = dataNbInLooseInterval;
    }

    public Integer getSnpsNbInSupportInterval() {
        return snpsNbInSupportInterval;
    }

    public void setSnpsNbInSupportInterval(Integer snpsNbInSupportInterval) {
        this.snpsNbInSupportInterval = snpsNbInSupportInterval;
    }

    public Integer getDataNbInSupportInterval() {
        return dataNbInSupportInterval;
    }

    public void setDataNbInSupportInterval(Integer dataNbInSupportInterval) {
        this.dataNbInSupportInterval = dataNbInSupportInterval;
    }

    public Long getEndPrevZone() {
        return endPrevZone;
    }

    public void setEndPrevZone(Long endPrevZone) {
        this.endPrevZone = endPrevZone;
    }

    public Long getStartZone() {
        return startZone;
    }

    public void setStartZone(Long startZone) {
        this.startZone = startZone;
    }

    public Boolean getFoundLooseInterval() {
        return foundLooseInterval;
    }

    public void setFoundLooseInterval(Boolean foundLooseInterval) {
        this.foundLooseInterval = foundLooseInterval;
    }

    public Boolean getFoundBkp() {
        return foundBkp;
    }

    public void setFoundBkp(Boolean foundBkp) {
        this.foundBkp = foundBkp;
    }    

}
