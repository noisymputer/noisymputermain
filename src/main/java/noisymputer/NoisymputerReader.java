/*******************************************************************************
 * NOISYmputer
 * Distributed under the GNU Affero General Public License V3.0
 * See <https://www.gnu.org/licenses/agpl-3.0.en.html> for details
 ******************************************************************************/
package noisymputer;

import java.io.IOException;

/**
 *
 * @author boizet
 */
public interface NoisymputerReader {
    
    public String getSnps() throws IOException, Exception;

}