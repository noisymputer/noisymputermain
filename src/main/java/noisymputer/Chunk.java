/*******************************************************************************
 * NOISYmputer
 * Distributed under the GNU Affero General Public License V3.0
 * See <https://www.gnu.org/licenses/agpl-3.0.en.html> for details
 ******************************************************************************/
package noisymputer;

/**
 *
 * @author boizet
 */
public class Chunk extends Zone {
    private byte newGenotype;
    private boolean corrected;
    private double dDB;
    private double dBE;
    private double rDBE;
    private int D, A, B, C, E; 

    public Chunk(byte newGenotype, boolean corrected, double dDB, double dBE, double rDBE, byte genotype, int start, int end, int D, int A, int B, int C, int E) {
        super(genotype, start, end, 0, 0);
        this.newGenotype = newGenotype;
        this.corrected = corrected;
        this.dDB = dDB;
        this.dBE = dBE;
        this.rDBE = rDBE;
        this.D = D;
        this.A = A;
        this.B = B;
        this.C = C;
        this.E = E;
    }    

    public byte getNewGenotype() {
        return newGenotype;
    }

    public void setNewGenotype(byte newGenotype) {
        this.newGenotype = newGenotype;
    }

    public boolean isCorrected() {
        return corrected;
    }

    public void setCorrected(boolean corrected) {
        this.corrected = corrected;
    }

    public double getdDB() {
        return dDB;
    }

    public void setdDB(long dDB) {
        this.dDB = dDB;
    }

    public double getdBE() {
        return dBE;
    }

    public void setdBE(long dBE) {
        this.dBE = dBE;
    }

    public double getrDBE() {
        return rDBE;
    }

    public void setrDBE(double rDBE) {
        this.rDBE = rDBE;
    }

    public int getD() {
        return D;
    }

    public void setD(int D) {
        this.D = D;
    }

    public int getA() {
        return A;
    }

    public void setA(int A) {
        this.A = A;
    }

    public int getB() {
        return B;
    }

    public void setB(int B) {
        this.B = B;
    }

    public int getC() {
        return C;
    }

    public void setC(int C) {
        this.C = C;
    }

    public int getE() {
        return E;
    }

    public void setE(int E) {
        this.E = E;
    }    
    
}
