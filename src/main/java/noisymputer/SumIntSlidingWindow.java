/*******************************************************************************
 * NOISYmputer
 * Distributed under the GNU Affero General Public License V3.0
 * See <https://www.gnu.org/licenses/agpl-3.0.en.html> for details
 ******************************************************************************/
package noisymputer;

import java.util.ArrayDeque;

/**
 *
 * @author boizet
 */
public class SumIntSlidingWindow {
    int size;
    ArrayDeque<byte[]> window;
    int[] sum;

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public ArrayDeque<byte[]> getWindow() {
        return window;
    }

    public void setWindow(ArrayDeque<byte[]> window) {
        this.window = window;
    }

    public int[] getSum() {
        return sum;
    }

    public void setSum(int[] sum) {
        this.sum = sum;
    }
    
    

    public SumIntSlidingWindow(int size, int nbOfSum) {
        this.size = size;
        this.window = new ArrayDeque<>();
        this.sum = new int[nbOfSum];
    }
    
    public byte[] put(byte[] elt) {
        byte[] removedElt = null;
        if (this.window.size() == this.size) {
            removedElt = this.window.remove();
            for (int i = 0; i < this.sum.length; i++) {
                this.sum[i] = this.sum[i] - removedElt[i];
            }
        }
        
        this.window.add(elt);
        for (int i = 0; i < this.sum.length; i++) {
                this.sum[i] = this.sum[i] + elt[i];
            }
        
        return removedElt;
    }    
    
    public int size() {
        return window.size();
    }
    
}
