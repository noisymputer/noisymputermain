package test;

import java.util.ArrayDeque;

/**
 *
 * @author boizet
 */
public class SimpleDoubleSlidingWindow {
    
    static final byte genPA1 = 1;
    static final byte genPA2 = 2;
    static final byte genHTZ = 3;
    static final byte genMIS = 9;   
    
    
    ArrayDeque<byte[]> leftWindow;
    ArrayDeque<byte[]> rightWindow;
    int halfSize;
    byte[] midSnp = null;
    
    public SimpleDoubleSlidingWindow(int halfSize, int nbSamples){
        this.halfSize = halfSize;
        this.leftWindow = new ArrayDeque();
        this.rightWindow = new ArrayDeque();
    }
    
    public byte[] put(byte[] elt) {
        byte[] removedElt = null;
        
        if (leftWindow.size() < halfSize) {
            leftWindow.add(elt);
            //alleleCountBySnpsLeftWindow.add(eltAlleleCounts);
        } else if (leftWindow.size() == halfSize && rightWindow.size() < halfSize) {
            if (midSnp == null && rightWindow.isEmpty()) {
                midSnp = elt;
            } else {            
                rightWindow.add(elt);
                //alleleCountBySnpsRightWindow.add(eltAlleleCounts);
            }
        } else if (leftWindow.size() == halfSize && rightWindow.size() == halfSize) {
            removedElt = leftWindow.remove();
            leftWindow.add(midSnp);
            midSnp = rightWindow.remove();
            rightWindow.add(elt);
        }
        
        return removedElt;

    }
    
    public int getSize() {
        return leftWindow.size() + 1 + rightWindow.size();
    }
}
