package test;

//package fr.cirad.noisymputer;
//
//import htsjdk.tribble.AbstractFeatureReader;
//import htsjdk.tribble.FeatureReader;
//import htsjdk.variant.variantcontext.VariantContext;
//import htsjdk.variant.vcf.VCFCodec;
//import java.io.BufferedWriter;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.net.MalformedURLException;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Iterator;
//import java.util.LinkedHashSet;
//import java.util.List;
//
///**
// *
// * @author boizet
// */
//public class NoisymputerMain {
//    
//    public static void main(String[] args) throws MalformedURLException, IOException, Exception {
//        final long startTime = System.currentTimeMillis();
//       
//        String mainFileUrl = "/home/boizet/Dropbox/NOISYmputer_project/Example_files/HC_50ind_control.vcf";
//        //String mainFileUrl = "/home/boizet/Documents/data_test/test.vcf";
//        String arrayDequeOutputFile = "/home/boizet/Documents/data_test/export_doubleWindowArrayDeque_all.csv";
//        String arrayOutputFile = "/home/boizet/Documents/data_test/export_windowArray_all.csv";
//        SnpSelectionCriteria criterias = new SnpSelectionCriteria();
//
//        criterias.setParent1("kitaake");
//        criterias.setParent2("kalinga");
//        //criterias.setParent1("P1");
//        //criterias.setParent2("P2");
//        criterias.setChromosome("chr1");
//        //criterias.setChromosome("20");
//        criterias.setPopType("SSD");
//        int windowSize = 15;
//        
//
//        VCFCodec vc = new VCFCodec();
//        FeatureReader<VariantContext> reader = AbstractFeatureReader.getFeatureReader(mainFileUrl, vc, false);
//        Iterator<VariantContext> variantIterator = reader.iterator();
//            
//            
//        BufferedWriter writer = new BufferedWriter(new FileWriter("/home/boizet/Documents/data_test/export_doubleWindowArrayDeque_chr1_20210119.csv"));
//        ArrayList<String> snpNames = new ArrayList();
//        List<String> samplesNames = new ArrayList(); 
//        LinkedHashSet<byte[]> uniqfilteredSNPs = new LinkedHashSet();
//
//        System.out.println("reading file");
//        while (variantIterator.hasNext()) {
//            VariantContext vcfEntry = variantIterator.next();
//            if (samplesNames.isEmpty()) {
//                samplesNames = vcfEntry.getSampleNamesOrderedByName();
//                //writer.write("," + String.join(",", samplesNames) + "\n");
//            }
//
//            //Get variant position and keep variants that corresponds to SNPselectionCriteria
//            String chr = vcfEntry.getContig();
//            long position = vcfEntry.getStart();
//            //TODO: what if chromosome and position are not in the file ?            
//
//            if (chr.equals(criterias.getChromosome())) {// && position >= criterias.getStart() && position <= criterias.getEnd()) {
//
//                //1. READ RAW DATA & CONVERT TO ABH FORMAT
//                //2. FILTERING ON HTZ %, MISSING DATA %, MAF
//                byte[] SNPdata =  new byte[vcfEntry.getNSamples()];
//                String parent1GT = vcfEntry.getGenotype(criterias.getParent1()).getGenotypeString();
//                String parent2GT = vcfEntry.getGenotype(criterias.getParent2()).getGenotypeString();  
//
//                String al1 = parent1GT.split("/")[0];
//                String al2 = parent2GT.split("/")[0];
//                List<String> htzGT = Arrays.asList(al1 + "/" + al2, al2 + "/" + al1);
//
//                int nbA = 0, nbB = 0, nbH = 0, nbMD = 0;
//                int ind = 0;
//                for (String sample:vcfEntry.getSampleNamesOrderedByName()) {
//                    String genotype = vcfEntry.getGenotype(sample).getGenotypeString();
//                    int DP = 0; 
//                    DP = vcfEntry.getGenotype(sample).getDP();
//                    if (DP >= criterias.getMinReads() && DP <= criterias.getMaxReads()) {
//                        if (genotype.equals(parent1GT)) {
//                            SNPdata[ind] = SimpleDoubleSlidingWindow.genPA1;
//                            nbA++;
//                        } else if (genotype.equals(parent2GT)) {
//                            SNPdata[ind] = SimpleDoubleSlidingWindow.genPA2;
//                            nbB++;
//                        } else if (htzGT.contains(genotype)){ 
//                            SNPdata[ind] = SimpleDoubleSlidingWindow.genHTZ;
//                            nbH++;
//                        } else {
//                            SNPdata[ind] = SimpleDoubleSlidingWindow.genMIS;
//                            nbMD++;
//                        }
//                    } else {
//                        SNPdata[ind] = SimpleDoubleSlidingWindow.genMIS;
//                    }
//                    ind++;
//                }
//
//                int Npop = nbA + nbB + nbH + nbMD;
//
//                if (Npop > 0) {
//
//                    float freqA = nbA / Npop;
//                    float freqB = nbB / Npop;
//                    float freqH = nbH / Npop;
//                    float freqMD = nbMD / Npop;
//
//                    if (!(freqMD >= criterias.getMinFreqMD() && freqMD <= criterias.getMaxFreqMD()
//                            && freqH >= criterias.getMinFreqH() && freqH <= criterias.getMaxFreqH()
//                            && freqA >= criterias.getMinFreqA() && freqA <= criterias.getMaxFreqA()
//                            && freqB >= criterias.getMinFreqB() && freqB <= criterias.getMaxFreqB())) {
//                        //don't keep this SNP
//                        continue;                            
//                    }
//
//                    //3. FILTERING OUT REDUNDANT LOCI
//                    //Add unique SNPdata (Redundant snp won't be saved)
//                    boolean added = uniqfilteredSNPs.add(SNPdata);
//                    if (added)
//                        snpNames.add(chr + "_" + position); 
//                }
//            }
//        }
//        final long t1 = System.currentTimeMillis();
//        System.out.println("execution time: " + (t1 - startTime));
//        System.out.println("number of snps " + uniqfilteredSNPs.size());
//
//        
//        //4. CORRECT OBVIOUS ERRONEOUS HETEROZYGOTE CALLS BEFORE FILLING
//        //WITH VERY HIGH THRESHOLD TO AVOID CORRECTING REAL HTZ CHUNKS
//        //using window of size 7
//        System.out.println("step 4: correct obvious erroneous htz calls");
//        windowSize = 7;
//        DoubleSlidingWindow slidingWindow = new DoubleSlidingWindow(windowSize, samplesNames.size());
//        long nbSNPImputed = 0;
//        ArrayList<byte[]> correctedSNPs = new ArrayList<>();
//        for (byte[] snp:uniqfilteredSNPs) {
//            byte[] removedElt = slidingWindow.put(snp);
//            if (removedElt != null) {
//                correctedSNPs.add(removedElt);
//            }
//            if (slidingWindow.getSize() == windowSize*2+1) {
//                for (int i = 0; i < slidingWindow.alleleCountBySamples.length; i++) {
//                    int Nwindow = slidingWindow.alleleCountBySamples[i].getTotalCount();
//                    int wNbA = slidingWindow.alleleCountBySamples[i].getNbA();
//                    int wNbB = slidingWindow.alleleCountBySamples[i].getNbB();
//                    byte myAllele = slidingWindow.midSnp[i];
//
//                    if (Nwindow == 0) {
//                        //this can happen when many consecutive missing data
//                    } else {
//                        //CORRECT OBVIOUS ERRONEOUS HETEROZYGOTE CALLS
//                        //If popType = "F2" Or popType = "BC1"  do not filter
//                        if (SnpSelectionCriteria.PopType.valueOf(criterias.getPopType()).equals(SnpSelectionCriteria.PopType.SSD) 
//                                || SnpSelectionCriteria.PopType.valueOf(criterias.getPopType()).equals(SnpSelectionCriteria.PopType.DH) ) {
//                            if (wNbA >= Nwindow -1 && wNbA > 4 && myAllele == DoubleSlidingWindow.genHTZ) {
//                                slidingWindow.setNewAlleleToMidSNP(i, DoubleSlidingWindow.genPA1);
//                                nbSNPImputed++;
//                            } else if (wNbB >= Nwindow -1 && wNbB > 4 && myAllele == DoubleSlidingWindow.genHTZ) {
//                                slidingWindow.setNewAlleleToMidSNP(i, DoubleSlidingWindow.genPA2);
//                                nbSNPImputed++;
//                            }
//                        }
//                        
//                    }
//                }
//            }
//            
//        }
//        
//        //Retrieve last window elements
//        correctedSNPs.addAll(slidingWindow.leftWindow);
//        correctedSNPs.add(slidingWindow.midSnp);
//        correctedSNPs.addAll(slidingWindow.rightWindow);
//        
//        final long t2 = System.currentTimeMillis();
//        System.out.println("execution time: " + (t2 - t1));
//        System.out.println("number of snps " + correctedSNPs.size());
//        
//        //5. MARK AND FILTER INCOHERENT LOCI
//        //FILLING MISSING DATA
//        System.out.println("step 5/ filling missing data");
//        for (int i = 0; i < samplesNames.size(); i++) {
//            for (int snp = 1; snp < correctedSNPs.size() - 1; snp++) {  
//                byte allele = correctedSNPs.get(snp)[i];
//                byte previousAllele = correctedSNPs.get(snp-1)[i];
//                if (allele == DoubleSlidingWindow.genMIS && previousAllele == DoubleSlidingWindow.genMIS) {
//                    for (int snp2 = snp + 1; i<correctedSNPs.size(); snp2++) {
//                        byte allele2 = correctedSNPs.get(snp2)[i];
//                        if (allele2 != DoubleSlidingWindow.genMIS) {
//                            if (allele2 == previousAllele) {
//                                correctedSNPs.get(snp)[i] = previousAllele;
//                                nbSNPImputed++;
//                            }
//                            break;
//                        }
//                    }
//                }
//            }
//        }
//        final long t3 = System.currentTimeMillis();
//        System.out.println("execution time: " + (t3 - t2));
//        System.out.println("number of snps " + correctedSNPs.size());
//        
//        // DETECT INCOHERENT SNPs
//        System.out.println("step5/ detect incoherent snps");
//        windowSize = 15; //parametre
//        double chi2threshold = 0.1;
//        boolean tryOtherPhase = true;
//        ArrayList<byte[]> coherentSNPs = new ArrayList();
//        slidingWindow = new DoubleSlidingWindow(windowSize, samplesNames.size());
//        ArrayList<Integer> badSnpIndexes = new ArrayList<>();
//        
//        for (byte[] snp:correctedSNPs) {
//                    
//            byte[] removedElt = slidingWindow.put(snp);
//            if (removedElt != null) {
//                coherentSNPs.add(removedElt);
//            }           
//
//            
//            if (slidingWindow.getSize() == windowSize*2+1) {
//                long wNbA = slidingWindow.leftWindowAlleleCounts.getNbA() + slidingWindow.rightWindowAlleleCounts.getNbA();
//                long wNbB = slidingWindow.leftWindowAlleleCounts.getNbB() + slidingWindow.rightWindowAlleleCounts.getNbB();
//                long wNbH = slidingWindow.leftWindowAlleleCounts.getNbH() + slidingWindow.rightWindowAlleleCounts.getNbH();
////                wNbA = slidingWindow.alleleCountBySnpsLeftWindow.stream().mapToInt(act -> act.getNbA()).sum();
////                wNbA = wNbA + slidingWindow.alleleCountBySnpsRightWindow.stream().mapToInt(act -> act.getNbA()).sum();
////                
//
//               
//                long wNpop = wNbA + wNbB + wNbH;
//
//                long Npop = slidingWindow.midSnpAlleleCounts.getNbA() + slidingWindow.midSnpAlleleCounts.getNbB()
//                        + slidingWindow.midSnpAlleleCounts.getNbH();
//                if (Npop > 0) {
//                    double chi2SNP;
//                    if (SnpSelectionCriteria.PopType.valueOf(criterias.getPopType()).equals(SnpSelectionCriteria.PopType.SSD) 
//                        || SnpSelectionCriteria.PopType.valueOf(criterias.getPopType()).equals(SnpSelectionCriteria.PopType.DH) ) {
//                        chi2SNP = (double) (Math.pow((slidingWindow.midSnpAlleleCounts.getNbA() - wNbA*Npop/wNpop), 2)/(wNbA*Npop/wNpop))
//                                + Math.pow((slidingWindow.midSnpAlleleCounts.getNbB() - wNbB*Npop/wNpop), 2)/(wNbB*Npop/wNpop);
//                    } else {
//                        chi2SNP = (double) (Math.pow((slidingWindow.midSnpAlleleCounts.getNbA() - wNbA*Npop/wNpop), 2)/(wNbA*Npop/wNpop))
//                                + Math.pow((slidingWindow.midSnpAlleleCounts.getNbB() - wNbB*Npop/wNpop), 2)/(wNbB*Npop/wNpop)
//                                + Math.pow((slidingWindow.midSnpAlleleCounts.getNbH() - wNbH*Npop/wNpop), 2)/(wNbH*Npop/wNpop);                        
//                    }
//                    
//                    if (chi2SNP > chi2threshold) { //the Chi2 is significant
//                        
//                        if (tryOtherPhase) {
//                            if (SnpSelectionCriteria.PopType.valueOf(criterias.getPopType()).equals(SnpSelectionCriteria.PopType.SSD) 
//                                || SnpSelectionCriteria.PopType.valueOf(criterias.getPopType()).equals(SnpSelectionCriteria.PopType.DH) ) {
//                                chi2SNP = (double) (Math.pow((slidingWindow.midSnpAlleleCounts.getNbB() - wNbA*Npop/wNpop), 2)/(wNbA*Npop/wNpop))
//                                        + Math.pow((slidingWindow.midSnpAlleleCounts.getNbA() - wNbB*Npop/wNpop), 2)/(wNbB*Npop/wNpop);
//                            } else {
//                                chi2SNP = (double) (Math.pow((slidingWindow.midSnpAlleleCounts.getNbB() - wNbA*Npop/wNpop), 2)/(wNbA*Npop/wNpop))
//                                        + Math.pow((slidingWindow.midSnpAlleleCounts.getNbA() - wNbB), 2)/(wNbB*Npop/wNpop)
//                                        + Math.pow((slidingWindow.midSnpAlleleCounts.getNbH() - wNbH), 2)/(wNbH*Npop/wNpop);                        
//                            }
//                            
//                        }
//                        if (chi2SNP > chi2threshold) { //the Chi2 is again significant, don't save the SNP
//                            //NbOfBadSNP = NbOfBadSNP + 1
//                            //badSnpIndexes.add(slidingWindow.midSnpIndex);
//                            slidingWindow.removeMidSnp(); //remove snp from slidingWindow so that won't be kept in total snp list
//                            snpNames.remove(slidingWindow.midSnpIndex); //remove snp name
//                            //TODO: see if problematic to update slidingWindow here
//                            // or if it is better to keep initial values for next snps chi2 computations
//                            
//                        } else {
//                            
//                            for (int i = 0; i < slidingWindow.midSnp.length; i++) {
//                                if (slidingWindow.midSnp[i] == DoubleSlidingWindow.genPA1) {
//                                    slidingWindow.setNewAlleleToMidSNP(i, DoubleSlidingWindow.genPA2);
//                                } else if (slidingWindow.midSnp[i] == DoubleSlidingWindow.genPA2) {
//                                    slidingWindow.setNewAlleleToMidSNP(i, DoubleSlidingWindow.genPA1);
//                                }
//                            }
//                        }
//
//                    }
//                }
//                
//            }
//            
//        }
//        
//        //Retrieve last window elements
//        coherentSNPs.addAll(slidingWindow.leftWindow);
//        coherentSNPs.add(slidingWindow.midSnp);
//        coherentSNPs.addAll(slidingWindow.rightWindow);
//        
//        final long t4 = System.currentTimeMillis();
//        System.out.println("execution time: " + (t4 - t3));
//        System.out.println("number of snps " + coherentSNPs.size());
//        
//        final long endTime = System.currentTimeMillis();
//        System.out.println("Total execution time: " + (endTime - startTime));
//        
//        int j = 0;
//        for (byte[] snp:coherentSNPs) {            
//            String genotypes = Arrays.toString(snp).substring(1);
//            genotypes = genotypes.substring(0, genotypes.length() - 1);
//            String line = snpNames.get(j) + "," + genotypes + "\n";
//            writer.write(line); 
//            j++;
//        }
//        
//        writer.close();
//        
//    }
//        
//
//    
//}
//
