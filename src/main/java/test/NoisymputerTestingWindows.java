//package test;
//
//import htsjdk.tribble.AbstractFeatureReader;
//import htsjdk.tribble.FeatureReader;
//import htsjdk.variant.variantcontext.VariantContext;
//import htsjdk.variant.vcf.VCFCodec;
//import java.io.BufferedWriter;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.net.MalformedURLException;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.HashMap;
//import java.util.HashSet;
//import java.util.Iterator;
//import java.util.LinkedHashSet;
//import java.util.List;
//import java.util.Set;
//import noisymputer.ArraySlidingWindow;
//import noisymputer.SnpSelectionCriteria;
//
///**
// *
// * @author boizet
// */
//public class NoisymputerTestingWindows {
//    
//    public static void main(String[] args) throws MalformedURLException, IOException, Exception {
//        final long startTime = System.currentTimeMillis();
//       
//        String mainFileUrl = "/home/boizet/Dropbox/NOISYmputer_project/Example_files/HC_50ind_control.vcf";
//        //String mainFileUrl = "/home/boizet/Documents/data_test/test.vcf";
//        SnpSelectionCriteria criterias = new SnpSelectionCriteria();
//
//        criterias.setParent1("kitaake");
//        criterias.setParent2("kalinga");
//        //criterias.setParent1("P1");
//        //criterias.setParent2("P2");
//        criterias.setChromosome("chr1");
//        //criterias.setChromosome("20");
//        int windowSize = 15;
//        
//
//        VCFCodec vc = new VCFCodec();
//        FeatureReader<VariantContext> reader = AbstractFeatureReader.getFeatureReader(mainFileUrl, vc, false);
//        Iterator<VariantContext> variantIterator = reader.iterator();
//        
//        
//        
//        String arrayDequeOutputFile = "/home/boizet/Documents/data_test/export_doubleWindowArrayDeque_all.csv";
//        String arrayOutputFile = "/home/boizet/Documents/data_test/export_windowArray_all.csv";
//        
//        ArrayList<String> snps1 = readWithArrayDeque(windowSize, criterias, arrayDequeOutputFile, mainFileUrl);
//        ArrayList<String> snps2 = readWithArray(windowSize, criterias, arrayOutputFile, mainFileUrl);
//        
////        final long t1 = System.currentTimeMillis();
////        System.out.println("Comparing the 2 snp list");
////        if (snps1.equals(snps2)) {
////            System.out.println("OK");
////        } else {
////            System.out.println("NOK");
////            for (int i = 0; i < snps1.size(); i++) {
////                if (!snps1.get(i).equals(snps2.get(i))) {
////                    System.out.println(snps1.get(i));
////                    System.out.println(snps2.get(i));
////                }
////            }
////        } 
////
////        final long t2 = System.currentTimeMillis();
////        System.out.println(t2-t1);
//        
//    }
//        
//        private static ArrayList<String> readWithArrayDeque(int windowSize, SnpSelectionCriteria criterias, String outputfile, String mainFileUrl) throws IOException {
//            final long startTime = System.currentTimeMillis();
//            System.out.println("ArrayDeque results: ");
//
//            VCFCodec vc = new VCFCodec();
//            FeatureReader<VariantContext> reader = AbstractFeatureReader.getFeatureReader(mainFileUrl, vc, false);
//            Iterator<VariantContext> variantIterator = reader.iterator();
//            
//            
//            BufferedWriter writer = new BufferedWriter(new FileWriter(outputfile));
//            SimpleDoubleSlidingWindow slidingWindow = null;
//            ArrayList<String> keptSNPs = new ArrayList();
//            int nbSNPImputed = 0;
//            List<String> samplesNames = null; 
//            ArrayList<byte[]> filteredSNPs = new ArrayList();
//            int index = 0;
//            
//            while (variantIterator.hasNext()) {
//                VariantContext vcfEntry = variantIterator.next();
//                if (samplesNames == null) {
//                    samplesNames = vcfEntry.getSampleNamesOrderedByName();
//                    writer.write("," + String.join(",", samplesNames) + "\n");
//                }
//                if (slidingWindow == null)
//                    slidingWindow = new SimpleDoubleSlidingWindow(windowSize, vcfEntry.getNSamples());
//                //1. convert to ABH format
//                //Get variant position and keep variants that corresponds to SNPselectionCriteria
//                String chr = vcfEntry.getContig();
//                long position = vcfEntry.getStart();
//                //TODO: what if chromosome and position are not in the file ?            
//
//                //if (chr.equals(criterias.getChromosome())) {// && position >= criterias.getStart() && position <= criterias.getEnd()) {
//                if (true) {    
//                    index++;
//                    
//                    byte[] SNPdata =  new byte[vcfEntry.getNSamples()];
//                    String parent1GT = vcfEntry.getGenotype(criterias.getParent1()).getGenotypeString();
//                    String parent2GT = vcfEntry.getGenotype(criterias.getParent2()).getGenotypeString();  
//                                            
//                    String al1 = parent1GT.split("/")[0];
//                    String al2 = parent2GT.split("/")[0];
//                    List<String> htzGT = Arrays.asList(al1 + "/" + al2, al2 + "/" + al1);
//                    
//                    String name = chr + "_" + position;
////                    if (name.equals("chr1_44278423")) {
////                        System.out.println(vcfEntry.getGenotypes().toString());
////                    }
//
//                    int nbA = 0, nbB = 0, nbH = 0, nbMD = 0;
//                    int ind = 0;
//                    for (String sample:vcfEntry.getSampleNamesOrderedByName()) {
//                        String genotype = vcfEntry.getGenotype(sample).getGenotypeString();
//                        int DP = 0; 
//                        DP = vcfEntry.getGenotype(sample).getDP();
//                        if (DP >= criterias.getMinReads() && DP <= criterias.getMaxReads()) {
//                            if (genotype.equals(parent1GT)) {
//                                SNPdata[ind] = SimpleDoubleSlidingWindow.genPA1;
//                                nbA++;
//                            } else if (genotype.equals(parent2GT)) {
//                                SNPdata[ind] = SimpleDoubleSlidingWindow.genPA2;
//                                nbB++;
//                            } else if (htzGT.contains(genotype)){ 
//                                SNPdata[ind] = SimpleDoubleSlidingWindow.genHTZ;
//                                nbH++;
//                            } else {
//                                SNPdata[ind] = SimpleDoubleSlidingWindow.genMIS;
//                                nbMD++;
//                            }
//                        } else {
//                            SNPdata[ind] = SimpleDoubleSlidingWindow.genMIS;
//                        }
//                        ind++;
//                    }
//
//                    int Npop = nbA + nbB + nbH + nbMD;
//
//                    if (Npop > 0) {
//
//                        float freqA = nbA / Npop;
//                        float freqB = nbB / Npop;
//                        float freqH = nbH / Npop;
//                        float freqMD = nbMD / Npop;
//
//                        if (!(freqMD >= criterias.getMinFreqMD() && freqMD <= criterias.getMaxFreqMD()
//                                && freqH >= criterias.getMinFreqH() && freqH <= criterias.getMaxFreqH()
//                                && freqA >= criterias.getMinFreqA() && freqA <= criterias.getMaxFreqA()
//                                && freqB >= criterias.getMinFreqB() && freqB <= criterias.getMaxFreqB())) {
//                            //don't keep this SNP
//                            continue;                            
//                        }
//
//                        //track which snps are kept
//                        keptSNPs.add(chr + "_" + position);
//
//                        //fill windows
//                        byte[] removedElt = slidingWindow.put(SNPdata);
////                        if (removedElt != null) {
////                            filteredSNPs.add(removedElt);
////                        }
//
//
//                        
//                    }
//                }
//            }
//        
//            final long time2 = System.currentTimeMillis();
//            System.out.println("reading and converting to ABH: " + (time2 - startTime));
//
////            //Retrieve last window elements
////            filteredSNPs.addAll(slidingWindow.leftWindow);
////            filteredSNPs.add(slidingWindow.midSnp);
////            filteredSNPs.addAll(slidingWindow.rightWindow);
////
////
////            //FILTERING OUT REDUNDANT LOCI
////            LinkedHashSet<byte[]> uniqFilteredSNPs = new LinkedHashSet();
////            for (int i = 0; i < filteredSNPs.size(); i++) {
////                boolean added = uniqFilteredSNPs.add(filteredSNPs.get(i));
////                if (!added) {
////                    keptSNPs.remove(i);
////                }
////            } 
////            final long time3 = System.currentTimeMillis();
////            System.out.println("Filtering redundant loci: " + (time3 - time2));
////
////
////
////
////
////            System.out.println("number of snps:" + uniqFilteredSNPs.size());
////            System.out.println("number of imputed snps:" + nbSNPImputed);
////            ArrayList<String> snps = new ArrayList<>();
////            int j = 0;
////            for (byte[] snp:uniqFilteredSNPs) {            
////                String genotypes = Arrays.toString(snp).substring(1);
////                genotypes = genotypes.substring(0, genotypes.length() - 1);
////                String line = keptSNPs.get(j) + "," + genotypes + "\n";
////                writer.write(line); 
////                snps.add(line);
////                j++;
////            }
////
////            writer.close();
////            final long endTime = System.currentTimeMillis();
////            System.out.println("writing output file: " + (endTime - time3));
////            System.out.println("Total execution time: " + (endTime - startTime));
//            
////            return snps;
//            return new ArrayList<>();
//        }
//
//
//
//
//        private static ArrayList<String> readWithArray(int windowSize, SnpSelectionCriteria criterias, String outputfile, String mainFileUrl) throws IOException {
//            final long startTime = System.currentTimeMillis();
//            System.out.println("array results: ");
//            
//            VCFCodec vc = new VCFCodec();
//            FeatureReader<VariantContext> reader = AbstractFeatureReader.getFeatureReader(mainFileUrl, vc, false);
//            Iterator<VariantContext> variantIterator = reader.iterator();
//            
//            ArraySlidingWindow slidingWindow = null;
//        
//            BufferedWriter writer = new BufferedWriter(new FileWriter(outputfile));
//
//            ArrayList<String> keptSNPs = new ArrayList();
//            int nbSNPImputed = 0;
//            List<String> samplesNames = null; 
//            ArrayList<byte[]> filteredSNPs = new ArrayList();
//
//            int index = 0;
//            while (variantIterator.hasNext()) {
//                VariantContext vcfEntry = variantIterator.next();
//                if (samplesNames == null) {
//                    samplesNames = vcfEntry.getSampleNamesOrderedByName();
//                    writer.write("," + String.join(",", samplesNames) + "\n");
//                }
//                if (slidingWindow == null)
//                    slidingWindow = new ArraySlidingWindow(windowSize, vcfEntry.getNSamples());
//                //1. convert to ABH format
//                //Get variant position and keep variants that corresponds to SNPselectionCriteria
//                String chr = vcfEntry.getContig();
//                long position = vcfEntry.getStart();
//                //TODO: what if chromosome and position are not in the file ?            
//
//                //if (chr.equals(criterias.getChromosome())) { //&& position >= criterias.getStart() && position <= criterias.getEnd()) {
//                if (true) {
//                    index++;
//                    //System.out.println(Integer.toString(index));
//                    byte[] SNPdata =  new byte[vcfEntry.getNSamples()];
//                    String parent1GT = vcfEntry.getGenotype(criterias.getParent1()).getGenotypeString();
//                    String parent2GT = vcfEntry.getGenotype(criterias.getParent2()).getGenotypeString();
//                    String al1 = parent1GT.split("/")[0];
//                    String al2 = parent2GT.split("/")[0];
//                    List<String> htzGT = Arrays.asList(al1 + "/" + al2, al2 + "/" + al1);                
//
//                    int nbA = 0, nbB = 0, nbH = 0, nbMD = 0;
//                    int ind = 0;
//                    for (String sample:vcfEntry.getSampleNamesOrderedByName()) {                       
//                        String genotype = vcfEntry.getGenotype(sample).getGenotypeString();
//                        int DP = 0; 
//                        DP = vcfEntry.getGenotype(sample).getDP();
//                        if (DP >= criterias.getMinReads() && DP <= criterias.getMaxReads()) {
//                            if (genotype.equals(parent1GT)) {
//                                SNPdata[ind] = SimpleDoubleSlidingWindow.genPA1;
//                                nbA++;
//                            } else if (genotype.equals(parent2GT)) {
//                                SNPdata[ind] = SimpleDoubleSlidingWindow.genPA2;
//                                nbB++;
//                            } else if (htzGT.contains(genotype)){ 
//                                SNPdata[ind] = SimpleDoubleSlidingWindow.genHTZ;
//                                nbH++;
//                            } else {
//                                SNPdata[ind] = SimpleDoubleSlidingWindow.genMIS;
//                                nbMD++;
//                            }
//                        } else {
//                            SNPdata[ind] = SimpleDoubleSlidingWindow.genMIS;
//                        }
//                        ind++;
//                    }
//
//                    int Npop = nbA + nbB + nbH + nbMD;
//
//                    if (Npop > 0) {
//
//                        float freqA = nbA / Npop;
//                        float freqB = nbB / Npop;
//                        float freqH = nbH / Npop;
//                        float freqMD = nbMD / Npop;
//
//                        if (!(freqMD >= criterias.getMinFreqMD() && freqMD <= criterias.getMaxFreqMD()
//                                && freqH >= criterias.getMinFreqH() && freqH <= criterias.getMaxFreqH()
//                                && freqA >= criterias.getMinFreqA() && freqA <= criterias.getMaxFreqA()
//                                && freqB >= criterias.getMinFreqB() && freqB <= criterias.getMaxFreqB())) {
//                            //don't keep this SNP
//                            continue;                            
//                        }
//
//                        //track which snps are kept
//                        keptSNPs.add(chr + "_" + position);
//
//                        //fill windows
//                        byte[] removedElt = slidingWindow.put(SNPdata);
////                        if (removedElt != null) {
////
////                            if (removedElt[0] != 0) {
////                                filteredSNPs.add(removedElt);
////                            }
////                        }
//
//                    }      
//
//                }
//            }
//
//            final long time2 = System.currentTimeMillis();
//            System.out.println("reading and converting to ABH: " + (time2 - startTime));
//
//
////            //Retrieve last window elements
////            for (int i = 0; i < slidingWindow.storage.length; i++ ) {
////                
//////                System.out.println("_________________________");
//////                System.out.println("index " + i);
//////                System.out.println(Arrays.toString(slidingWindow.storage[i]));
//////                System.out.println("_________________________");
////                
////                byte[] removedElt = slidingWindow.put(new byte[samplesNames.size()]);
////                filteredSNPs.add(removedElt);
////                
////            }
////
////
////            //FILTERING OUT REDUNDANT LOCI
////            LinkedHashSet<byte[]> uniqFilteredSNPs = new LinkedHashSet();
////            for (int i = 0; i<filteredSNPs.size(); i++) {
////                boolean added = uniqFilteredSNPs.add(filteredSNPs.get(i));
////                if (!added) {
////                    keptSNPs.remove(i);
////                }
////            }      
////
////            final long time3 = System.currentTimeMillis();
////            System.out.println("Filtering redundant loci: " + (time3 - time2));
////
////
////            System.out.println("number of snps:" + uniqFilteredSNPs.size());
////            System.out.println("number of imputed snps:" + nbSNPImputed);
////            
////            ArrayList<String> snps = new ArrayList<>();                    
////            int j = 0;
////            for (byte[] snp:uniqFilteredSNPs) {            
////                String genotypes = Arrays.toString(snp).substring(1);
////                genotypes = genotypes.substring(0, genotypes.length() - 1);
////                String line = keptSNPs.get(j) + "," + genotypes + "\n";
////                writer.write(line);    
////                snps.add(line);
////                j++;
////            }
////
////            writer.close();
////            final long endTime = System.currentTimeMillis();
////            System.out.println("writing output file: " + (endTime - time3));
////            System.out.println("Total execution time: " + (endTime - startTime));
////            
////            return snps;
//            return new ArrayList<>();
//        }        
//    
//}
//
